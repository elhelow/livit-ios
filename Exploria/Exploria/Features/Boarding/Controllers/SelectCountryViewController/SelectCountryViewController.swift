//
//  SelectCountryViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 9/28/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

class SelectCountryViewController: UIViewController {
    @IBOutlet weak var countryFlag : UIImageView!
    @IBOutlet weak var countryName : UILabel!
    @IBOutlet weak var arrowImage : UIImageView!

    @IBOutlet weak var languageLabel : UILabel!
    @IBOutlet weak var startButton : UIButton!
    
    @IBOutlet weak var languageContainerView : UIView!
    
    @IBOutlet weak var locationMainContainerView : UIView!
    @IBOutlet weak var locationFlagContainerView : UIView!
    @IBOutlet weak var locationDetailsContainerView : UIView!
    
    var selectedCountry : Country = Country(countryNameEn: "Kuwait", countryNameAr:"الكويت",countryImageName: "kuwait", countryID: "9", prefix:"+965")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationFlagContainerView.layer.cornerRadius = 3
        locationDetailsContainerView.layer.cornerRadius = 3
        languageContainerView.layer.cornerRadius = 10
        startButton.layer.cornerRadius = 40
        
        updateLayoutBasedOnLocalizationWithAnimation(false)
        
        self.start(UIButton())
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        self.countryFlag.image = selectedCountry.countryImage
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.countryName.text = selectedCountry.countryNameAr
        }else{
            self.countryName.text = selectedCountry.countryNameEn
        }

        self.selectedCountry.loadImageWithDelegate(self)
        
    }
    
    override func viewDidLayoutSubviews() {

    }

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showCountryList"){
            let countryListViewController = segue.destination as! CountryListViewController
            countryListViewController.delegate = self
            countryListViewController.currentSelectedCountry = selectedCountry
        }
    }
    
    @IBAction func start(_ sender: AnyObject){
        CountriesDataCenter.sharedInstance.selectedCountry = selectedCountry
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setMainNavigatorAsRootViewController()
    }
    
    @IBAction func changeLanguage(_ sender: AnyObject){
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            LocalizationManager.sharedInstance.changeCurrentLanguageTo(.english)
        }else{
            LocalizationManager.sharedInstance.changeCurrentLanguageTo(.arabic)
        }
        
        updateLayoutBasedOnLocalizationWithAnimation(true)
    }
    
    func updateLayoutBasedOnLocalizationWithAnimation(_ isAnimating : Bool){
        func updateLayout(){
            
            self.startButton.setTitle(
                LocalizationManager.sharedInstance.getTranslationForKey("Start"), for: .normal)
            self.languageLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Language")
            
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                self.view.semanticContentAttribute = .forceRightToLeft
                self.locationMainContainerView.semanticContentAttribute = .forceRightToLeft
                self.locationFlagContainerView.semanticContentAttribute = .forceRightToLeft
                self.locationDetailsContainerView.semanticContentAttribute = .forceRightToLeft
                
                self.countryName.textAlignment = .right
                UIUtilities.rotateView(self.arrowImage, withDegrees: Double.pi)
            }else{
                self.view.semanticContentAttribute = .unspecified
                self.locationMainContainerView.semanticContentAttribute = .unspecified
                self.locationFlagContainerView.semanticContentAttribute = .unspecified
                self.locationDetailsContainerView.semanticContentAttribute = .unspecified
                
                self.countryName.textAlignment = .left
                UIUtilities.resetRotationToInitialFormFor(self.arrowImage)
            }
        }
        
        if(isAnimating){
            updateLayout()
            AnimationsUtility.animateLayoutFor(self.view, duration: 0.5, delay: 0.0, completionBlock: nil)
        }else{
            updateLayout()
        }
    }
}

extension SelectCountryViewController : CountryListViewControllerDelegate{
    func didSelectCountry(_ country: Country) {
        selectedCountry = country
    }
}

extension SelectCountryViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        self.countryFlag.image = selectedCountry.countryImage
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}
