//
//  CountryListViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/1/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

protocol CountryListViewControllerDelegate : NSObjectProtocol {
    func didSelectCountry(_ country: Country)
}

class CountryListViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var startButton : UIButton!    
    weak var delegate : CountryListViewControllerDelegate?
    var currentSelectedCountry : Country?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startButton.clipsToBounds = true
        startButton.layer.cornerRadius = 10
        
        self.title = LocalizationManager.sharedInstance.getTranslationForKey("SelectCountry")!
        
        self.startButton.setTitle(
            LocalizationManager.sharedInstance.getTranslationForKey("Start"), for: .normal)
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.hidesBackButton = true
        
        
        self.tableView.estimatedRowHeight = 68
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.tableView.semanticContentAttribute = .forceRightToLeft
        }
        
        CountriesDataCenter.sharedInstance.loadCountriesWithDelegate(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    @IBAction func start(_ sender: AnyObject){
        CountriesDataCenter.sharedInstance.selectedCountry = currentSelectedCountry
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setMainNavigatorAsRootViewController()
    }
}


extension CountryListViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CountriesDataCenter.sharedInstance.countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell") as! CountryTableViewCell
        let country = CountriesDataCenter.sharedInstance.countries[indexPath.row]
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            cell.countryName.text = country.countryNameAr
        }else{
            cell.countryName.text = country.countryNameEn
        }


        
        if(country.countryImage == nil){
            country.loadImageWithDelegate(self)
        }else{
            cell.countryIcon.image = country.countryImage
        }
        
        if(currentSelectedCountry?.countryID == country.countryID){
            cell.checkMarkImage.isHidden = false
        }
        
        if(indexPath.row + 1 == CountriesDataCenter.sharedInstance.countries.count){
            cell.seperatorImage.isHidden = true
        }
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            cell.contentView.semanticContentAttribute = .forceRightToLeft
            cell.countryName.textAlignment = .right
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentSelectedCountry = CountriesDataCenter.sharedInstance.countries[indexPath.row]
        delegate?.didSelectCountry(currentSelectedCountry!)
        self.tableView.reloadData()
    }
}

extension CountryListViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        self.tableView.reloadData()
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}
