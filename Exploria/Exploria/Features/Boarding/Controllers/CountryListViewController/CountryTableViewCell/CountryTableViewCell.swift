//
//  CountryTableViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/1/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class CountryTableViewCell: UITableViewCell {
    @IBOutlet weak var countryName : UILabel!
    @IBOutlet weak var countryIcon : UIImageView!
    @IBOutlet weak var checkMarkImage : UIImageView!
    @IBOutlet weak var seperatorImage : UIImageView!
    
    override func awakeFromNib() {
        checkMarkImage.isHidden = true
    }
    
    override func prepareForReuse() {
        self.countryIcon.image = #imageLiteral(resourceName: "imagePlaceHolder")
        self.checkMarkImage.isHidden = true
        self.seperatorImage.isHidden = false
    }
}

