//
//  MenuViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/2/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

import Foundation
import UIKit

enum MenuItem : String{
    case Home
    case Offers
    case MyAccount
    case ContactUs
    case Language
    case Signin
    case PersonalInfo
    case MyBookings
//    case PaymentHistory
    case MyFavorites
    
    func stringValueBasedOnLocalization() -> String {
        if(self == .Signin){
            if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
                return LocalizationManager.sharedInstance.getTranslationForKey("SingOut")!
            }
        }
        return LocalizationManager.sharedInstance.getTranslationForKey(self.rawValue)!
    }
    
    func intValue() -> Int {
        switch self {
        case .Home:
            return 0
        case .Offers:
            return 1
        case .MyAccount:
            return 2
        case .ContactUs:
            return 3
        case .Language:
            return 4
        case .Signin:
            return 5
        case .PersonalInfo:
            return 1
        case .MyBookings:
            return 2
//        case .PaymentHistory:
//            return 3
        case .MyFavorites:
            return 3
        }
    }
    
    static var count = 6
}

class MenuViewController: UIViewController {
    @IBOutlet weak var menuTableView : UITableView!
    
    @IBOutlet weak var offersContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var offerImage : UIImageView!
    
    @IBOutlet weak var userName : UILabel!
    @IBOutlet weak var userNameSeperator : UIImageView!
    @IBOutlet weak var userNameContainerView : UIView!
    
    fileprivate var menuItems : [MenuItem] = []
    fileprivate var selectedMenuItem : MenuItem = .Home
    fileprivate var isSubMenuItemIsOpened = false
    fileprivate var isCenteralPanelContainsSubMenuItem = false
    fileprivate var isChangingLanguage = false
    
    class func sharedInstance() -> MenuViewController {
        return MainNavigator.sharedInstance.getCurrentMenuViewController()
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.menuTableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        self.menuTableView.register(UINib(nibName: "MenuHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuHeaderTableViewCell")
        
        self.menuTableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.view.semanticContentAttribute = .forceRightToLeft
            self.menuTableView.semanticContentAttribute = .forceRightToLeft
            self.userNameContainerView.semanticContentAttribute = .forceRightToLeft
            self.userName.textAlignment = .right
        }
        
        self.reloadMenuItems()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func reloadMenuItems(){
        
        OffersDataCenter.sharedInstance.loadSpecialOffersWithDelegate(self)
        
        if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
            self.userName.text = (UserIdentificationDataCenter.sharedInstance.loggedInUser?.name)!
            self.userNameSeperator.isHidden = false
        }else if(OffersDataCenter.sharedInstance.specialOffer == nil){
            self.userNameSeperator.isHidden = true
            self.userName.text = ""
        }else{
            self.userNameSeperator.isHidden = false
            self.userName.text = ""
        }
        
        self.reloadOffersData()
        
        menuItems.removeAll()
        if(isSubMenuItemIsOpened){
            menuItems.append(.MyAccount)
            menuItems.append(.PersonalInfo)
            menuItems.append(.MyBookings)
//            menuItems.append(.PaymentHistory)
            menuItems.append(.MyFavorites)
        }else{
            if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
                menuItems.append(.Home)
                menuItems.append(.Offers)
                menuItems.append(.MyAccount)
                menuItems.append(.ContactUs)
                menuItems.append(.Language)
                menuItems.append(.Signin)
            }else{
                menuItems.append(.Home)
                menuItems.append(.Offers)
                menuItems.append(.ContactUs)
                menuItems.append(.Language)
                menuItems.append(.Signin)
            }
        }
        self.menuTableView.reloadData()
    }
    
    func reloadOffersData(){
        if(OffersDataCenter.sharedInstance.specialOffer != nil){
            if(OffersDataCenter.sharedInstance.specialOffer?.image != nil){
                self.offerImage.image = OffersDataCenter.sharedInstance.specialOffer?.image
            }else{
                self.offerImage.image = #imageLiteral(resourceName: "imagePlaceHolder")
                OffersDataCenter.sharedInstance.specialOffer?.loadImageWithDelegate(self)
            }
            offersContainerViewHeightConstraint.constant = 150
            self.userNameSeperator.isHidden = false
        }else{
            offersContainerViewHeightConstraint.constant = 0
        }
    }
    
    func reloadViewsBasedOnCurrentLocalization(){
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.view.semanticContentAttribute = .forceRightToLeft
            self.menuTableView.semanticContentAttribute = .forceRightToLeft
            self.userNameContainerView.semanticContentAttribute = .forceRightToLeft
            self.userName.textAlignment = .right
        }else{
            self.view.semanticContentAttribute = .forceLeftToRight
            self.menuTableView.semanticContentAttribute = .forceLeftToRight
            self.userNameContainerView.semanticContentAttribute = .forceLeftToRight
            self.userName.textAlignment = .left
        }
        self.menuTableView.reloadData()
    }
    
    func goToMenuItem(_ menuItem: MenuItem){
        self.tableView(menuTableView, didSelectRowAt: IndexPath(row: menuItem.intValue(), section: 0))
    }
    
    func dismissSubMenu(){
        isSubMenuItemIsOpened = false
        self.reloadMenuItems()
    }
    
    @IBAction func goToProviderDetails(_ sender: UIButton) {
        
        MainNavigator.sharedInstance.closeMenu()
        
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let providerDetailsViewController = storyboard.instantiateViewController(withIdentifier: "ProviderDetailsViewController") as! ProviderDetailsViewController
        providerDetailsViewController.activityProviderID = (OffersDataCenter.sharedInstance.specialOffer?.activityProviderID)!
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: providerDetailsViewController)
    }
}

extension MenuViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : MenuTableViewCell = MenuTableViewCell()
        
        if(isSubMenuItemIsOpened && indexPath.row == 0){
            cell = tableView.dequeueReusableCell(withIdentifier: "MenuHeaderTableViewCell") as! MenuHeaderTableViewCell
            (cell as! MenuHeaderTableViewCell).delegate = self
            UIUtilities.resetRotationToInitialFormFor((cell as! MenuHeaderTableViewCell).backArrowImageView)
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
                UIUtilities.rotateView((cell as! MenuHeaderTableViewCell).backArrowImageView, withDegrees: Double.pi)
            }
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        }
        
        cell.title.text = menuItems[indexPath.row].stringValueBasedOnLocalization()
        cell.title.awakeFromNib()
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            cell.title.textAlignment = .right
        }else{
            cell.title.textAlignment = .left
        }
        
        if(menuItems[indexPath.row] == .Language){
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
                cell.title.font = UIFont(name: "DroidArabicKufi-Bold", size: CGFloat(cell.title.font.pointSize))
            }else{
                cell.title.font = UIFont(name: "Dosis-Medium", size: CGFloat(cell.title.font.pointSize))
            }
        }
        
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(isSubMenuItemIsOpened){
            isCenteralPanelContainsSubMenuItem = true
            switch indexPath.row {
            case MenuItem.PersonalInfo.intValue():
                let storyboard = UIStoryboard(name: "UserIdentification", bundle: nil)
                let profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController")
                selectedMenuItem = .PersonalInfo
                MainNavigator.sharedInstance.changeCenterPanel(to: profileViewController)
            case MenuItem.MyFavorites.intValue():
                let storyboard = UIStoryboard(name: "Activities", bundle: nil)
                let favoritesViewController = storyboard.instantiateViewController(withIdentifier: "FavoritesViewController")
                selectedMenuItem = .MyFavorites
                MainNavigator.sharedInstance.changeCenterPanel(to: favoritesViewController)
            case MenuItem.MyBookings.intValue():
                let storyboard = UIStoryboard(name: "Booking", bundle: nil)
                let favoritesViewController = storyboard.instantiateViewController(withIdentifier: "MyBookingsViewController")
                selectedMenuItem = .MyBookings
                MainNavigator.sharedInstance.changeCenterPanel(to: favoritesViewController)                
            default:
            break
            }
            
            let tempChante = isChangingLanguage
            ThreadsUtility.execute({
                if(!tempChante){
                    MainNavigator.sharedInstance.closeMenu()
                }
            }, afterDelay: 0.05)
            
            isChangingLanguage = false
            
            return
        }
        
        
        let row = indexPath.row > 1 ? indexPath.row + (MenuItem.count - menuItems.count) : indexPath.row
        
        switch row {
        case MenuItem.Home.intValue():
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
            self.selectedMenuItem = .Home
            MainNavigator.sharedInstance.changeCenterPanel(to: homeViewController)
        case MenuItem.Offers.intValue():
            let storyboard = UIStoryboard(name: "Offers", bundle: nil)
            let offersViewController = storyboard.instantiateViewController(withIdentifier: "OffersViewController")
            self.selectedMenuItem = .Offers
            MainNavigator.sharedInstance.changeCenterPanel(to: offersViewController)
        case MenuItem.Language.intValue():
            ThreadsUtility.execute({
                if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                    LocalizationManager.sharedInstance.changeCurrentLanguageTo(.english)
                    MainNavigator.sharedInstance.changeMenuToTheLeft()
                }else{
                    LocalizationManager.sharedInstance.changeCurrentLanguageTo(.arabic)
                    MainNavigator.sharedInstance.changeMenuToTheRight()
                }
                (UIApplication.shared.delegate as! AppDelegate).updateCurrentTitleFont()
                self.isChangingLanguage = true
                ThreadsUtility.execute({
                    self.tableView(self.menuTableView, didSelectRowAt: IndexPath(row: 0, section: 1))
                }, afterDelay: 0.0)
            }, afterDelay: 0.0)
            
            ThreadsUtility.execute({
                MainNavigator.sharedInstance.closeReversedMenu()
            }, afterDelay: 0.15)
            return
        case MenuItem.ContactUs.intValue():
            MainNavigator.sharedInstance.closeMenu()
            
            ThreadsUtility.execute({
                ContactUsViewController.showInController(MainNavigator.sharedInstance.getCenteralPanelViewController())
            }, afterDelay: 0.2)
        case MenuItem.Signin.intValue():
            ActivityProvidersDataCenter.sharedInstance.clearDataForLogout()
            if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
                UserIdentificationDataCenter.sharedInstance.logout()
                self.goToMenuItem(.Home)
                self.reloadMenuItems()
            }else{
                let storyboard = UIStoryboard(name: "UserIdentification", bundle: nil)
                let signInViewController = storyboard.instantiateViewController(withIdentifier: "SignInViewController")
                self.selectedMenuItem = .Signin
                MainNavigator.sharedInstance.changeCenterPanel(to: signInViewController)
            }
        case MenuItem.MyAccount.intValue():
            self.isSubMenuItemIsOpened = true
            self.reloadMenuItems()
        default:
            break
        }
        
        self.isCenteralPanelContainsSubMenuItem = false
//        self.isChangingLanguage = false
        
        ThreadsUtility.execute({
            if(row != MenuItem.MyAccount.intValue() && row != MenuItem.Language.intValue()){
                MainNavigator.sharedInstance.closeMenu()
            }
        }, afterDelay: 0.05)
    }
}

extension MenuViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        if(operationID == .LoadSpecialOffers || operationID == .LoadImageGeneralOperationID){
            self.reloadOffersData()
        }
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {

    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}
