//
//  MenuHeaderTableViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/20/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class MenuHeaderTableViewCell: MenuTableViewCell {
    @IBOutlet weak var backArrowImageView : UIImageView!
    weak var delegate : MenuViewController?
    
    override func awakeFromNib() {
        backArrowImageView.image = #imageLiteral(resourceName: "backIcon").withRenderingMode(.alwaysTemplate)
        backArrowImageView.tintColor = .white
    }
    
    @IBAction func goBack(_ sender: AnyObject){
        delegate?.dismissSubMenu()
    }
}

