//
//  OfferCollectionViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/10/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class OfferCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image : UIImageView!
    @IBOutlet weak var detailsContainer : UIView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!
    @IBOutlet weak var providerLabel : UILabel!
    
    override func awakeFromNib() {
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
    }
    
    override func prepareForReuse() {

    }
}

