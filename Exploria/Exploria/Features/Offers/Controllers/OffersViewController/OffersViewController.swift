//
//  OffersViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/10/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

class OffersViewController: UIViewController {
    
    @IBOutlet weak var offersCollectionView : UICollectionView!
    @IBOutlet weak var pageIndicator : UIPageControl!
    var offers : [Offer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MainNavigator.sharedInstance.setScreenTitle(LocalizationManager.sharedInstance.getTranslationForKey("Offers")!)
        
        offers = OffersDataCenter.sharedInstance.offers
        self.pageIndicator.numberOfPages = offers.count
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.view.semanticContentAttribute = .forceRightToLeft
            offers.reverse()
            self.scrollToTheEndOfCollectionView()
        }
        
        OffersDataCenter.sharedInstance.loadOffersWithDelegate(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    func scrollToTheEndOfCollectionView(){
        DispatchQueue.main.async {
            if(self.offers.count > 0){
                let index = IndexPath(row: (self.offers.count - 1), section: 0)
                self.offersCollectionView.scrollToItem(at: index, at: UICollectionViewScrollPosition.right, animated: false)
            }
        }
    }

}

extension OffersViewController : UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OfferCollectionViewCell", for: indexPath) as! OfferCollectionViewCell
        
        let offer = offers[indexPath.row]
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            cell.titleLabel.text = offer.nameEn
            cell.descriptionLabel.text = offer.descriptionEn
            cell.providerLabel.text = offer.activityProviderNameEn
        }else{
            cell.titleLabel.text = offer.nameAr
            cell.descriptionLabel.text = offer.descriptionAr
            cell.providerLabel.text = offer.activityProviderNameAr
            cell.semanticContentAttribute = .forceRightToLeft
            cell.detailsContainer.semanticContentAttribute = .forceRightToLeft
        }
        
        cell.priceLabel.text = "\(offer.price) \(LocalizationManager.sharedInstance.getTranslationForKey("KD") ?? "")"
        
        if(offer.image == nil){
            offer.loadImageWithDelegate(self)
        }else{
            cell.image.image = offer.image
            cell.image.backgroundColor = .white
            cell.image.contentMode = .scaleAspectFill
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let providerDetailsViewController = storyboard.instantiateViewController(withIdentifier: "ProviderDetailsViewController") as! ProviderDetailsViewController
        let offer = offers[indexPath.row]
        providerDetailsViewController.activityProviderID = offer.activityProviderID
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: providerDetailsViewController)
    }
}

extension OffersViewController :  UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageIndicator.currentPage = indexPath.row
    }
}

extension OffersViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        if(operationID == .LoadOffers){
            offers = OffersDataCenter.sharedInstance.offers
            self.pageIndicator.numberOfPages = offers.count
            
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                offers.reverse()
                self.scrollToTheEndOfCollectionView()
            }
        }
        self.offersCollectionView.reloadData()
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

