//
//  SignUpViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/20/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var scrollViewContentView : UIView!
    @IBOutlet weak var introMessageLabel : UILabel!
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var nameTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    @IBOutlet weak var confirmPasswordTextField : UITextField!
    @IBOutlet weak var passwordContainerView : UIView!
    @IBOutlet weak var mobileNumberTextField : UITextField!
    @IBOutlet weak var mobileNumberPrefixContainerView : UIView!
    @IBOutlet weak var mobileNumberPrefixLabel : UILabel!
    @IBOutlet weak var mobileNumberPrefixImageView : UIImageView!
    @IBOutlet weak var dateOfBirthTextField : UITextField!
    @IBOutlet weak var dateOfBirthArrowImageView : UIImageView!
    @IBOutlet weak var genderTextField : UITextField!
    @IBOutlet weak var addressTextField : UITextField!
    @IBOutlet weak var signUpButton : UIButton!
    @IBOutlet weak var orLabel : UILabel!
    @IBOutlet weak var haveAccountButton : UIButton!
    @IBOutlet weak var keypadHandleViewHeightConstraint : NSLayoutConstraint!
    
    fileprivate var genderPickerView : UIPickerView!
    fileprivate var dateOfBirthPickerView : UIDatePicker!
    fileprivate var genders : [String] = []
    fileprivate var selectedGenderIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signUpButton.clipsToBounds = true
        signUpButton.layer.cornerRadius = 10
        
        haveAccountButton.clipsToBounds = true
        haveAccountButton.layer.cornerRadius = 10
        
        introMessageLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("SignupIntro")
        emailTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupEmailPlaceHolder")
        nameTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupNamePlaceHolder")
        passwordTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupPasswordPlaceHolder")
        confirmPasswordTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupPasswordConfirmationPlaceHolder")
        mobileNumberTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupMobilePlaceHolder")
        dateOfBirthTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupDateOfBirthPlaceHolder")
        genderTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupGenderPlaceHolder")
        addressTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupAddressPlaceHolder")
        
        orLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Or")
        haveAccountButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("AlreadyHaveAnAccount"), for: .normal)
        signUpButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("SingUp"), for: .normal)
        
        MainNavigator.sharedInstance.setScreenTitle(LocalizationManager.sharedInstance.getTranslationForKey("UserSignUp")!)
        
        MainNavigator.sharedInstance.delegate = self
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(true)
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.scrollViewContentView.semanticContentAttribute = .forceRightToLeft
            self.mobileNumberPrefixContainerView.semanticContentAttribute = .forceRightToLeft
            self.passwordContainerView.semanticContentAttribute = .forceRightToLeft
            self.introMessageLabel.textAlignment = .right
            self.emailTextField.textAlignment = .right
            self.nameTextField.textAlignment = .right
            self.passwordTextField.textAlignment = .right
            self.confirmPasswordTextField.textAlignment = .right
            self.mobileNumberTextField.textAlignment = .right
            self.addressTextField.textAlignment = .right
        }
        
        UIUtilities.notifyMeWhenKeyPadWillShow(target: self, selector: #selector(updateViewScroll))
        UIUtilities.notifyMeWhenKeyPadDidChangeFrame(target: self, selector: #selector(updateViewScroll))
        UIUtilities.notifyMeWhenKeyPadWillHide(target: self, selector: #selector(updateViewScroll))
        
        UIUtilities.addTapGestureToView(self.view, withTarget: self, andSelector: #selector(dismissKeypad), andCanCancelTouchesInTheView: false)
        
        UIUtilities.rotateView90DegreeClockWise(self.dateOfBirthArrowImageView)
        
        self.mobileNumberPrefixLabel.text = CountriesDataCenter.sharedInstance.selectedCountry?.prefix
        if(CountriesDataCenter.sharedInstance.selectedCountry?.countryImage != nil){
            self.mobileNumberPrefixImageView.image = CountriesDataCenter.sharedInstance.selectedCountry?.countryImage
        }else{
            CountriesDataCenter.sharedInstance.selectedCountry?.loadImageWithDelegate(self)
        }
        
        genders.append(LocalizationManager.sharedInstance.getTranslationForKey("Male")!)
        genders.append(LocalizationManager.sharedInstance.getTranslationForKey("Female")!)
        
        self.setupGenderPicker()
        self.setupDateOfBirthPicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func dismissKeypad(){
        self.emailTextField.resignFirstResponder()
        self.nameTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
        self.confirmPasswordTextField.resignFirstResponder()
        self.mobileNumberTextField.resignFirstResponder()
        self.dateOfBirthTextField.resignFirstResponder()
        self.genderTextField.resignFirstResponder()
        self.addressTextField.resignFirstResponder()
    }
    
    func setupGenderPicker(){
        genderPickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 200))
        
        genderPickerView.backgroundColor = UIColor.white
        genderPickerView.showsSelectionIndicator = true
        genderPickerView.dataSource = self
        genderPickerView.delegate = self
        
        let genderPickerViewToolBar = UIToolbar()
        genderPickerViewToolBar.barStyle = .default
        genderPickerViewToolBar.isTranslucent = true
        genderPickerViewToolBar.tintColor = UIColor.black
        genderPickerViewToolBar.sizeToFit()
        
        var font : UIFont = UIFont()
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            font = UIFont(name: "Dosis-Light", size: CGFloat(18))!
        }else{
            font = UIFont(name: "DroidArabicKufi", size: CGFloat(18))!
        }
        
        let doneButton = UIBarButtonItem(title: LocalizationManager.sharedInstance.getTranslationForKey("Select")!, style: .plain, target: self, action: #selector(self.didSelectGenderPicker))
        doneButton.setTitleTextAttributes([NSFontAttributeName:font], for: .normal)
        let middleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: LocalizationManager.sharedInstance.getTranslationForKey("Cancel")!, style: .plain, target: self, action: #selector(self.dismissKeypad))
        cancelButton.setTitleTextAttributes([NSFontAttributeName:font], for: .normal)
        
        
        genderPickerViewToolBar.items = [cancelButton, middleSpace, doneButton]
        genderPickerViewToolBar.isUserInteractionEnabled = true
        
        genderTextField.inputView = genderPickerView
        genderTextField.inputAccessoryView = genderPickerViewToolBar
    }
    
    func setupDateOfBirthPicker(){
        dateOfBirthPickerView = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 200))
        
        dateOfBirthPickerView.backgroundColor = UIColor.white
        dateOfBirthPickerView.maximumDate = Date()
        dateOfBirthPickerView.datePickerMode = .date
        
        let dateOfBirthPickerViewToolBar = UIToolbar()
        dateOfBirthPickerViewToolBar.barStyle = .default
        dateOfBirthPickerViewToolBar.isTranslucent = true
        dateOfBirthPickerViewToolBar.tintColor = UIColor.black
        dateOfBirthPickerViewToolBar.sizeToFit()
        
        var font : UIFont = UIFont()
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            font = UIFont(name: "Dosis-Light", size: CGFloat(18))!
        }else{
            font = UIFont(name: "DroidArabicKufi", size: CGFloat(18))!
        }
        
        let doneButton = UIBarButtonItem(title: LocalizationManager.sharedInstance.getTranslationForKey("Select")!, style: .plain, target: self, action: #selector(self.didSelectDatePicker))
        doneButton.setTitleTextAttributes([NSFontAttributeName:font], for: .normal)
        let middleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: LocalizationManager.sharedInstance.getTranslationForKey("Cancel")!, style: .plain, target: self, action: #selector(self.dismissKeypad))
        cancelButton.setTitleTextAttributes([NSFontAttributeName:font], for: .normal)
        
        
        dateOfBirthPickerViewToolBar.items = [cancelButton, middleSpace, doneButton]
        dateOfBirthPickerViewToolBar.isUserInteractionEnabled = true
        
        dateOfBirthTextField.inputView = dateOfBirthPickerView
        dateOfBirthTextField.inputAccessoryView = dateOfBirthPickerViewToolBar
    }
    
    func updateViewScroll(notification: NSNotification){
        let userInfo = notification.userInfo!
        
        let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        keypadHandleViewHeightConstraint.constant = view.bounds.maxY - convertedKeyboardEndFrame.minY
        AnimationsUtility.animateLayoutFor(self.view, duration: Float(animationDuration), delay: 0.0, completionBlock: nil)
    }
    
    func validateSignUpForm() -> Bool{
        if(!ValidationsUtility.isStringNotEmpty(self.emailTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsRequired"))
            return false
        }else if(!ValidationsUtility.isEmailIsValid(self.emailTextField.text!, isStrict: true)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsNotCorrect"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.nameTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("NameIsRequired"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.passwordTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PasswordIsRequired"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.confirmPasswordTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PasswordConfirmationIsRequired"))
            return false
        }else if(!ValidationsUtility.isString(self.passwordTextField.text!, equalsString: self.confirmPasswordTextField.text!, withCaseSensitivityCoparison: true)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PasswordConfirmationMismatch"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.mobileNumberTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("MobileIsRequired"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.dateOfBirthTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("DateOfBirthIsRequired"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.genderTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("GenderIsRequired"))
            return false
        }
        
        return true
    }
    
    func didSelectGenderPicker(){
        self.dismissKeypad()
        self.selectedGenderIndex = genderPickerView.selectedRow(inComponent: 0)
        self.genderTextField.text = genders[genderPickerView.selectedRow(inComponent: 0)]
    }
    
    func didSelectDatePicker(){
        self.dismissKeypad()
        self.dateOfBirthTextField.text = DateUtility.convertDateToString(dateOfBirthPickerView.date, style: .withSeperators, sequence: .dayMonthYear)
    }
    
    @IBAction func signUp(_ sender: AnyObject){
        self.dismissKeypad()
        if(self.validateSignUpForm()){
            UserIdentificationDataCenter.sharedInstance.signupWithDelegate(self, name: self.nameTextField.text!, email: self.emailTextField.text!, mobile: (self.mobileNumberTextField.text!), password: self.passwordTextField.text!, birthDate: self.dateOfBirthTextField.text!, gender: selectedGenderIndex == 0 ? "Male" : "Female", address: self.addressTextField.text!)
        }
    }
    
    @IBAction func togglePasswordEnc(_ sender: AnyObject){
        self.passwordTextField.isSecureTextEntry = !self.passwordTextField.isSecureTextEntry
        self.confirmPasswordTextField.isSecureTextEntry = !self.confirmPasswordTextField.isSecureTextEntry
    }
}

extension SignUpViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        if(operationID == .LoadImageGeneralOperationID){
            self.mobileNumberPrefixImageView.image = CountriesDataCenter.sharedInstance.selectedCountry?.countryImage
        }
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if(operationID == .SignUp){
            let isError = object as! Bool
            if(!isError){
                self.didTapOnBack()
                MenuViewController.sharedInstance().reloadMenuItems()
                MenuViewController.sharedInstance().goToMenuItem(.Home)
            }else{
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("ErrorInSignUp"))
            }
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension SignUpViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.dismissKeypad()
        self.signUp(UIButton())
        return true
    }
}

extension SignUpViewController : MainNavigatorNavigationBarDelegate{
    @IBAction func didTapOnBack() {
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(false)
        MainNavigator.sharedInstance.popViewControllerInCenterPanel()
    }
    
    func didTapOnSort() {
        
    }
}

extension SignUpViewController : UIPickerViewDataSource, UIPickerViewDelegate {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabelRegular()
        pickerLabel.font = UIFont.systemFont(ofSize: 18)
        pickerLabel.awakeFromNib()
        pickerLabel.textColor = UIColor.black
        pickerLabel.textAlignment = .center
        pickerLabel.text = genders[row]
        
        return pickerLabel
    }
    
}
