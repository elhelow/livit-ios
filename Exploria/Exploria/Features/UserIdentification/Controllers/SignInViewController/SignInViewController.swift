//
//  SignInViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/14/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

class SignInViewController: UIViewController {
    
    @IBOutlet weak var scrollViewContentView : UIView!
    @IBOutlet weak var introMessageLabel : UILabel!
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    @IBOutlet weak var passwordContainerView : UIView!
    @IBOutlet weak var signInButton : UIButton!
    @IBOutlet weak var forgotPasswordButton : UIButton!
    @IBOutlet weak var orLabel : UILabel!
    @IBOutlet weak var signUpButton : UIButton!
    @IBOutlet weak var keypadHandleViewHeightConstraint : NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signInButton.clipsToBounds = true
        signInButton.layer.cornerRadius = 10
        
        signUpButton.clipsToBounds = true
        signUpButton.layer.cornerRadius = 10
        
        forgotPasswordButton.clipsToBounds = true
        forgotPasswordButton.layer.cornerRadius = 10
        
        introMessageLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("LoginIntro")
        emailTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("LoginEmailPlaceHolder")
        passwordTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("LoginPasswordPlaceHolder")
        signInButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("SingIn"), for: .normal)
        forgotPasswordButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("ForgotPassword"), for: .normal)
        orLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Or")
        signUpButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("SingUp"), for: .normal)
        
        forgotPasswordButton.layer.borderWidth = 1.5
        forgotPasswordButton.layer.borderColor = UIColor.black.cgColor
        
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.scrollViewContentView.semanticContentAttribute = .forceRightToLeft
            self.passwordContainerView.semanticContentAttribute = .forceRightToLeft
            self.introMessageLabel.textAlignment = .right
            self.emailTextField.textAlignment = .right
            self.passwordTextField.textAlignment = .right
        }
        
        UIUtilities.notifyMeWhenKeyPadWillShow(target: self, selector: #selector(updateViewScroll))
        UIUtilities.notifyMeWhenKeyPadDidChangeFrame(target: self, selector: #selector(updateViewScroll))
        UIUtilities.notifyMeWhenKeyPadWillHide(target: self, selector: #selector(updateViewScroll))
        
        UIUtilities.addTapGestureToView(self.view, withTarget: self, andSelector: #selector(dismissKeypad), andCanCancelTouchesInTheView: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        MainNavigator.sharedInstance.setScreenTitle(LocalizationManager.sharedInstance.getTranslationForKey("UserSignIn")!)        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func dismissKeypad(){
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    func updateViewScroll(notification: NSNotification){
        let userInfo = notification.userInfo!
        
        let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        keypadHandleViewHeightConstraint.constant = view.bounds.maxY - convertedKeyboardEndFrame.minY
        AnimationsUtility.animateLayoutFor(self.view, duration: Float(animationDuration), delay: 0.0, completionBlock: nil)
    }
    
    func validateLoginForm() -> Bool{
        if(!ValidationsUtility.isStringNotEmpty(self.emailTextField.text!)){
           AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsRequired"))
            return false
        }else if(!ValidationsUtility.isEmailIsValid(self.emailTextField.text!, isStrict: true)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsNotCorrect"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.passwordTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PasswordIsRequired"))
            return false
        }
        
        return true
    }
    
    @IBAction func signIn(_ sender: AnyObject){
        self.dismissKeypad()
        if(self.validateLoginForm()){
            UserIdentificationDataCenter.sharedInstance.loginWithDelegate(self, email: self.emailTextField.text!, password: self.passwordTextField.text!)
        }
    }
    
    
    @IBAction func signUp(_ sender: AnyObject){
        let storyboard = UIStoryboard(name: "UserIdentification", bundle: nil)
        let signUpViewController = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: signUpViewController)
    }
    
    @IBAction func togglePasswordEnc(_ sender: AnyObject){
        self.passwordTextField.isSecureTextEntry = !self.passwordTextField.isSecureTextEntry
    }
    
}

extension SignInViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if(operationID == .SignIn){
            let isError = object as! Bool
            if(!isError){
                MenuViewController.sharedInstance().reloadMenuItems()
                MenuViewController.sharedInstance().goToMenuItem(.Home)
            }else{
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("ErrorInLogIn"))
            }
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension SignInViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.dismissKeypad()
        self.signIn(UIButton())
        return true
    }
}
