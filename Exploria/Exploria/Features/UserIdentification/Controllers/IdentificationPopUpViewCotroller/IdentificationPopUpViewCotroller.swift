//
//  IdentificationPopUpViewCotroller.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/19/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation
import UIKit
import CCMPopup

class IdentificationPopUpViewCotroller: UIViewController {
    
    @IBOutlet weak var scrollViewContentView : UIView!
    @IBOutlet weak var keypadHandleViewHeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var headerLabel : UILabel!
    @IBOutlet weak var newUserHeaderLabel : UILabel!
    @IBOutlet weak var existedUserHeaderLabel : UILabel!
    
    @IBOutlet weak var signUpEmailTextField : UITextField!
    @IBOutlet weak var signUpNameTextField : UITextField!
    @IBOutlet weak var signUpPasswordTextField : UITextField!
    @IBOutlet weak var signUpConfirmPasswordTextField : UITextField!
    @IBOutlet weak var signUpPasswordContainerView : UIView!
    
    @IBOutlet weak var signUpButton : UIButton!
    @IBOutlet weak var orLabel : UILabel!

    @IBOutlet weak var loginInfoContainerView : UIView!
    @IBOutlet weak var loginEmailTextField : UITextField!
    @IBOutlet weak var loginPasswordTextField : UITextField!
    @IBOutlet weak var loginPasswordContainerView : UIView!
    
    @IBOutlet weak var signInButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        signUpButton.clipsToBounds = true
        signUpButton.layer.cornerRadius = 10
        
        signInButton.clipsToBounds = true
        signInButton.layer.cornerRadius = 10
        
        headerLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("IdentificationPopupHeader")
        newUserHeaderLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("CreateNewUser")
        existedUserHeaderLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("AlreadyHaveAnAccount")
        
        signUpEmailTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupEmailPlaceHolder")
        signUpNameTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupNamePlaceHolder")
        signUpPasswordTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupPasswordPlaceHolder")
        signUpConfirmPasswordTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SignupPasswordConfirmationPlaceHolder")
        
        loginEmailTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("LoginEmailPlaceHolder")
        loginPasswordTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("LoginPasswordPlaceHolder")
        
        orLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Or")
        
        signUpButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("SingUp"), for: .normal)
        signInButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("SingIn"), for: .normal)
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.scrollViewContentView.semanticContentAttribute = .forceRightToLeft
            self.signUpPasswordContainerView.semanticContentAttribute = .forceRightToLeft
            self.loginPasswordContainerView.semanticContentAttribute = .forceRightToLeft
            self.headerLabel.textAlignment = .right
            self.signUpEmailTextField.textAlignment = .right
            self.signUpNameTextField.textAlignment = .right
            self.signUpPasswordTextField.textAlignment = .right
            self.signUpConfirmPasswordTextField.textAlignment = .right
            self.loginEmailTextField.textAlignment = .right
            self.loginPasswordTextField.textAlignment = .right
        }
        
        UIUtilities.notifyMeWhenKeyPadWillShow(target: self, selector: #selector(updateViewScroll))
        UIUtilities.notifyMeWhenKeyPadDidChangeFrame(target: self, selector: #selector(updateViewScroll))
        UIUtilities.notifyMeWhenKeyPadWillHide(target: self, selector: #selector(updateViewScroll))
        
        UIUtilities.addTapGestureToView(self.view, withTarget: self, andSelector: #selector(dismissKeypad), andCanCancelTouchesInTheView: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    class func showInController(_ controller: UIViewController){
        
        let storyboard = UIStoryboard(name: "UserIdentification", bundle: nil)
        let popupViewController = storyboard.instantiateViewController(withIdentifier: "IdentificationPopUpViewCotroller") as! IdentificationPopUpViewCotroller
        popupViewController.view.alpha = 1.0
        
        let popup = CCMPopupTransitioning.sharedInstance()
        popup?.destinationBounds = CGRect(x: 0, y: 0, width: controller.view.frame.width - 50, height: controller.view.frame.height - 80)
        popup?.backgroundBlurRadius = 0.7
        popup?.backgroundViewAlpha = 0.7
        popup?.backgroundViewColor = UIColor.black
        popup?.dismissableByTouchingBackground = true
        popup?.presentedController = popupViewController
        popup?.presentingController = controller
        
        controller.present(popupViewController, animated: true, completion: nil)
    }
    
    
    func dismissKeypad(){
        self.signUpEmailTextField.resignFirstResponder()
        self.signUpNameTextField.resignFirstResponder()
        self.signUpPasswordTextField.resignFirstResponder()
        self.signUpConfirmPasswordTextField.resignFirstResponder()
        self.loginEmailTextField.resignFirstResponder()
        self.loginPasswordTextField.resignFirstResponder()
    }
    
    func updateViewScroll(notification: NSNotification){
        let userInfo = notification.userInfo!
        
        let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        keypadHandleViewHeightConstraint.constant = self.view.bounds.maxY - convertedKeyboardEndFrame.minY
        if(keypadHandleViewHeightConstraint.constant < 0){
            keypadHandleViewHeightConstraint.constant = 0
        }
        AnimationsUtility.animateLayoutFor(self.view, duration: Float(animationDuration), delay: 0.0, completionBlock: nil)
    }
    
    func validateSignUpForm() -> Bool{
        if(!ValidationsUtility.isStringNotEmpty(self.signUpEmailTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsRequired"))
            return false
        }else if(!ValidationsUtility.isEmailIsValid(self.signUpEmailTextField.text!, isStrict: true)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsNotCorrect"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.signUpNameTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("NameIsRequired"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.signUpPasswordTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PasswordIsRequired"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.signUpConfirmPasswordTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PasswordConfirmationIsRequired"))
            return false
        }else if(!ValidationsUtility.isString(self.signUpPasswordTextField.text!, equalsString: self.signUpConfirmPasswordTextField.text!, withCaseSensitivityCoparison: true)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PasswordConfirmationMismatch"))
            return false
        }
        
        return true
    }
    
    func validateLoginForm() -> Bool{
        if(!ValidationsUtility.isStringNotEmpty(self.loginEmailTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsRequired"))
            return false
        }else if(!ValidationsUtility.isEmailIsValid(self.loginEmailTextField.text!, isStrict: true)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsNotCorrect"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.loginPasswordTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PasswordIsRequired"))
            return false
        }
        
        return true
    }
    
    @IBAction func signUp(_ sender: AnyObject){
        self.dismissKeypad()
        if(self.validateSignUpForm()){
            UserIdentificationDataCenter.sharedInstance.signupWithDelegate(self, name: self.signUpNameTextField.text!, email: self.signUpEmailTextField.text!, mobile: "", password: self.signUpPasswordTextField.text!, birthDate: "", gender: "", address: "")
        }
    }
    
    @IBAction func signIn(_ sender: AnyObject){
        self.dismissKeypad()
        if(self.validateLoginForm()){
            UserIdentificationDataCenter.sharedInstance.loginWithDelegate(self, email: self.loginEmailTextField.text!, password: self.loginPasswordTextField.text!)
        }
    }
    
    @IBAction func toggleSignUpPasswordEnc(_ sender: AnyObject){
        self.signUpPasswordTextField.isSecureTextEntry = !self.signUpPasswordTextField.isSecureTextEntry
        self.signUpConfirmPasswordTextField.isSecureTextEntry = !self.signUpConfirmPasswordTextField.isSecureTextEntry
    }
    
    @IBAction func toggleSignInPasswordEnc(_ sender: AnyObject){
        self.loginPasswordTextField.isSecureTextEntry = !self.loginPasswordTextField.isSecureTextEntry
    }
}

extension IdentificationPopUpViewCotroller : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if(operationID == .SignUp){
            let isError = object as! Bool
            if(!isError){
                MenuViewController.sharedInstance().reloadMenuItems()
                self.dismissAnimated()
            }else{
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("ErrorInSignUp"))
            }
        }else if(operationID == .SignIn){
            let isError = object as! Bool
            if(!isError){
                MenuViewController.sharedInstance().reloadMenuItems()
                self.dismissAnimated()
            }else{
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("ErrorInLogIn"))
            }
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension IdentificationPopUpViewCotroller : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.dismissKeypad()
        if(textField == loginEmailTextField || textField == loginPasswordTextField){
            self.signIn(UIButton())
        }else{
            self.signUp(UIButton())
        }
        return true
    }
}
