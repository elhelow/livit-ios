//
//  BookCourseCalendarViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/14/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation
import FSCalendar

protocol BookCourseCalendarViewControllerDelegate : NSObjectProtocol {
    func didSelectDate(_ date : CourseBookingDate, selectedTime: CourseBookingTime, forLesson : CourseBookingScheduledLesson)
}

class BookCourseCalendarViewController : UIViewController{
    @IBOutlet weak var calendar : FSCalendar!
    @IBOutlet weak var selectButton : UIButton!
    @IBOutlet weak var availableTimesTableView : UITableView!
    
    
    var courseBookingLesson : CourseBookingScheduledLesson?
    var selectedTime : CourseBookingTime?
    var selectedDate : CourseBookingDate?
    
    
    weak var delegate : BookCourseCalendarViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectButton.clipsToBounds = true
        selectButton.layer.cornerRadius = 10
        
        calendar.delegate = self
        calendar.dataSource = self
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.availableTimesTableView.semanticContentAttribute = .forceRightToLeft
        }
        
        self.selectButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Select"), for: .normal)
        
        if(selectedDate != nil && selectedTime != nil){
            self.calendar.select(selectedDate?.date, scrollToDate: true)
            self.availableTimesTableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    @IBAction func selectDateAndTime(_ sender: AnyObject){
        if(selectedTime != nil && selectedDate != nil){
            self.delegate?.didSelectDate(selectedDate!, selectedTime: selectedTime!, forLesson: courseBookingLesson!)
            self.dismissAnimated()
        }else{
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PleaseSelectedDateAndTime"))
        }
    }
    
}

extension BookCourseCalendarViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.selectedDate != nil){
            return (self.selectedDate?.times.count)!
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookCourseTimeTableViewCell") as! BookCourseTimeTableViewCell
        
        let timeDetails = (self.selectedDate?.times[indexPath.row])!
        
        cell.time.text = timeDetails.startTime + " - " + timeDetails.endTime
        cell.price.text = "\(timeDetails.price) " + LocalizationManager.sharedInstance.getTranslationForKey("KD")!
        
        
        if(indexPath.row == (self.selectedDate?.times.count)!-1){
            cell.separator.isHidden = true
        }else{
            cell.separator.isHidden = false
        }
        
        if(timeDetails == selectedTime){
            cell.selectionImage.isHidden = false
        }else{
            cell.selectionImage.isHidden = true
        }
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            cell.contentView.semanticContentAttribute = .forceRightToLeft
            cell.time.textAlignment = .right
            cell.price.textAlignment = .right
        }
        
        if(timeDetails.isTaken){
            cell.backgroundColor = .lightGray
        }else{
            cell.backgroundColor = .white
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let timeDetails = (self.selectedDate?.times[indexPath.row])!
        if(timeDetails.isTaken){
            return
        }
        self.selectedTime = timeDetails
        self.availableTimesTableView.reloadData()
    }
}


extension BookCourseCalendarViewController: FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        selectedTime = nil
        
        for courseBookingDate in (courseBookingLesson?.dates)! {
            if(NSCalendar.current.compare(self.calendar.selectedDate!, to: courseBookingDate.date,
                                          toGranularity: .day) == .orderedSame){
                selectedDate = courseBookingDate
            }
        }
        
        self.availableTimesTableView.reloadData()
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        for courseBookingDate in (courseBookingLesson?.dates)! {
            if(NSCalendar.current.compare(date, to: courseBookingDate.date,
                                          toGranularity: .day) == .orderedSame){
                return true
            }
        }
        
        return false
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        
        for courseBookingDate in (courseBookingLesson?.dates)! {
            if(NSCalendar.current.compare(date, to: courseBookingDate.date,
                                          toGranularity: .day) == .orderedSame){
                return UIColor.clear
            }
        }
        
        return UIColor.lightGray
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        for courseBookingDate in (courseBookingLesson?.dates)! {
            if(NSCalendar.current.compare(date, to: courseBookingDate.date,
                                          toGranularity: .day) == .orderedSame){
                return ColorsUtility.colorWithHexString("#ffc107")
            }
        }
        return UIColor.black
    }
}
