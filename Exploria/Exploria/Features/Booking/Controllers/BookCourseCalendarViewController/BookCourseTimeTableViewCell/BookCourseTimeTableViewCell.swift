//
//  BookCourseTimeTableViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/14/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

class BookCourseTimeTableViewCell: UITableViewCell {
    @IBOutlet weak var time : UILabel!
    @IBOutlet weak var price : UILabel!
    @IBOutlet weak var selectionImage : UIImageView!
    
    @IBOutlet weak var separator : UIView!
    
    override func awakeFromNib() {
        
    }
    
    override func prepareForReuse() {
        
    }
}
