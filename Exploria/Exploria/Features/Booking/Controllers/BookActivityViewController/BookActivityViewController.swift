//
//  BookActivityViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/26/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit
import CCMPopup

enum PaymentMethod {
    case Deposit
    case FullPayment
}

class BookActivityViewController: UIViewController {
    
    @IBOutlet weak var pageScrollView : UIScrollView!
    @IBOutlet weak var pageScrollViewContentView : UIView!
    
    @IBOutlet weak var introLabel : UILabel!

    @IBOutlet weak var selectionContainerView : UIView!
    @IBOutlet weak var playerSelectionContainerView : UIView!
    @IBOutlet weak var playerSelectionTextField : UITextField!
    @IBOutlet weak var playerSelectionArrow : UIImageView!
    @IBOutlet weak var dateSelectionContainerView : UIView!
    @IBOutlet weak var dateSelectionTextField : UITextField!
    @IBOutlet weak var dateSelectionArrow : UIImageView!
    
    @IBOutlet weak var pereferedTimeLabel : UILabel!
    @IBOutlet weak var preferedTimeContainerView : UIView!
    @IBOutlet weak var preferedTimeContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var preferedTimeScrollView : UIScrollView!
    @IBOutlet weak var preferedTimeScrollViewContentView : UIView!
    @IBOutlet weak var preferedTimeSample : UIButton!
    
    @IBOutlet weak var additionalServicesContainerView : UIView!
    @IBOutlet weak var additionalServicesContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var additionalServicesPerPlayerContainerView : UIView!
    @IBOutlet weak var additionalServicesSampleContainerView : UIView!
    @IBOutlet weak var additionalServicesSelectSampleContainerView : UIView!
    @IBOutlet weak var additionalServicesSelectSampleTextField : UITextField!
    @IBOutlet weak var additionalServicesSampleHeaderLabel : UILabel!
    
    @IBOutlet weak var bookingTypeContainerView : UIView!
    @IBOutlet weak var bookingTypeContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var singleBookingLabel : UILabel!
    @IBOutlet weak var singleBookingUpperBorder : UIImageView!
    @IBOutlet weak var singleBookingBottomBorder : UIImageView!
    @IBOutlet weak var singleBookingLeftBorder : UIImageView!
    @IBOutlet weak var groupBookingLabel : UILabel!
    @IBOutlet weak var groupBookingUpperBorder : UIImageView!
    @IBOutlet weak var groupBookingBottomBorder : UIImageView!
    @IBOutlet weak var groupBookingRightBorder : UIImageView!
    
    
    @IBOutlet weak var questionaireContainerView : UIView!
    @IBOutlet weak var questionaireContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var questionaireIntroLabel : UILabel!
    @IBOutlet weak var questionaireButton : UIButton!
    
    @IBOutlet weak var paymentContainerView : UIView!
    @IBOutlet weak var knetContainerView : UIView!
    @IBOutlet weak var knetLabel : UILabel!
    @IBOutlet weak var knetLogoImage : UIImageView!
    
    @IBOutlet weak var paymentInformationContainerView : UIView!
    @IBOutlet weak var paymentInformationContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var depositeInformationContainerView : UIView!
    @IBOutlet weak var depositeInformationContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var depositeHeaderLabel : UILabel!
    @IBOutlet weak var depositeValueTextField : UITextField!
    @IBOutlet weak var depositeRadioButton : UIButton!
    @IBOutlet weak var pricingInformationContainerView : UIView!
    @IBOutlet weak var pricingHeaderLabel : UILabel!
    @IBOutlet weak var pricingValueTextField : UITextField!
    @IBOutlet weak var pricingRadioButton : UIButton!
    
    @IBOutlet weak var termsAndConditionsContainerView : UIView!
    @IBOutlet weak var termsAndConditionsIntro : UILabel!
    @IBOutlet weak var termsAndConditionsLabel : UILabel!
    
    
    @IBOutlet weak var bookingButton : UIButton!
    
    let spacingBetweenButtons = 8.0
    let buttonWidth = 85.0
    
    let spacingBetweenServices = 8.0
    let serviceHeight = 30.0
    
    let spacingBetweenPlayers = 8.0
    
    var bookingDetails : ActivityBookingDetails = ActivityBookingDetails(data: [:])
    var preferedTimeContainerViewHeightConstraintConstantInitialValue : CGFloat = 0
    var questionaireContainerViewHeightConstraintConstantInitialPriority : Float = 0
    var depositeInformationContainerViewHeightConstraintConstantInitialValue : CGFloat = 0
    var bookingTypeContainerViewHeightConstraintInitialValue : CGFloat = 0
    var preferedTimeSampleInitialBackgroundColor : UIColor = .clear
    
    let playersPickerIdentifier = -3051212
    let additionalServicesSampleContainerViewBottomConstraintIdentifier = "-09BT78u"
    var pickers : [String : UIPickerView] = [:]
    var alreadySetupAdditionalServicesSample = false
    var uniqueIdentifierForServicesPickers = 0
    var currentActiveTextField : UITextField?
    
    
    var selectedNumberOfPlayers = 0
    var selectedActivityBookingDate : ActivityBookingDate?
    var selectedActivityTime : ActivityBookingTime?
    var selectedBookingType : BookingType = .None
    var selectedPricingType : PricingType = .None
    var pickersTextfields : [String : UITextField] = [:]
    var questionaireResults : [ActivityBookingQuestion : ActivityBookingQuestionAnswer]?
    var selectedPaymentMethod : PaymentMethod = .FullPayment
    var totalPrice = 0.0
    
    var providerID = ""
    var activityName = ""
    var referenceNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bookingButton.clipsToBounds = true
        bookingButton.layer.cornerRadius = 10
        
        questionaireButton.layer.borderWidth = 1
        questionaireButton.layer.borderColor = UIColor.black.cgColor
        
        paymentInformationContainerView.layer.borderWidth = 1
        paymentInformationContainerView.layer.borderColor = UIColor.black.cgColor
        paymentInformationContainerView.layer.cornerRadius = 5
        paymentInformationContainerView.clipsToBounds = true
        
        
        self.introLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("FilloutActivityForm")
        self.dateSelectionTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SelectDate")
        self.playerSelectionTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("SelectNumberOfPlayers")
        self.pereferedTimeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("SelectPreferedTimeWarning")
        self.singleBookingLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("SingleBooking")
        self.groupBookingLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("GroupBooking")
        self.knetLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("knet")
        self.depositeHeaderLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Deposite")
        self.pricingHeaderLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("FullPayment")
        self.termsAndConditionsIntro.text = LocalizationManager.sharedInstance.getTranslationForKey("BookingTermsAndConditionsIntro")
        self.termsAndConditionsLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("BookingTermsAndConditions")
        self.questionaireIntroLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("QuestionaireIntro")
        self.questionaireButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("StartQuestions"), for: .normal)
        self.bookingButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("ConfirmBooking"), for: .normal)
        
        preferedTimeContainerViewHeightConstraintConstantInitialValue = preferedTimeContainerViewHeightConstraint.constant
        preferedTimeContainerViewHeightConstraint.constant = 0
        
        questionaireContainerViewHeightConstraintConstantInitialPriority = questionaireContainerViewHeightConstraint.priority
        questionaireContainerViewHeightConstraint.priority = 750
        
        depositeInformationContainerViewHeightConstraintConstantInitialValue = depositeInformationContainerViewHeightConstraint.constant
        depositeInformationContainerViewHeightConstraint.constant = 0
        
        bookingTypeContainerViewHeightConstraintInitialValue = bookingTypeContainerViewHeightConstraint.constant
        bookingTypeContainerViewHeightConstraint.constant = 0
        
        additionalServicesContainerViewHeightConstraint.priority = 751
        paymentInformationContainerViewHeightConstraint.priority = 751
        
        self.preferedTimeSampleInitialBackgroundColor = preferedTimeSample.backgroundColor!
        
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.selectionContainerView.semanticContentAttribute = .forceRightToLeft
            self.playerSelectionContainerView.semanticContentAttribute = .forceRightToLeft
            self.dateSelectionContainerView.semanticContentAttribute = .forceRightToLeft
            self.preferedTimeContainerView.semanticContentAttribute = .forceRightToLeft
            self.preferedTimeScrollView.semanticContentAttribute = .forceRightToLeft
            self.preferedTimeScrollViewContentView.semanticContentAttribute = .forceRightToLeft
            self.selectionContainerView.semanticContentAttribute = .forceRightToLeft
            self.additionalServicesSelectSampleContainerView.semanticContentAttribute = .forceRightToLeft
            self.additionalServicesSampleContainerView.semanticContentAttribute = .forceRightToLeft
            self.additionalServicesPerPlayerContainerView.semanticContentAttribute = .forceRightToLeft
            self.additionalServicesContainerView.semanticContentAttribute = .forceRightToLeft
            self.bookingTypeContainerView.semanticContentAttribute = .forceRightToLeft
            self.termsAndConditionsContainerView.semanticContentAttribute = .forceRightToLeft
            self.paymentInformationContainerView.semanticContentAttribute = .forceRightToLeft
            self.depositeInformationContainerView.semanticContentAttribute = .forceRightToLeft
            self.pricingInformationContainerView.semanticContentAttribute = .forceRightToLeft
            self.knetContainerView.semanticContentAttribute = .forceRightToLeft
            
            self.introLabel.textAlignment = .right
            self.playerSelectionTextField.textAlignment = .right
            self.dateSelectionTextField.textAlignment = .right
            self.pereferedTimeLabel.textAlignment = .right
            self.additionalServicesSelectSampleTextField.textAlignment = .right
            self.additionalServicesSampleHeaderLabel.textAlignment = .right
            self.termsAndConditionsIntro.textAlignment = .right
            self.termsAndConditionsLabel.textAlignment = .right
            self.questionaireIntroLabel.textAlignment = .right
            self.knetLabel.textAlignment = .right
        }
        
        
        self.view.layoutIfNeeded()
        
        BookingDataCenter.sharedInstance.loadBookingDetailsWithDelegate(self, providerID: self.providerID)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func reloadBookingDetails(){
        self.setupPickerViewForTextField(self.playerSelectionTextField, identifier: playersPickerIdentifier)
        
        if(self.bookingDetails.bookingType == .Both || self.bookingDetails.bookingType == .Group){
            additionalServicesPerPlayerContainerView.layer.borderWidth = 1
            additionalServicesPerPlayerContainerView.layer.borderColor = UIColor.black.cgColor
        }
        
        if(self.bookingDetails.questions.count > 0){
            self.questionaireContainerViewHeightConstraint.priority = questionaireContainerViewHeightConstraintConstantInitialPriority
        }
        
        self.updatePricingInformation()
    }
    
    func updatePricingInformation(){
        self.selectedPricingType = self.bookingDetails.pricingType
        
        if(self.selectedActivityBookingDate != nil){
            if(self.bookingDetails.pricingType != .Activity){
                if(self.pickersTextfields.count > 0){
                    var isValidForOneServiceAtLeast : Bool = false
                    for pickersTextfield in pickersTextfields{
                        if(ValidationsUtility.isStringNotEmpty(pickersTextfield.value.text!)){
                            isValidForOneServiceAtLeast = true
                            break
                        }
                    }
                    if(!isValidForOneServiceAtLeast){
                        depositeInformationContainerViewHeightConstraint.constant = 0
                        paymentInformationContainerViewHeightConstraint.priority = 751
                        return
                    }
                }
            }
            
            if(bookingDetails.deposite != 0){
                self.paymentInformationContainerViewHeightConstraint.priority = 250
                self.depositeInformationContainerViewHeightConstraint.constant = depositeInformationContainerViewHeightConstraintConstantInitialValue
                
                self.depositeValueTextField.text = LocalizationManager.sharedInstance.getTranslationForKey("Minimum")! + " \(bookingDetails.deposite) " + LocalizationManager.sharedInstance.getTranslationForKey("KD")!
            }
            
            var pricingValue = 0.0
            if(self.bookingDetails.pricingType == .Activity){
                pricingValue = (self.selectedActivityBookingDate?.price)!
            }else if(self.bookingDetails.pricingType == .Service){
                for pickersTextfield in pickersTextfields{
                    if(ValidationsUtility.isStringNotEmpty(pickersTextfield.value.text!)){
                        let subService = self.bookingDetails.services[pickersTextfield.value.tag % self.bookingDetails.services.count].subServices[(pickers["\(pickersTextfield.value.tag)"]?.selectedRow(inComponent: 0))!]
                        for priceInfo in subService.prices{
                            if(priceInfo.title == self.selectedActivityBookingDate?.title){
                                pricingValue += priceInfo.price
                                break
                            }
                        }
                    }
                }
                if(self.selectedBookingType == .Single){
                    pricingValue *= Double(selectedNumberOfPlayers)
                }
            }else if(self.bookingDetails.pricingType == .Player){
                pricingValue = (self.selectedActivityBookingDate?.price)! * Double(selectedNumberOfPlayers)
            }
            
            self.paymentInformationContainerViewHeightConstraint.priority = 250
            self.pricingValueTextField.text = "\(pricingValue) " + LocalizationManager.sharedInstance.getTranslationForKey("KD")!
            
            totalPrice = pricingValue
        }
    }
    
    func setupPickerViewForTextField(_ textfield : UITextField, identifier : Int){
        let aPickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 200))
        
        aPickerView.backgroundColor = UIColor.white
        aPickerView.showsSelectionIndicator = true
        aPickerView.dataSource = self
        aPickerView.delegate = self
        aPickerView.tag = identifier
        
        let aToolBar = UIToolbar()
        aToolBar.barStyle = .default
        aToolBar.isTranslucent = true
        aToolBar.tintColor = UIColor.black
        aToolBar.sizeToFit()
        
        var font : UIFont = UIFont()
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            font = UIFont(name: "Dosis-Light", size: CGFloat(18))!
        }else{
            font = UIFont(name: "DroidArabicKufi", size: CGFloat(18))!
        }
        
        let doneButton = UIBarButtonItem(title: LocalizationManager.sharedInstance.getTranslationForKey("Select")!, style: .plain, target: self, action: #selector(self.didSelectPicker(_:)))
        doneButton.setTitleTextAttributes([NSFontAttributeName:font], for: .normal)
        doneButton.tag = identifier
        let middleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: LocalizationManager.sharedInstance.getTranslationForKey("Cancel")!, style: .plain, target: self, action: #selector(self.dismissKeypad))
        cancelButton.setTitleTextAttributes([NSFontAttributeName:font], for: .normal)
        
        
        aToolBar.items = [cancelButton, middleSpace, doneButton]
        aToolBar.isUserInteractionEnabled = true
        
        textfield.inputView = aPickerView
        textfield.inputAccessoryView = aToolBar
        
        textfield.inputView?.translatesAutoresizingMaskIntoConstraints = false

        pickers["\(identifier)"] = aPickerView
        if(identifier != playersPickerIdentifier){
            pickersTextfields["\(identifier)"] = textfield
        }
    }
    
    func dismissKeypad(){
        self.playerSelectionTextField.resignFirstResponder()
        self.dateSelectionTextField.resignFirstResponder()
        self.currentActiveTextField?.resignFirstResponder()
    }
    
    func didSelectPicker(_ sender : AnyObject){
        self.dismissKeypad()
        if(sender.tag == playersPickerIdentifier){
            selectedNumberOfPlayers = (pickers["\(playersPickerIdentifier)"]?.selectedRow(inComponent: 0))! + 1
            self.playerSelectionTextField.text = " \(selectedNumberOfPlayers) \(LocalizationManager.sharedInstance.getTranslationForKey("Player(s)")!)"
            self.showAdditionalServicesSectionWithNewNumberOfPlayers()
        }else{
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                currentActiveTextField?.text = self.bookingDetails.services[(currentActiveTextField?.tag)! % self.bookingDetails.services.count].subServices[(pickers["\(currentActiveTextField?.tag ?? -040)"]?.selectedRow(inComponent: 0))!].nameAr
            }else{
                currentActiveTextField?.text = self.bookingDetails.services[(currentActiveTextField?.tag)! % self.bookingDetails.services.count].subServices[(pickers["\(currentActiveTextField?.tag  ?? -040)"]?.selectedRow(inComponent: 0))!].nameEn
            }
        }
        
        self.updatePricingInformation()
    }
    
    func showAdditionalServicesSectionWithNewNumberOfPlayers(){
        
        if(bookingDetails.services.count > 0){
            self.setupAdditionalServicesSample(bookingDetails.services)
            
            if(bookingDetails.bookingType == .Both){
                bookingTypeContainerViewHeightConstraint.constant = bookingTypeContainerViewHeightConstraintInitialValue
                if(selectedBookingType == .None){
                    self.changeSelectionType(UIButton())
                }
            }else {
                selectedBookingType = bookingDetails.bookingType
            }
            
            self.setupNumberOfPlayers(selectedNumberOfPlayers)
            
            additionalServicesContainerViewHeightConstraint.priority = 250
            
            AnimationsUtility.animateLayoutFor(self.view, duration: 0.5, delay: 0.0) {
                
            }
        }else{
            if(bookingDetails.bookingType == .Both){
                selectedBookingType = .Single
            }else{
                selectedBookingType = bookingDetails.bookingType
            }
        }
    }
    
    func setupTimes(_ times:[ActivityBookingTime]){
        for subView in self.preferedTimeScrollViewContentView.subviews {
            if(subView.tag != 0){
                subView.removeFromSuperview()
            }
        }
        
        if(times.count != 0){
            preferedTimeContainerViewHeightConstraint.constant = preferedTimeContainerViewHeightConstraintConstantInitialValue
            self.pereferedTimeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("SelectPreferedTime")
        }else{
            self.pereferedTimeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("SelectPreferedTimeWarning")
        }
        
        var isFirstButton = true
        var buttonIndex = 0
        var lastTimeButton : UIButton?
        for time in times{
            var timeButton : UIButton = UIButton()
            if(isFirstButton){
                timeButton = self.preferedTimeSample
            }else{
                timeButton = UIUtilities.makeACopyOfView(self.preferedTimeSample) as! UIButton
                timeButton.awakeFromNib()
                self.preferedTimeScrollViewContentView.addSubview(timeButton)
            }
            
            timeButton.removeConstraints(timeButton.constraints)
            timeButton.translatesAutoresizingMaskIntoConstraints = false
            timeButton.tag = buttonIndex
            timeButton.setTitle(time.time, for: .normal)
            timeButton.addTarget(self, action: #selector(didChangeTime), for: .touchUpInside)
            timeButton.layer.borderWidth = 1
            timeButton.layer.borderColor = UIColor.lightGray.cgColor
            
            if(time.isTaken){
                timeButton.backgroundColor = .lightGray
                timeButton.setTitleColor(.white, for: .normal)
            }else{
                timeButton.backgroundColor = preferedTimeSampleInitialBackgroundColor
                timeButton.setTitleColor(.black, for: .normal)
            }

            ConstraintsUtility.addTopConstraintTo(superView: self.preferedTimeScrollViewContentView, view: timeButton, relatedView: self.preferedTimeScrollViewContentView, constant: 0, relation: .equal)
            ConstraintsUtility.addBottomConstraintTo(superView: self.preferedTimeScrollViewContentView, view: timeButton, relatedView: self.preferedTimeScrollViewContentView, constant: 0, relation: .equal)
            ConstraintsUtility.addLeadingConstraintTo(superView: self.preferedTimeScrollViewContentView, view: timeButton, relatedView: lastTimeButton == nil ? self.preferedTimeScrollViewContentView : lastTimeButton!, constant: lastTimeButton == nil ? 0 : spacingBetweenButtons + buttonWidth, relation: .equal)
            ConstraintsUtility.addWidthConstraintTo(timeButton, width: buttonWidth, relation: .equal)
            
            isFirstButton = false
            buttonIndex += 1
            lastTimeButton = timeButton
        }
        
        if(lastTimeButton != nil){
            ConstraintsUtility.addTrailingConstraintTo(superView: self.preferedTimeScrollViewContentView, view: lastTimeButton!, relatedView: self.preferedTimeScrollViewContentView, constant: 0, relation: .equal)
        }
        
        AnimationsUtility.animateLayoutFor(self.preferedTimeContainerView, duration: 0.5, delay: 0.0) {
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                if((self.preferedTimeScrollViewContentView.frame.size.width - self.preferedTimeScrollView.frame.size.width) >= 0){
                    self.preferedTimeScrollView.setContentOffset(CGPoint(x: (self.preferedTimeScrollViewContentView.frame.size.width - self.preferedTimeScrollView.frame.size.width), y:0), animated: true)
                }
            }
        }
        
        self.preferedTimeScrollView.flashScrollIndicators()
    }
    
    
    func setupAdditionalServicesSample(_ services:[ActivityBookingService]){
        if(alreadySetupAdditionalServicesSample){
           return
        }
        var isFirstService = true
        var serviceIndex = 1
        var lastServiceView : UIView?

        for aService in services{
            var serviceView : UIView = UIView()
            if(isFirstService){
                serviceView = self.additionalServicesSelectSampleContainerView
            }else{
                serviceView = UIUtilities.makeACopyOfView(self.additionalServicesSelectSampleContainerView) as! UIView
                serviceView.viewWithTag(0)?.awakeFromNib()
                self.additionalServicesSampleContainerView.addSubview(serviceView)
            }
            
            serviceView.translatesAutoresizingMaskIntoConstraints = false
            serviceView.tag = serviceIndex
            (serviceView.subviews[0] as! UITextField).delegate = self
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                (serviceView.subviews[0] as! UITextField).placeholder = aService.nameAr
            }else{
                (serviceView.subviews[0] as! UITextField).placeholder = aService.nameEn
            }
            
            self.setupPickerViewForTextField((serviceView.subviews[0] as! UITextField), identifier: uniqueIdentifierForServicesPickers)
            (serviceView.subviews[0] as! UITextField).tag = uniqueIdentifierForServicesPickers
            uniqueIdentifierForServicesPickers += 1
            
            if(isFirstService){
                isFirstService = false
                lastServiceView = serviceView
                serviceIndex += 1
                continue
            }
            
            ConstraintsUtility.addLeadingConstraintTo(superView: self.additionalServicesSampleContainerView, view: serviceView, relatedView: self.additionalServicesSampleContainerView, constant: 0, relation: .equal)
            ConstraintsUtility.addTrailingConstraintTo(superView: self.additionalServicesSampleContainerView, view: serviceView, relatedView: self.additionalServicesSampleContainerView, constant: 0, relation: .equal)
            ConstraintsUtility.addTopConstraintTo(superView: self.additionalServicesSampleContainerView, view: serviceView, relatedView: lastServiceView!, constant: spacingBetweenServices + serviceHeight, relation: .equal)
            ConstraintsUtility.addHeightConstraintTo(serviceView, height: serviceHeight, relation: .equal)
            
            isFirstService = false
            serviceIndex += 1
            lastServiceView = serviceView
        }
        
        if(lastServiceView != nil){
            ConstraintsUtility.addBottomConstraintTo(superView: self.additionalServicesSampleContainerView, view: lastServiceView!, relatedView: self.additionalServicesSampleContainerView, constant: -spacingBetweenServices*2, relation: .equal)
        }
        
        alreadySetupAdditionalServicesSample = true
    }
    
    func setupNumberOfPlayers(_ aNumberOfPlayers : Int){
        
        uniqueIdentifierForServicesPickers = self.bookingDetails.services.count
        
        var newPickersTextfields : [String : UITextField] = [:]
        for pickersTextfield in pickersTextfields {
            if(pickersTextfield.value.tag < uniqueIdentifierForServicesPickers){
                newPickersTextfields[pickersTextfield.key] = pickersTextfield.value
            }
        }
        pickersTextfields = newPickersTextfields
        
        for subView in self.additionalServicesContainerView.subviews {
            if(subView.tag != 0){
                subView.removeFromSuperview()
            }else{
                for constraint in self.additionalServicesContainerView.constraints{
                    if(constraint.identifier == additionalServicesSampleContainerViewBottomConstraintIdentifier){
                        self.additionalServicesContainerView.removeConstraint(constraint)
                    }
                }
            }
        }
        
        let numberOfPlayers = self.selectedBookingType == .Group ? aNumberOfPlayers : 1
        var isFirstPlayer = true
        var playerIndex = 0
        var lastPlayerView : UIView?
        
        self.additionalServicesSampleContainerView.layoutIfNeeded()
        (self.additionalServicesSampleContainerView.subviews[0] as! UILabel).text = "Temp"
        self.additionalServicesSampleContainerView.layoutIfNeeded()
        
        let playerHeight = Double(self.additionalServicesSampleContainerView.frame.size.height)
        for _ in 1...numberOfPlayers{
            var playerView : UIView = UIView()
            if(isFirstPlayer){
                playerView = self.additionalServicesSampleContainerView
            }else{
                playerView = UIUtilities.makeACopyOfView(self.additionalServicesSampleContainerView) as! UIView
                self.additionalServicesContainerView.addSubview(playerView)
            }
            
            playerView.translatesAutoresizingMaskIntoConstraints = false
            playerView.tag = playerIndex
            
            if(self.selectedBookingType == .Group){
                (playerView.subviews[0] as! UILabel).text = LocalizationManager.sharedInstance.getTranslationForKey("Player")! + " \(playerIndex+1)"
            }else{
                (playerView.subviews[0] as! UILabel).text = ""
            }
            
            
            for view in playerView.subviews{
                if(view.tag == 0){
                    continue
                }
                
                (view.subviews[0] as! UITextField).text = ""
            }
            
            if(isFirstPlayer){
                isFirstPlayer = false
                lastPlayerView = playerView
                playerIndex += 1
                continue
            }
            
            ConstraintsUtility.addLeadingConstraintTo(superView: self.additionalServicesContainerView, view: playerView, relatedView: self.additionalServicesContainerView, constant: 0, relation: .equal)
            ConstraintsUtility.addTrailingConstraintTo(superView: self.additionalServicesContainerView, view: playerView, relatedView: self.additionalServicesContainerView, constant: 0, relation: .equal)
            ConstraintsUtility.addTopConstraintTo(superView: self.additionalServicesContainerView, view: playerView, relatedView: lastPlayerView!, constant: spacingBetweenPlayers + playerHeight, relation: .equal)
            ConstraintsUtility.addHeightConstraintTo(playerView, height: playerHeight, relation: .equal)
            
            
            var lastView : UIView?
            for view in playerView.subviews{
                if(view.tag == 0){
                    continue
                }
                
                (view.subviews[0] as! UITextField).delegate = self
                self.setupPickerViewForTextField((view.subviews[0] as! UITextField), identifier: uniqueIdentifierForServicesPickers)
                (view.subviews[0] as! UITextField).tag = uniqueIdentifierForServicesPickers
                uniqueIdentifierForServicesPickers += 1
                
                if(view.tag == 1){
                    lastView = view
                    continue
                }
                
                if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                    (view.subviews[0] as! UITextField).textAlignment = .right
                }
                ConstraintsUtility.addLeadingConstraintTo(superView: playerView, view: view, relatedView: playerView, constant: 0, relation: .equal)
                ConstraintsUtility.addTrailingConstraintTo(superView: playerView, view: view, relatedView: playerView, constant: 0, relation: .equal)
                ConstraintsUtility.addTopConstraintTo(superView: playerView, view: view, relatedView: lastView!, constant: serviceHeight + spacingBetweenServices, relation: .equal)
                ConstraintsUtility.addHeightConstraintTo(view, height: serviceHeight, relation: .equal)
                lastView = view
            }
            
            if(lastView != nil){
                ConstraintsUtility.addBottomConstraintTo(superView: playerView, view: lastView!, relatedView: playerView, constant: -spacingBetweenServices*2, relation: .equal)
                
            }
            
            isFirstPlayer = false
            playerIndex += 1
            lastPlayerView = playerView
        }
        
        if(lastPlayerView != nil){
            let bottomConstraint = ConstraintsUtility.addBottomConstraintTo(superView: self.additionalServicesContainerView, view: lastPlayerView!, relatedView: self.additionalServicesContainerView, constant: -spacingBetweenPlayers*2, relation: .equal)
            if(numberOfPlayers == 1){
                    bottomConstraint.identifier = additionalServicesSampleContainerViewBottomConstraintIdentifier
            }
        }
        
    }

    
    @IBAction func didChangeTime(_ sender: AnyObject){
        if(self.selectedActivityBookingDate != nil){
            self.preferedTimeScrollView.flashScrollIndicators()
            if(!((self.selectedActivityBookingDate?.times[sender.tag].isTaken)!)){
                self.selectedActivityTime = self.selectedActivityBookingDate?.times[sender.tag]
                for subView in self.preferedTimeScrollViewContentView.subviews {
                    if(subView.tag == sender.tag){
                        (subView as! UIButton).backgroundColor = .black
                        (subView as! UIButton).setTitleColor(.white, for: .normal)

                    }else if(((self.selectedActivityBookingDate?.times[subView.tag].isTaken)!)){
                        (subView as! UIButton).backgroundColor = .lightGray
                        (subView as! UIButton).setTitleColor(.white, for: .normal)
                    }else{
                        (subView as! UIButton).backgroundColor = preferedTimeSampleInitialBackgroundColor
                        (subView as! UIButton).setTitleColor(.black, for: .normal)
                    }
                }
            }
        }
    }
    
    @IBAction func openTermsAndConditions(_ sender: AnyObject){
        
    }
    
    @IBAction func changePaymentMethod(_ sender: AnyObject){
        if(sender as! NSObject == depositeRadioButton){
            self.selectedPaymentMethod = .Deposit
        }else if(sender as! NSObject == pricingRadioButton){
            self.selectedPaymentMethod = .FullPayment
        }
        
        if(selectedPaymentMethod == .Deposit){
            self.depositeRadioButton.setBackgroundImage(#imageLiteral(resourceName: "selectedRB"), for: .normal)
            self.pricingRadioButton.setBackgroundImage(#imageLiteral(resourceName: "notSelectedRB"), for: .normal)
        }else if(selectedPaymentMethod == .FullPayment){
            self.pricingRadioButton.setBackgroundImage(#imageLiteral(resourceName: "selectedRB"), for: .normal)
            self.depositeRadioButton.setBackgroundImage(#imageLiteral(resourceName: "notSelectedRB"), for: .normal)
        }
    }
    
    @IBAction func changeSelectionType(_ sender: AnyObject){
        self.dismissKeypad()
        if(selectedBookingType == .None || selectedBookingType == .Group){
            singleBookingUpperBorder.backgroundColor = .black
            groupBookingBottomBorder.backgroundColor = .black
            singleBookingBottomBorder.backgroundColor = .white
            groupBookingUpperBorder.backgroundColor = .white
            
            singleBookingLeftBorder.backgroundColor = .black
            groupBookingRightBorder.backgroundColor = .white
            
            selectedBookingType = .Single
        }else{
//            if(self.bookingDetails.pricingType == .Service){
//                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("GroupBookingNotAllowed"))
//                return
//            }
            singleBookingUpperBorder.backgroundColor = .white
            groupBookingBottomBorder.backgroundColor = .white
            singleBookingBottomBorder.backgroundColor = .black
            groupBookingUpperBorder.backgroundColor = .black
            
            singleBookingLeftBorder.backgroundColor = .white
            groupBookingRightBorder.backgroundColor = .black
            
            selectedBookingType = .Group
        }
        
        self.setupNumberOfPlayers(selectedNumberOfPlayers)
        
        AnimationsUtility.animateLayoutFor(self.view, duration: 0.5, delay: 0.0) {
            
        }
        
        self.updatePricingInformation()
    }
    
    func isDataFormValid() -> Bool{
        
        if(selectedNumberOfPlayers == 0){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("NumberOfPlayerIsRequired"))
            return false
        }else if(selectedActivityBookingDate == nil){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("BookingDateIsRequired"))
            return false
        }else if(selectedActivityTime == nil){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("TimeIsRequired"))
            return false
        }else if(self.bookingDetails.pricingType == .Service){
            for pickersTextfield in pickersTextfields{
                if(!ValidationsUtility.isStringNotEmpty(pickersTextfield.value.text!)){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("AllServicesIsRequired"))
                    return false
                }
            }
        }
        
        
        return true
    }
    
    @IBAction func confirmBooking(_ sender: AnyObject){
        if(isDataFormValid()){
            if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
                self.performSegue(withIdentifier: "showConfirmationPopup", sender: nil)
            }else{
                IdentificationPopUpViewCotroller.showInController(self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showActivityCalendar") {
            let popupSegue = segue as! CCMPopupSegue
            
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.width-50
                , height: 400)
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let bookActivityCalendarViewController = popupSegue.destination as! BookActivityCalendarViewController
            bookActivityCalendarViewController.activityBookingDates = bookingDetails.dates
            if(self.selectedActivityBookingDate != nil){
                bookActivityCalendarViewController.selectedDate = self.selectedActivityBookingDate?.date
            }
            
            bookActivityCalendarViewController.delegate = self
            bookActivityCalendarViewController.view.layer.cornerRadius = 10
            bookActivityCalendarViewController.view.clipsToBounds = true
            
        }else if(segue.identifier == "showActivityQuestionaire"){
            let popupSegue = segue as! CCMPopupSegue
            
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.width-50
                , height: 400)
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let bookActivityQuestionaireViewController = popupSegue.destination as! BookActivityQuestionaireViewController
            bookActivityQuestionaireViewController.activityBookingQuestion = bookingDetails.questions
            
            if(self.questionaireResults != nil){
                bookActivityQuestionaireViewController.answers = self.questionaireResults!
            }
            
            bookActivityQuestionaireViewController.delegate = self
            bookActivityQuestionaireViewController.view.layer.cornerRadius = 10
            bookActivityQuestionaireViewController.view.clipsToBounds = true
        }else if(segue.identifier == "showConfirmationPopup"){
            let popupSegue = segue as! CCMPopupSegue
            
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.width-50
                , height: self.view.frame.height - 80)
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let confirmBookingViewController = popupSegue.destination as! ConfirmBookingViewController
            confirmBookingViewController.delegate = self
            
            confirmBookingViewController.activity = activityName
            confirmBookingViewController.numberOfPlayers = "\(selectedNumberOfPlayers)"
            confirmBookingViewController.date = (selectedActivityBookingDate?.dateAsString)!
            confirmBookingViewController.time = (selectedActivityTime?.time)!
            if(selectedPaymentMethod == .FullPayment){
                confirmBookingViewController.type = LocalizationManager.sharedInstance.getTranslationForKey("FullPayment")!
            }else if(selectedPaymentMethod == .Deposit){
                confirmBookingViewController.type = LocalizationManager.sharedInstance.getTranslationForKey("Deposite")!
            }
            confirmBookingViewController.method = LocalizationManager.sharedInstance.getTranslationForKey("knet")!
            
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                confirmBookingViewController.total = LocalizationManager.sharedInstance.getTranslationForKey("KD")! + " \(bookingDetails.deposite)"
            }else{
                confirmBookingViewController.total = "\(totalPrice) " + LocalizationManager.sharedInstance.getTranslationForKey("KD")!
            }
        }else if(segue.identifier == "showSuccessPopup"){
            let popupSegue = segue as! CCMPopupSegue
            
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.width-50
                , height: 470)
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let bookingResultViewController = popupSegue.destination as! BookingResultViewController
            bookingResultViewController.referenceNumber = self.referenceNumber
            bookingResultViewController.phoneNumber = "+965 000 000 00"
        }
    }

}

extension BookActivityViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if(operationID == .BookingActivityDetails){
            self.bookingDetails = object as! ActivityBookingDetails
            self.reloadBookingDetails()
        }else if(operationID == .SaveBookingActivityDetails){
            if let _ = object as? Bool{
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("GenericError"))
            }else{
                self.referenceNumber = object as! String
                self.performSegue(withIdentifier: "showSuccessPopup", sender: nil)
            }
            
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension BookActivityViewController : UIPickerViewDataSource, UIPickerViewDelegate {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == playersPickerIdentifier){
            return self.bookingDetails.maxNumberOfPlayers
        }else{
            return self.bookingDetails.services[pickerView.tag % self.bookingDetails.services.count].subServices.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabelRegular()
        pickerLabel.font = UIFont.systemFont(ofSize: 18)
        pickerLabel.awakeFromNib()
        pickerLabel.textColor = UIColor.black
        pickerLabel.textAlignment = .center
        
        if(pickerView.tag == playersPickerIdentifier){
            pickerLabel.text = "\(row+1) " + LocalizationManager.sharedInstance.getTranslationForKey("Player(s)")!

        }else{
            var priceValue = ""
            
            if(self.selectedActivityBookingDate != nil){
                for price in self.bookingDetails.services[pickerView.tag % self.bookingDetails.services.count].subServices[row].prices{
                    if(self.selectedActivityBookingDate?.title == price.title){
                        priceValue = "( \(price.price) " +  LocalizationManager.sharedInstance.getTranslationForKey("KD")! + " )"
                    }
                }
            }
            
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                pickerLabel.text = self.bookingDetails.services[pickerView.tag % self.bookingDetails.services.count].subServices[row].nameAr + "   " + priceValue
            }else{
                pickerLabel.text = self.bookingDetails.services[pickerView.tag % self.bookingDetails.services.count].subServices[row].nameEn + "   " + priceValue
            }
        }
        
        return pickerLabel
    }
    
}

extension BookActivityViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == dateSelectionTextField){
            self.performSegue(withIdentifier: "showActivityCalendar", sender: nil)
            return false
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentActiveTextField = textField
    }
}

extension BookActivityViewController : BookActivityCalendarViewControllerDelegate{
    func didSelectDate(_ date: ActivityBookingDate) {
        self.selectedActivityBookingDate = date
        self.selectedActivityTime = nil
        self.dateSelectionTextField.text = DateUtility.convertDateToString((self.selectedActivityBookingDate?.date)!, style: .withSeperators, sequence: .dayMonthYear)
        self.setupTimes((self.selectedActivityBookingDate?.times)!)
        self.updatePricingInformation()
    }
}

extension BookActivityViewController : BookActivityQuestionaireViewControllerDelegate{
    func didFinishQuestionaireWithResults(_ questionaireResults: [ActivityBookingQuestion : ActivityBookingQuestionAnswer]) {
        self.questionaireResults = questionaireResults
        self.questionaireButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("QuestionaireCompleted"), for: .normal)
    }
}

extension BookActivityViewController : ConfirmBookingViewControllerDelegate{
    func didApproveBooking() {
        let bookingQuery = BookingQuery()
        
        bookingQuery.activityName = self.activityName
        bookingQuery.activityPrice = "\(self.bookingDetails.activityPrice)"
        bookingQuery.activityProviderID = self.providerID
        bookingQuery.dateAsString = (selectedActivityBookingDate?.dateAsString)!
        bookingQuery.depositeValue = "\(self.bookingDetails.deposite)"
        bookingQuery.numberOfPlayers = "\(selectedNumberOfPlayers)"
        bookingQuery.paymentType = selectedPaymentMethod == .FullPayment ? "Full Payment" : "Deposite"
        bookingQuery.pricingType = bookingDetails.pricingTypeAsString
        
        if(questionaireResults != nil){
            for questionaireItem in questionaireResults!{
                bookingQuery.questions.append(questionaireItem.value.id)
            }
        }
        
        bookingQuery.dateAsID = (selectedActivityBookingDate?.id)!
        bookingQuery.dateTitle = ""
        bookingQuery.timeAsID = (selectedActivityTime?.id)!
        bookingQuery.timeAsString = (selectedActivityTime?.time)!
        bookingQuery.totalPayment = "\(totalPrice)"
        
        
        bookingQuery.services = [[:]]
        for pickersTextfield in pickersTextfields{
            if(ValidationsUtility.isStringNotEmpty(pickersTextfield.value.text!)){
                let service = self.bookingDetails.services[pickersTextfield.value.tag % self.bookingDetails.services.count]
                let subService = service.subServices[(pickers["\(pickersTextfield.value.tag)"]?.selectedRow(inComponent: 0))!]
                bookingQuery.services.append(["selection":subService.id,"package_id":service.id])
            }
        }
        
        ThreadsUtility.execute({
            BookingDataCenter.sharedInstance.saveActivityBookingDetailsWithDelegate(self, bookingQuery: bookingQuery)
        }, afterDelay: 0.5)
        
    }
}
