//
//  BookActivityQuestionaireAnswerTableViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 12/1/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

protocol BookActivityQuestionaireAnswerTableViewCellDelegate : NSObjectProtocol {
    func didSelectAnswerForQuestion(_ question : ActivityBookingQuestion, answer:ActivityBookingQuestionAnswer)
}

class BookActivityQuestionaireAnswerTableViewCell: UITableViewCell {
    @IBOutlet weak var answerTitle : UILabel!
    @IBOutlet weak var answerRadioButton : UIButton!
    
    var question : ActivityBookingQuestion?
    var answer : ActivityBookingQuestionAnswer?
    
    weak var delegate : BookActivityQuestionaireAnswerTableViewCellDelegate?
    
    override func awakeFromNib() {
        
    }
    
    override func prepareForReuse() {

    }
    
    @IBAction func selectAnswer(_ sender: AnyObject){
        delegate?.didSelectAnswerForQuestion(self.question!, answer: self.answer!)
    }
}

