//
//  BookActivityQuestionaireViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 12/1/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

protocol BookActivityQuestionaireViewControllerDelegate : NSObjectProtocol {
    func didFinishQuestionaireWithResults(_ questionaireResults : [ActivityBookingQuestion : ActivityBookingQuestionAnswer])
}

class BookActivityQuestionaireViewController : UIViewController{

    @IBOutlet weak var questionsTableView : UITableView!
    @IBOutlet weak var actionButtonsContainerView : UIView!
    @IBOutlet weak var previouseButton : UIButton!
    @IBOutlet weak var nextButton : UIButton!

    var currentQuestionIndex = 0
    var activityBookingQuestion : [ActivityBookingQuestion] = []
    var previouseButtonInitialBackground : UIColor = .clear
    var answers : [ActivityBookingQuestion : ActivityBookingQuestionAnswer] = [:]
    
    weak var delegate : BookActivityQuestionaireViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.questionsTableView.estimatedRowHeight = 44
        self.questionsTableView.rowHeight = UITableViewAutomaticDimension
        
        self.previouseButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Previous"), for: .normal)
        
        previouseButtonInitialBackground = previouseButton.backgroundColor!
        
        self.validateActionButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func validateActionButtons(){
        if(currentQuestionIndex+1 == activityBookingQuestion.count){
            self.nextButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Finish"), for: .normal)
        }else{
            self.nextButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Next"), for: .normal)
        }
        
        self.previouseButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Previouse"), for: .normal)
        
        if(currentQuestionIndex-1 < 0){
            self.previouseButton.isEnabled = false
            self.previouseButton.backgroundColor = UIColor.lightGray
        }else{
            self.previouseButton.isEnabled = true
            self.previouseButton.backgroundColor = previouseButtonInitialBackground
        }
    }
    
    @IBAction func nextQuestion(_ sender: AnyObject){
        if(currentQuestionIndex+1 == activityBookingQuestion.count){
            self.delegate?.didFinishQuestionaireWithResults(self.answers)
            self.dismissAnimated()
        }else{
            currentQuestionIndex += 1
            validateActionButtons()
            self.questionsTableView.reloadData()
        }
    }
    
    @IBAction func previouseQuestion(_ sender: AnyObject){
        currentQuestionIndex -= 1
        validateActionButtons()
        self.questionsTableView.reloadData()
    }
    
}

extension BookActivityQuestionaireViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activityBookingQuestion[currentQuestionIndex].answers.count + 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let questionCell = tableView.dequeueReusableCell(withIdentifier: "BookActivityQuestionaireQuestionTableViewCell") as! BookActivityQuestionaireQuestionTableViewCell
        let answerCell = tableView.dequeueReusableCell(withIdentifier: "BookActivityQuestionaireAnswerTableViewCell") as! BookActivityQuestionaireAnswerTableViewCell
        
        if(indexPath.row == 0){
            questionCell.selectionStyle = .none
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                questionCell.questionTitle.text = activityBookingQuestion[currentQuestionIndex].questionAr
            }else{
                questionCell.questionTitle.text = activityBookingQuestion[currentQuestionIndex].questionEn
            }
            return questionCell
        }
        
        let answer = activityBookingQuestion[currentQuestionIndex].answers[indexPath.row-1]
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            answerCell.answerTitle.text = answer.choiceAr
        }else{
            answerCell.answerTitle.text = answer.choiceEn
        }

        if(answers[activityBookingQuestion[currentQuestionIndex]] != nil){
            if(answers[activityBookingQuestion[currentQuestionIndex]]?.id == answer.id){
                answerCell.answerRadioButton.setBackgroundImage(#imageLiteral(resourceName: "selectedRB"), for: .normal)
            }else{
                answerCell.answerRadioButton.setBackgroundImage(#imageLiteral(resourceName: "notSelectedRB"), for: .normal)
            }
        }else if (indexPath.row == 1){
            answerCell.answerRadioButton.setBackgroundImage(#imageLiteral(resourceName: "selectedRB"), for: .normal)
        }else{
            answerCell.answerRadioButton.setBackgroundImage(#imageLiteral(resourceName: "notSelectedRB"), for: .normal)
        }
        
        answerCell.answer = answer
        answerCell.question = activityBookingQuestion[currentQuestionIndex]
        answerCell.delegate = self
            
        answerCell.selectionStyle = .none
        
        return answerCell
    }
}

extension BookActivityQuestionaireViewController : BookActivityQuestionaireAnswerTableViewCellDelegate{
    func didSelectAnswerForQuestion(_ question: ActivityBookingQuestion, answer: ActivityBookingQuestionAnswer) {
        self.answers[question] = answer
        self.questionsTableView.reloadData()
    }
}
