//
//  BookActivityCalendarViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 12/6/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import FSCalendar

protocol BookActivityCalendarViewControllerDelegate : NSObjectProtocol {
    func didSelectDate(_ date : ActivityBookingDate)
}

class BookActivityCalendarViewController : UIViewController{
    @IBOutlet weak var calendar : FSCalendar!
    @IBOutlet weak var selectButton : UIButton!
    
    var activityBookingDates : [ActivityBookingDate] = []
    var selectedDate : Date?
    
    weak var delegate : BookActivityCalendarViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectButton.clipsToBounds = true
        selectButton.layer.cornerRadius = 10
        
        calendar.delegate = self
        calendar.dataSource = self
        
        self.selectButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Select"), for: .normal)
        
        if(selectedDate != nil){
            self.calendar.select(selectedDate, scrollToDate: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    @IBAction func selectDate(_ sender: AnyObject){
        if(self.calendar.selectedDate != nil){
            for activityBookingDate in activityBookingDates {
                if(NSCalendar.current.compare(self.calendar.selectedDate!, to: activityBookingDate.date,
                                              toGranularity: .day) == .orderedSame){
                    self.delegate?.didSelectDate(activityBookingDate)
                }
            }
            
            self.dismissAnimated()
        }else{
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PleaseSelectedDate"))
        }
    }
    
}

extension BookActivityCalendarViewController: FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        for activityBookingDate in activityBookingDates {
            if(NSCalendar.current.compare(date, to: activityBookingDate.date,
                                          toGranularity: .day) == .orderedSame){
                return true
            }
        }
        
        return false
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        
        for activityBookingDate in activityBookingDates {
            if(NSCalendar.current.compare(date, to: activityBookingDate.date,
                                          toGranularity: .day) == .orderedSame){
                return UIColor.clear
            }
        }
        
        return UIColor.lightGray
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        for activityBookingDate in activityBookingDates {
            if(NSCalendar.current.compare(date, to: activityBookingDate.date,
                                          toGranularity: .day) == .orderedSame){
                return ColorsUtility.colorWithHexString("#ffc107")
            }
        }
        return UIColor.black
    }
}
