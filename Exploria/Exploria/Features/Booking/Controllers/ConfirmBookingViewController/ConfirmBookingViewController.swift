//
//  ConfirmBookingViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/20/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation
import UIKit
import CCMPopup

protocol ConfirmBookingViewControllerDelegate : NSObjectProtocol {
    func didApproveBooking()
}

class ConfirmBookingViewController: UIViewController {
    
    @IBOutlet weak var scrollViewContentView : UIView!
    
    @IBOutlet weak var headerLabel : UILabel!
    
    @IBOutlet weak var userInformationHeaderLabel : UILabel!
    @IBOutlet weak var nameDetailsContainerView : UIView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var nameDetailsLabel : UILabel!
    @IBOutlet weak var mobileDetailsContainerView : UIView!
    @IBOutlet weak var mobileLabel : UILabel!
    @IBOutlet weak var mobileDetailsLabel : UILabel!
    @IBOutlet weak var mobileNumberPrefixContainerView : UIView!
    @IBOutlet weak var mobileNumberPrefixLabel : UILabel!
    @IBOutlet weak var mobileNumberPrefixImageView : UIImageView!
    @IBOutlet weak var emailDetailsContainerView : UIView!
    @IBOutlet weak var emailLabel : UILabel!
    @IBOutlet weak var emailDetailsLabel : UILabel!
    
    
    @IBOutlet weak var bookingInformationHeaderLabel : UILabel!
    @IBOutlet weak var activityDetailsContainerView : UIView!
    @IBOutlet weak var activityLabel : UILabel!
    @IBOutlet weak var activityDetailsLabel : UILabel!
    @IBOutlet weak var numberOfPlayersDetailsContainerView : UIView!
    @IBOutlet weak var numberOfPlayersLabel : UILabel!
    @IBOutlet weak var numberOfPlayersDetailsLabel : UILabel!
    @IBOutlet weak var dateDetailsContainerView : UIView!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var dateDetailsLabel : UILabel!
    @IBOutlet weak var timeDetailsContainerView : UIView!
    @IBOutlet weak var timeLabel : UILabel!
    @IBOutlet weak var timeDetailsLabel : UILabel!
    
    @IBOutlet weak var paymentHeaderLabel : UILabel!
    @IBOutlet weak var typeDetailsContainerView : UIView!
    @IBOutlet weak var typeLabel : UILabel!
    @IBOutlet weak var typeDetailsLabel : UILabel!
    @IBOutlet weak var methodDetailsContainerView : UIView!
    @IBOutlet weak var methodLabel : UILabel!
    @IBOutlet weak var methodDetailsLabel : UILabel!
    @IBOutlet weak var totalDetailsContainerView : UIView!
    @IBOutlet weak var totalLabel : UILabel!
    @IBOutlet weak var totalDetailsLabel : UILabel!
    
    @IBOutlet weak var confirmButton : UIButton!
    
    
    weak var delegate : ConfirmBookingViewControllerDelegate?
    
    var activity = ""
    var numberOfPlayers = ""
    var date = ""
    var time = ""
    var type = ""
    var method = ""
    var total = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confirmButton.clipsToBounds = true
        confirmButton.layer.cornerRadius = 10
        
        headerLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("ConfirmBooking")
        userInformationHeaderLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("UserInformation")
        nameLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Name")
        mobileLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Mobile")
        emailLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Email")
        bookingInformationHeaderLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("BookingInformation")
        activityLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Activity")
        numberOfPlayersLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("NumberOfPlayers")
        dateLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Date")
        timeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Time")
        headerLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("ConfirmBooking")
        paymentHeaderLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Payment")
        typeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Type")
        methodLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Method")
        totalLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("Total")
        
        confirmButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Confirm"), for: .normal)
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.scrollViewContentView.semanticContentAttribute = .forceRightToLeft
            self.nameDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.mobileDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.emailDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.activityDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.numberOfPlayersDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.dateDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.timeDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.typeDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.methodDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.totalDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.mobileNumberPrefixContainerView.semanticContentAttribute = .forceRightToLeft
            
            self.nameLabel.textAlignment = .left
            self.mobileLabel.textAlignment = .left
            self.emailLabel.textAlignment = .left
//            self.activityLabel.textAlignment = .left
//            self.numberOfPlayersLabel.textAlignment = .left
//            self.dateLabel.textAlignment = .left
//            self.timeLabel.textAlignment = .left
//            self.typeLabel.textAlignment = .left
//            self.methodLabel.textAlignment = .left
//            self.totalLabel.textAlignment = .left
            
            self.nameDetailsLabel.textAlignment = .right
            self.mobileDetailsLabel.textAlignment = .right
            self.emailDetailsLabel.textAlignment = .right
            self.activityDetailsLabel.textAlignment = .right
            self.numberOfPlayersDetailsLabel.textAlignment = .right
            self.dateDetailsLabel.textAlignment = .right
            self.timeDetailsLabel.textAlignment = .right
            self.typeDetailsLabel.textAlignment = .right
            self.methodDetailsLabel.textAlignment = .right
            self.totalDetailsLabel.textAlignment = .right
        }
        
        self.mobileNumberPrefixLabel.text = CountriesDataCenter.sharedInstance.selectedCountry?.prefix
        if(CountriesDataCenter.sharedInstance.selectedCountry?.countryImage != nil){
            self.mobileNumberPrefixImageView.image = CountriesDataCenter.sharedInstance.selectedCountry?.countryImage
        }else{
            CountriesDataCenter.sharedInstance.selectedCountry?.loadImageWithDelegate(self)
        }
        
        self.nameDetailsLabel.text = UserIdentificationDataCenter.sharedInstance.loggedInUser?.name ?? ""
        self.mobileDetailsLabel.text = UserIdentificationDataCenter.sharedInstance.loggedInUser?.phone ?? ""
        self.emailDetailsLabel.text = UserIdentificationDataCenter.sharedInstance.loggedInUser?.email ?? ""
        
        self.activityDetailsLabel.text = activity
        self.numberOfPlayersDetailsLabel.text = numberOfPlayers
        self.dateDetailsLabel.text = date
        self.timeDetailsLabel.text = time
        self.typeDetailsLabel.text = type
        self.methodDetailsLabel.text = method
        self.totalDetailsLabel.text = total
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    @IBAction func confirm(_ sender: AnyObject){
        self.dismiss(UIButton())
        delegate?.didApproveBooking()
    }
    
    @IBAction func dismiss(_ sender: AnyObject){
        self.dismissAnimated()
    }
}

extension ConfirmBookingViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        if(operationID == .LoadImageGeneralOperationID){
            self.mobileNumberPrefixImageView.image = CountriesDataCenter.sharedInstance.selectedCountry?.countryImage
        }
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {

    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}
