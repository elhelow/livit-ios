//
//  BookCourseViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/14/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation
import UIKit
import SwiftyStarRatingView
import MapKit
import CCMPopup

class BookCourseViewController: UIViewController {
    
    @IBOutlet weak var providerName : UILabel!
    @IBOutlet weak var providerImage : UIImageView!
    @IBOutlet weak var favButton : UIButton!
    @IBOutlet weak var topDetailsContainerView : UIView!
    
    @IBOutlet weak var lessonsHeader : UILabel!
    @IBOutlet weak var lessonsTableView : UITableView!
    @IBOutlet weak var lessonsTableViewHeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var pricingContainerView : UIView!
    @IBOutlet weak var pricingTitle : UILabel!
    @IBOutlet weak var pricingDetails : UILabel!
    
    @IBOutlet weak var promoCodeContainerView : UIView!
    @IBOutlet weak var promoCodeTextField : UITextField!
    @IBOutlet weak var addPromoCodeButton : UIButton!
    
    @IBOutlet weak var courseOverviewContainerView : UIView!
    @IBOutlet weak var courseOverviewTitle : UILabel!
    @IBOutlet weak var courseOverviewDetails : UILabel!
    
    @IBOutlet weak var organizerContainerView : UIView!
    @IBOutlet weak var organizerImageView : UIImageView!
    @IBOutlet weak var organizerTitle : UILabel!
    @IBOutlet weak var organizerDetails : UILabel!
    
    @IBOutlet weak var locationContainerView : UIView!
    @IBOutlet weak var locationTitle : UILabel!
    @IBOutlet weak var locationMapView : MKMapView!
    
    @IBOutlet weak var enrollContainerView : UIView!
    @IBOutlet weak var enrollButton : UIButton!
    
    var bookingDetails : CourseBookingDetails = CourseBookingDetails(data: [:])
    var providerID = ""
    
    fileprivate var dateKey = "DateKey"
    fileprivate var timeKey = "TimeKey"
    var selectedTimes : [CourseBookingScheduledLesson : [String : AnyObject]] = [:]
    var totalPrice = ""
    
    var referenceNumber = ""
    var appliedPromoCode : PromoCode?
    
    override func viewDidLoad() {
        
        UIUtilities.createCircularViewforView(addPromoCodeButton, withRadius: 10)
        UIUtilities.createCircularViewforView(enrollButton, withRadius: 10)
        
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.lessonsTableView.semanticContentAttribute = .forceRightToLeft
        }
        
        self.lessonsHeader.text = LocalizationManager.sharedInstance.getTranslationForKey("CourseTiming")
        self.courseOverviewTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("CourseOverview")
        self.organizerTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("Organizer")
        self.locationTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("LocationOnTheMap")
        self.promoCodeTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("HaveAPromoCode")
        self.enrollButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("EnrollNow"), for: .normal)
        self.addPromoCodeButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Add"), for: .normal)
        
        self.providerName.text = ""
        self.pricingDetails.text = "0"
        self.courseOverviewDetails.text = ""
        self.organizerDetails.text = ""
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.topDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.pricingContainerView.semanticContentAttribute = .forceRightToLeft
            self.courseOverviewContainerView.semanticContentAttribute = .forceRightToLeft
            self.organizerContainerView.semanticContentAttribute = .forceRightToLeft
            self.locationContainerView.semanticContentAttribute = .forceRightToLeft
            self.enrollContainerView.semanticContentAttribute = .forceRightToLeft
            self.promoCodeContainerView.semanticContentAttribute = .forceRightToLeft
            
            self.providerName.textAlignment = .right
            self.lessonsHeader.textAlignment = .right
            self.courseOverviewTitle.textAlignment = .right
            self.courseOverviewDetails.textAlignment = .right
            self.organizerTitle.textAlignment = .right
            self.organizerDetails.textAlignment = .right
            self.locationTitle.textAlignment = .right
            self.promoCodeTextField.textAlignment = .right
        }
        
        UIUtilities.changePlaceHolderTextColorForTextField(promoCodeTextField, color: .darkGray)
        
        promoCodeTextField.layer.borderWidth = 1
        promoCodeTextField.layer.borderColor = UIColor.darkGray.cgColor
        
        UIUtilities.createCircularViewforView(promoCodeTextField, withRadius: 10)
        
        UIUtilities.addTapGestureToView(self.view, withTarget: self, andSelector: #selector(dismissKeypad), andCanCancelTouchesInTheView: false)
        
        BookingDataCenter.sharedInstance.loadCourseDetailsWithDelegate(self, providerID: self.providerID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.lessonsTableView.reloadData()
        self.updatePricing()
        MainNavigator.sharedInstance.setScreenTitle(LocalizationManager.sharedInstance.getTranslationForKey("BookCourse")!)
        
        MainNavigator.sharedInstance.delegate = self
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showCourseCalendar") {
            let popupSegue = segue as! CCMPopupSegue
            
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.width-50
                , height: 460)
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let bookCourseCalendarViewController = popupSegue.destination as! BookCourseCalendarViewController
            bookCourseCalendarViewController.courseBookingLesson = (sender as! CourseBookingScheduledLesson)
            if(selectedTimes[(sender as! CourseBookingScheduledLesson)] != nil){
                bookCourseCalendarViewController.selectedDate = (selectedTimes[(sender as! CourseBookingScheduledLesson)]?[dateKey] as! CourseBookingDate)
                bookCourseCalendarViewController.selectedTime = (selectedTimes[(sender as! CourseBookingScheduledLesson)]?[timeKey] as! CourseBookingTime)
            }

            bookCourseCalendarViewController.delegate = self
            bookCourseCalendarViewController.view.layer.cornerRadius = 10
            bookCourseCalendarViewController.view.clipsToBounds = true
            
        }else if(segue.identifier == "showSuccessPopup"){
            let popupSegue = segue as! CCMPopupSegue
            
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.width-50
                , height: 470)
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let bookingResultViewController = popupSegue.destination as! BookingResultViewController
            bookingResultViewController.referenceNumber = self.referenceNumber
            bookingResultViewController.phoneNumber = "+965 000 000 00"
        }
    }
    
    func dismissKeypad(){
        self.promoCodeTextField.resignFirstResponder()
    }
    
    func updatePricing(){
        if(bookingDetails.isPreDefined){
            var priceValue = Double(bookingDetails.price) ?? 0.0
            
            if(appliedPromoCode != nil){
                priceValue = (priceValue * (appliedPromoCode?.percentage)!) / 100
                priceValue = Double(round(1000*priceValue)/1000)
            }
            
            totalPrice = "\(priceValue)"
            
            self.pricingDetails.text = "\(priceValue) " + LocalizationManager.sharedInstance.getTranslationForKey("KD")!
            
            self.pricingTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("Price")

        }else if(bookingDetails.isScheduled){
            var priceValue = 0.0
            
            for lesson in selectedTimes.values{
                priceValue += (lesson[timeKey] as! CourseBookingTime).price
            }
            
            if(appliedPromoCode != nil){
                priceValue = (priceValue * (appliedPromoCode?.percentage)!) / 100
                priceValue = Double(round(1000*priceValue)/1000)
            }
            
            totalPrice = "\(priceValue)"
            
            self.pricingDetails.text = "\(totalPrice) " + LocalizationManager.sharedInstance.getTranslationForKey("KD")!
            
            self.pricingTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("Total")
        }
            
    }
    
    func reloadBookingDetails(){
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.providerName.text = bookingDetails.nameAr
            self.courseOverviewDetails.text = bookingDetails.overviewAr
            self.organizerDetails.text = bookingDetails.organizerAr
        }else{
            self.providerName.text = bookingDetails.nameEn
            self.courseOverviewDetails.text = bookingDetails.overviewEn
            self.organizerDetails.text = bookingDetails.organizerEn
        }
        
        
        if(bookingDetails.courseImage != nil){
            self.providerImage.image = bookingDetails.courseImage
        }else{
            bookingDetails.loadCourseImageWithDelegate(self)
        }
        
        if(bookingDetails.organizerImage != nil){
            self.organizerImageView.image = bookingDetails.organizerImage
        }else{
            bookingDetails.loadOrganizerImageWithDelegate(self)
        }
        
        if(Double(bookingDetails.lat) != nil && Double(bookingDetails.lng) != nil){
            var annotations : [MKPointAnnotation] = []
            
            let dropPin = MKPointAnnotation()
            dropPin.coordinate = CLLocationCoordinate2D(latitude: Double(bookingDetails.lat)!, longitude: Double(bookingDetails.lng)!)
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                dropPin.title = bookingDetails.nameAr
            }else{
                dropPin.title = bookingDetails.nameEn
            }
            
            annotations.append(dropPin)
            
            self.locationMapView.addAnnotations(annotations)
            
            let center = CLLocationCoordinate2D(latitude: Double(bookingDetails.lat)!, longitude: Double(bookingDetails.lng)!)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
            self.locationMapView.setRegion(region, animated: true)
        }
        
        if(bookingDetails.isFavorite){
            self.favButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
        }else{
            self.favButton.setImage(#imageLiteral(resourceName: "notFavorite"), for: .normal)
        }
        
        self.lessonsTableView.reloadData()
        
        self.updatePricing()
        
    }
    
    
    @IBAction func toggleFavoriteStatus(_ sender: AnyObject){
        if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
            ActivityProvidersDataCenter.sharedInstance.toggleProviderFavoriteStatusWithDelegate(self, activityProviderID: bookingDetails.activityProviderID, newFavStatus: bookingDetails.isFavorite ? "0" : "1")
        }else{
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("YouNeedToSignIn"))
        }
    }
    
    @IBAction func addPromoCode(_ sender: AnyObject){
        self.dismissKeypad()
        if(UserIdentificationDataCenter.sharedInstance.loggedInUser == nil){
            IdentificationPopUpViewCotroller.showInController(self)
            return
        }
        if(promoCodeTextField.text != ""){
            BookingDataCenter.sharedInstance.checkPromoCodeWithDelegate(self, providerID: self.providerID, promoCode: promoCodeTextField.text!)
        }else{
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PromoCodeIsRequired"))
        }
    }
    
    @IBAction func enroll(_ sender: AnyObject){
        
        if(UserIdentificationDataCenter.sharedInstance.loggedInUser == nil){
            IdentificationPopUpViewCotroller.showInController(self)
            return
        }
        
        if(bookingDetails.isPreDefined){
            let courseBookingQuery = CourseBookingQuery()
            courseBookingQuery.activityProviderID = self.providerID
            courseBookingQuery.lessonsIDs = []
            courseBookingQuery.selectedDatesIDs = []
            courseBookingQuery.selectedTimesIDs = []
            courseBookingQuery.totalPayment = self.totalPrice
            courseBookingQuery.pricingType = self.bookingDetails.priceType
            
            BookingDataCenter.sharedInstance.saveCourseBookingDetailsWithDelegate(self, bookingQuery: courseBookingQuery)
        }else{
            if(selectedTimes.count > 0){
                let courseBookingQuery = CourseBookingQuery()
                courseBookingQuery.activityProviderID = self.providerID
                courseBookingQuery.lessonsIDs = []
                courseBookingQuery.selectedDatesIDs = []
                courseBookingQuery.selectedTimesIDs = []
                
                for lesson in selectedTimes.keys{
                    courseBookingQuery.lessonsIDs.append(lesson.id)
                    courseBookingQuery.selectedDatesIDs.append((selectedTimes[lesson]![dateKey] as! CourseBookingDate).id)
                    courseBookingQuery.selectedTimesIDs.append((selectedTimes[lesson]![timeKey] as! CourseBookingTime).id)
                }
                
                courseBookingQuery.totalPayment = self.totalPrice
                courseBookingQuery.pricingType = self.bookingDetails.priceType
                
                BookingDataCenter.sharedInstance.saveCourseBookingDetailsWithDelegate(self, bookingQuery: courseBookingQuery)
            }else{
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("SelectAtLeastOneLesson"))
            }
        }
    }
}

extension BookCourseViewController : BookCourseCalendarViewControllerDelegate{
    func didSelectDate(_ date: CourseBookingDate, selectedTime: CourseBookingTime, forLesson: CourseBookingScheduledLesson) {
        selectedTimes[forLesson] = [dateKey:date,timeKey:selectedTime]
    }
}

extension BookCourseViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(bookingDetails.isPreDefined){
            return bookingDetails.predefinedLessons.count
        }else if(bookingDetails.isScheduled){
            return bookingDetails.scheduledLessons.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LessonTableViewCell") as! LessonTableViewCell
        
        if(bookingDetails.isPreDefined){
            let lessonDetails = bookingDetails.predefinedLessons[indexPath.row]
            
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                cell.lessonName.text = lessonDetails.nameAr
            }else{
                cell.lessonName.text = lessonDetails.nameEn
            }
            
            cell.lessonDate.text = lessonDetails.bookDate
            cell.lessonTime.text = lessonDetails.startTime + " - " + lessonDetails.endTime
            
            if(indexPath.row == bookingDetails.predefinedLessons.count-1){
                cell.separator.isHidden = true
            }else{
                cell.separator.isHidden = false
            }
            
            cell.scheduleButton.isHidden = true
            
        }else{
            let lessonDetails = bookingDetails.scheduledLessons[indexPath.row]
            
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                cell.lessonName.text = lessonDetails.nameAr
            }else{
                cell.lessonName.text = lessonDetails.nameEn
            }
            
            if(selectedTimes[lessonDetails] != nil){
                cell.scheduleButton.isHidden = true
                let selectedDate = (selectedTimes[lessonDetails]?[dateKey] as! CourseBookingDate)
                let selectedTime = (selectedTimes[lessonDetails]?[timeKey] as! CourseBookingTime)
                
                cell.lessonDate.text = selectedDate.dateAsString
                cell.lessonTime.text = selectedTime.startTime + " - " + selectedTime.endTime
            }else{
                cell.lessonDate.text = ""
                cell.lessonTime.text = ""
                cell.scheduleButton.isHidden = false
            }
            
            if(indexPath.row == bookingDetails.scheduledLessons.count-1){
                cell.separator.isHidden = true
            }else{
                cell.separator.isHidden = false
            }
        }
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            cell.contentView.semanticContentAttribute = .forceRightToLeft
            cell.lessonName.textAlignment = .right
            cell.lessonTime.textAlignment = .left
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(bookingDetails.isScheduled && !bookingDetails.isPreDefined){
            let lessonDetails = bookingDetails.scheduledLessons[indexPath.row]
            self.performSegue(withIdentifier: "showCourseCalendar", sender: lessonDetails)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        lessonsTableViewHeightConstraint.constant = lessonsTableView.contentSize.height
    }
}


extension BookCourseViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        self.reloadBookingDetails()
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if(operationID == .ToggleFavoriteStatus){
            let isError = object as! Bool
            if(isError){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("GenericError"))
            }else{
                bookingDetails.isFavorite = !bookingDetails.isFavorite
                self.reloadBookingDetails()
            }
        }else if(operationID == .BookingCourseDetails){
            self.bookingDetails = object as! CourseBookingDetails
            self.reloadBookingDetails()
        }else if(operationID == .SaveCourseDetails){
            if let _ = object as? Bool{
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("GenericError"))
            }else{
                self.referenceNumber = object as! String
                self.performSegue(withIdentifier: "showSuccessPopup", sender: nil)
            }
            
        }else if(operationID == .CheckPromoCode){
            if let _ = object as? Bool{
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("PromoCodeNotValid"))
            }else{
                self.appliedPromoCode = object as? PromoCode
                self.promoCodeTextField.isEnabled = false
                self.addPromoCodeButton.isEnabled = false
                self.addPromoCodeButton.backgroundColor = .gray
                self.addPromoCodeButton.setTitle("\((self.appliedPromoCode?.percentage)!)%", for: .normal)
                self.updatePricing()
            }
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension BookCourseViewController : MainNavigatorNavigationBarDelegate{
    func didTapOnBack() {
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(false)
        MainNavigator.sharedInstance.popViewControllerInCenterPanel()
    }
    
    func didTapOnSort() {
        
    }
}

extension BookCourseViewController : MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) {
            //if annotation is not an MKPointAnnotation (eg. MKUserLocation),
            //return nil so map draws default view for it (eg. blue dot)...
            return nil
        }
        
        let reuseId = "locationAnnotation"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: "R")
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            // Resize image
            let pinImage = #imageLiteral(resourceName: "detailsPin")
            
            let size = CGSize(width: 30, height: 40)
            UIGraphicsBeginImageContext(size)
            pinImage.draw(in: CGRect(x:0, y:0, width:size.width, height:size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            anView?.image = resizedImage
            anView?.canShowCallout = false
        }
        else {
            //we are re-using a view, update its annotation reference...
            anView?.annotation = annotation
        }
        
        return anView
    }
}

