//
//  LessonTableViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/14/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

class LessonTableViewCell: UITableViewCell {
    @IBOutlet weak var lessonName : UILabel!
    @IBOutlet weak var lessonDate : UILabel!
    @IBOutlet weak var lessonTime : UILabel!

    @IBOutlet weak var separator : UIView!
    @IBOutlet weak var scheduleButton : UIButton!
    
    override func awakeFromNib() {
        scheduleButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Schedule"), for: .normal)
    }
    
    override func prepareForReuse() {
        
    }
}
