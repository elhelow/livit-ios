//
//  BookingResultViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/20/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

class BookingResultViewController: UIViewController {
    
    @IBOutlet weak var headerLabel : UILabel!
    
    @IBOutlet weak var introMessage1 : UILabel!
    @IBOutlet weak var referenceNumberLabel : UILabel!
    @IBOutlet weak var introMessage2 : UILabel!
    @IBOutlet weak var phoneLabel : UILabel!
    @IBOutlet weak var backButton : UIButton!
    
    
    var referenceNumber = ""
    var phoneNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backButton.clipsToBounds = true
        backButton.layer.cornerRadius = 10
        
        headerLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("BookingConfirmed")
        introMessage1.text = LocalizationManager.sharedInstance.getTranslationForKey("BookingSuccess1")
        referenceNumberLabel.text = referenceNumber
        introMessage2.text = LocalizationManager.sharedInstance.getTranslationForKey("BookingSuccess2")
        phoneLabel.text = phoneNumber
        
        backButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("BackToHome"), for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    @IBAction func back(_ sender: AnyObject){
        self.dismissAnimated()
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(false)
        MenuViewController.sharedInstance().goToMenuItem(.Home)
    }
}
