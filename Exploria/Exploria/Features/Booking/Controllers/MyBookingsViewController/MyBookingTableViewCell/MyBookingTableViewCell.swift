//
//  MyBookingTableViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 2/28/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

protocol MyBookingTableViewCellDelegate : NSObjectProtocol {
    func didTapOnRebookForCellWithIndex(index : Int)
}

class MyBookingTableViewCell: UITableViewCell {
    @IBOutlet weak var title : UILabel!
    @IBOutlet weak var date : UILabel!
    @IBOutlet weak var deposite : UILabel!
    @IBOutlet weak var total : UILabel!
    @IBOutlet weak var numberOfPlayers : UILabel!
    @IBOutlet weak var providerImage : UIImageView!
    @IBOutlet weak var rebookButton : UIButton!
    
    var index = -1
    weak var delegate : MyBookingTableViewCellDelegate?
    
    override func awakeFromNib() {
        self.rebookButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Rebook"), for: .normal)
    }
    
    override func prepareForReuse() {
        self.providerImage.image = #imageLiteral(resourceName: "imagePlaceHolder")
    }
    
    @IBAction func rebook(_ sender: UIButton) {
        delegate?.didTapOnRebookForCellWithIndex(index: index)
    }
    
}
