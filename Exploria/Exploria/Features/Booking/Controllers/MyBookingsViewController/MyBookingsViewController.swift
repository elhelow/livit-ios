//
//  MyBookingsViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 2/18/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation
import UIKit
import CCMPopup
import MapKit


class MyBookingsViewController: UIViewController {
 
    @IBOutlet weak var tableView : UITableView!
    
    var bookings : [BookingHistory] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        MainNavigator.sharedInstance.setScreenTitle(LocalizationManager.sharedInstance.getTranslationForKey("MyBookings")!)
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.tableView.semanticContentAttribute = .forceRightToLeft
        }
        
        self.tableView.estimatedRowHeight = 160
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        BookingDataCenter.sharedInstance.loadBookingsForCurrentUserWithDelegate(self)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        
    }

    override func viewWillDisappear(_ animated: Bool) {

    }

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}


extension MyBookingsViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookings.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "MyBookingTableViewCell") as! MyBookingTableViewCell
        let bookingHistory = bookings[indexPath.row]
        
        cell.index = indexPath.row
        cell.delegate = self
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            cell.title.text = bookingHistory.providerNameAr
            
            cell.contentView.semanticContentAttribute = .forceRightToLeft
            cell.title.textAlignment = .right
            cell.deposite.textAlignment = .right
            cell.date.textAlignment = .right
            cell.numberOfPlayers.textAlignment = .right
            cell.total.textAlignment = .right
        }else{
            cell.title.text = bookingHistory.providerNameEn
        }
        
        cell.date.text = bookingHistory.date
        
        cell.deposite.text = LocalizationManager.sharedInstance.getTranslationForKey("Deposite")! + ": " + bookingHistory.depositeValue + " " + LocalizationManager.sharedInstance.getTranslationForKey("KD")!
        cell.total.text = LocalizationManager.sharedInstance.getTranslationForKey("Total")! + ": " + bookingHistory.totalPayment + " " + LocalizationManager.sharedInstance.getTranslationForKey("KD")!
        
        cell.numberOfPlayers.text = LocalizationManager.sharedInstance.getTranslationForKey("NumberOfPlayers")! + ": " + bookingHistory.numberOfPlayers

        
        if(bookingHistory.providerImage != nil){
            cell.providerImage.image = bookingHistory.providerImage
        }else{
            bookingHistory.loadProviderImageWithDelegate(self)
        }

        cell.selectionStyle = .none
        return cell
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}

extension MyBookingsViewController : OperationDelegate{

    func didFinishOperation(_ operationID: OperationID) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        DispatchQueue.main.async {
            if let object = object as? [BookingHistory] {
                self.bookings = object
            }
            self.tableView.reloadData()
        }
    }

    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {

    }

    func didCancelOperation(_ operationID: OperationID) {

    }
}

extension MyBookingsViewController : MyBookingTableViewCellDelegate {
    func didTapOnRebookForCellWithIndex(index: Int) {
        let bookingHistory = bookings[index]
        if(bookingHistory.type == "activity"){
            let storyboard = UIStoryboard(name: "Booking", bundle: nil)
            let bookActivityViewController = storyboard.instantiateViewController(withIdentifier: "BookActivityViewController") as! BookActivityViewController
            bookActivityViewController.providerID = bookingHistory.activityProviderID
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                bookActivityViewController.activityName = bookingHistory.providerNameAr
            }else{
                bookActivityViewController.activityName = bookingHistory.providerNameEn
            }
            MainNavigator.sharedInstance.pushViewController(inCenterPanel: bookActivityViewController)
        }else{
            let storyboard = UIStoryboard(name: "Booking", bundle: nil)
            let bookCourseViewController = storyboard.instantiateViewController(withIdentifier: "BookCourseViewController") as! BookCourseViewController
            bookCourseViewController.providerID = bookingHistory.activityProviderID
            MainNavigator.sharedInstance.pushViewController(inCenterPanel: bookCourseViewController)
        }
        
    }
}
