//
//  ContactUsViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/18/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation
import CCMPopup

class ContactUsViewController: UIViewController {
    
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var keypadHandleViewHeightConstraint : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendButton.clipsToBounds = true
        sendButton.layer.cornerRadius = 10
        
        emailTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("ContactUsEmailPlaceHolder")
        nameTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("ContactUsNamePlaceHolder")
        messageTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("ContactUsMessagePlaceHolder")
        headerLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("ContactUsHeader")
        
        
        emailTextField.text = UserIdentificationDataCenter.sharedInstance.loggedInUser?.email ?? ""
        nameTextField.text = UserIdentificationDataCenter.sharedInstance.loggedInUser?.name ?? ""
        sendButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("ContactUsSendButton"), for: .normal)
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.emailTextField.textAlignment = .right
            self.nameTextField.textAlignment = .right
            self.headerLabel.textAlignment = .right
            self.messageTextField.textAlignment = .right
        }
        
        UIUtilities.notifyMeWhenKeyPadWillShow(target: self, selector: #selector(updateViewScroll))
        UIUtilities.notifyMeWhenKeyPadDidChangeFrame(target: self, selector: #selector(updateViewScroll))
        UIUtilities.notifyMeWhenKeyPadWillHide(target: self, selector: #selector(updateViewScroll))
        
        UIUtilities.addTapGestureToView(self.view, withTarget: self, andSelector: #selector(dismissKeypad), andCanCancelTouchesInTheView: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    class func showInController(_ controller: UIViewController){
        
        let storyboard = UIStoryboard(name: "AppInfos", bundle: nil)
        let popupViewController = storyboard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        popupViewController.view.alpha = 1.0
        
        let popup = CCMPopupTransitioning.sharedInstance()
        popup?.destinationBounds = CGRect(x: 0, y: 0, width: controller.view.frame.width - 50, height: 420)
        popup?.backgroundBlurRadius = 0.7
        popup?.backgroundViewAlpha = 0.7
        popup?.backgroundViewColor = UIColor.black
        popup?.dismissableByTouchingBackground = true
        popup?.presentedController = popupViewController
        popup?.presentingController = controller
        
        controller.present(popupViewController, animated: true, completion: nil)
    }
    
    
    func updateViewScroll(notification: NSNotification){
        let userInfo = notification.userInfo!
        
        let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        keypadHandleViewHeightConstraint.constant = self.view.bounds.maxY - convertedKeyboardEndFrame.minY
        if(keypadHandleViewHeightConstraint.constant < 0){
            keypadHandleViewHeightConstraint.constant = 0
        }
        AnimationsUtility.animateLayoutFor(self.view, duration: Float(animationDuration), delay: 0.0, completionBlock: nil)
    }
    
    func dismissKeypad(){
        self.emailTextField.resignFirstResponder()
        self.nameTextField.resignFirstResponder()
        self.messageTextField.resignFirstResponder()
    }
    
    func validateContactUsForm() -> Bool{
        if(!ValidationsUtility.isStringNotEmpty(self.nameTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("NameIsRequired"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.emailTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsRequired"))
            return false
        }else if(!ValidationsUtility.isEmailIsValid(self.emailTextField.text!, isStrict: true)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsNotCorrect"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.messageTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("MessageIsRequired"))
            return false
        }
        
        return true
    }
    
    @IBAction func submit(_ sender: UIButton) {
        self.dismissKeypad()
        if(self.validateContactUsForm()){
            AppInfosDataCenter.sharedInstance.contactUsWithDelegate(self, name: nameTextField.text!, email: emailTextField.text!, message: messageTextField.text!)
        }
    }
    
}

extension ContactUsViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.dismissKeypad()
        self.submit(UIButton())
        return true
    }
}


extension ContactUsViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
       self.dismissAnimated()
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
    
}
