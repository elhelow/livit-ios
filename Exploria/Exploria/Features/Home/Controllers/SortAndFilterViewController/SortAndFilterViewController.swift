//
//  SortAndFilterViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/10/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

enum ViewMode {
    case FilterCategory
    case SortActivity
    case Search
}

protocol SortAndFilterDelegate : NSObjectProtocol{
    func didTapOnActionWithMode(_ mode:ViewMode, ids : [String])
}

class SortAndFilterViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var actionButton : UIButton!
    weak var delegate : SortAndFilterDelegate?
    var selectedIDs : [String] = []
    var sortCategories : [FilterCategory] = []
    
    var mode : ViewMode = .FilterCategory
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        actionButton.clipsToBounds = true
        actionButton.layer.cornerRadius = 10
        
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        if(mode == .FilterCategory){
            CategoriesDataCenter.sharedInstance.loadFilterCategoiresWithDelegate(self)
            self.titleLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("FilterCategories")
            self.actionButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Filter"), for: .normal)
        }else{
            self.titleLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("SortActivities")
            self.actionButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("Sort"), for: .normal)
            self.initSortingData()
        }
        
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.titleLabel.textAlignment = .right
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func initSortingData(){
        let popular = FilterCategory()
        popular.nameEn = "Popular"
        popular.nameAr = "الجميع"
        popular.id = ""
        popular.image = #imageLiteral(resourceName: "popular")
        
        let new = FilterCategory()
        new.nameEn = "New"
        new.nameAr = "جديد"
        new.id = "new"
        new.image = #imageLiteral(resourceName: "new")
        
        let alpha = FilterCategory()
        alpha.nameEn = "Alphabet"
        alpha.nameAr = "الأبجدية"
        alpha.id = "alph"
        alpha.image = #imageLiteral(resourceName: "alpha")
        
        let phtl = FilterCategory()
        phtl.nameEn = "Price Hight to Low"
        phtl.nameAr = "السعر الاعلى الي الاقل"
        phtl.id = "highToLow"
        phtl.image = #imageLiteral(resourceName: "phtl")
        
        let plth = FilterCategory()
        plth.nameEn = "Price Low to High"
        plth.nameAr = "السعر الاقل الي الاعلى"
        plth.id = "lowToHigh"
        plth.image = #imageLiteral(resourceName: "plth")
        
        self.sortCategories.append(popular)
        self.sortCategories.append(new)
        self.sortCategories.append(alpha)
        self.sortCategories.append(phtl)
        self.sortCategories.append(plth)
        
    }
    
    @IBAction func doAction(_ sender: AnyObject){
        if(delegate != nil){
            delegate?.didTapOnActionWithMode(self.mode, ids: selectedIDs)
        }
        
        self.dismissAnimated()
    }
    
}

extension SortAndFilterViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(mode == .FilterCategory){
            return CategoriesDataCenter.sharedInstance.filterCategories.count
        }
        
        return sortCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortAndFilterTableViewCell") as! SortAndFilterTableViewCell
        var filterCategory = FilterCategory()
        
        if(mode == .FilterCategory){
            filterCategory = CategoriesDataCenter.sharedInstance.filterCategories[indexPath.row]
        }else{
            filterCategory = sortCategories[indexPath.row]
        }

        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            cell.titleLabel.text = filterCategory.nameAr
            cell.titleLabel.textAlignment = .right
            cell.contentView.semanticContentAttribute = .forceRightToLeft
            cell.containerView.semanticContentAttribute = .forceRightToLeft
        }else{
            cell.titleLabel.text = filterCategory.nameEn
        }
        
        
        if(selectedIDs.contains(filterCategory.id)){
            cell.checkMarkImage.isHidden = false
        }else{
            cell.checkMarkImage.isHidden = true
        }

        
        if(filterCategory.image == nil){
            filterCategory.loadImageWithDelegate(self)
        }else{
            cell.iconImage.image = filterCategory.image
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(mode == .SortActivity){
            selectedIDs.removeAll()
            selectedIDs.append(sortCategories[indexPath.row].id)
        }else{
            if(selectedIDs.contains(CategoriesDataCenter.sharedInstance.filterCategories[indexPath.row].id)){
                selectedIDs = selectedIDs.filter { $0 !=  CategoriesDataCenter.sharedInstance.filterCategories[indexPath.row].id}
            }else{
                selectedIDs.append(CategoriesDataCenter.sharedInstance.filterCategories[indexPath.row].id)
            }
        }
        
        self.tableView.reloadData()
        
    }
    
    
}

extension SortAndFilterViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        self.tableView.reloadData()
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}
