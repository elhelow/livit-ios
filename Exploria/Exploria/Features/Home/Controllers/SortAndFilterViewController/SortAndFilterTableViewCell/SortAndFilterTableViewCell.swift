//
//  SortAndFilterTableViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/11/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class SortAndFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var iconImage : UIImageView!
    @IBOutlet weak var checkMarkImage : UIImageView!
    
    var index = 0
    
    override func awakeFromNib() {
        
    }
    
    override func prepareForReuse() {
        self.iconImage.image = #imageLiteral(resourceName: "imagePlaceHolder")
    }
}



