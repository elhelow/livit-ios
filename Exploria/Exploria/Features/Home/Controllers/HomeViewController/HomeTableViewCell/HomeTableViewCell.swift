//
//  HomeTableViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/2/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class HomeTableViewCell: UITableViewCell {
    @IBOutlet weak var coursesAndOffersLabel : UILabel!
    @IBOutlet weak var coursesAndOffersContainerView : UIView!
    
    @IBOutlet weak var activityImage : UIImageView!
    
    @IBOutlet weak var activityName : UILabel!
    @IBOutlet weak var providersLabel : UILabel!
    @IBOutlet weak var activityCategories : UILabel!

    @IBOutlet weak var activityDetailContainerView : UIView!
    
    override func awakeFromNib() {

    }
    
    override func prepareForReuse() {
        self.activityImage.image = #imageLiteral(resourceName: "imagePlaceHolder")
    }
}


