//
//  HomeViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/2/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit
import CCMPopup
import MapKit


class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var tabBarContainer : UIView!
    @IBOutlet weak var searchViewContainer : UIView!
    @IBOutlet weak var searchTextField : UITextField!
    @IBOutlet weak var blockImage : UIImageView!
    
    @IBOutlet weak var searchViewContainerHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var tabBarContainerHeightConstraint : NSLayoutConstraint!
    
    var userlongitude = ""
    var userlatitude = ""
    let locationManager : CLLocationManager = CLLocationManager()
    var searchTask : DispatchWorkItem?
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)

        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.tableView.semanticContentAttribute = .forceRightToLeft
            self.tabBarContainer.semanticContentAttribute = .forceRightToLeft
            self.searchTextField.textAlignment = .right
        }
        
        self.searchTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("EnterSearchKeyWords")
        UIUtilities.changePlaceHolderTextColorForTextField(self.searchTextField, color: .white)
        
        searchViewContainerHeightConstraint.constant = 0
        blockImage.isHidden = true
        
        UIUtilities.addTapGestureToView(blockImage, withTarget: self, andSelector: #selector(dismissSearchScreen), andCanCancelTouchesInTheView: true)
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        ActivitiesDataCenter.sharedInstance.loadActivitiesWithDelegate(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(self.refreshControl.isRefreshing){
            self.refreshControl.endRefreshing()
        }
        
        let logoHeaderImage = UIImageView(image: #imageLiteral(resourceName: "logo"))
        logoHeaderImage.frame.size.height = 44
        logoHeaderImage.contentMode = .scaleAspectFit
        
       MainNavigator.sharedInstance.getViewController().navigationItem.titleView = logoHeaderImage
        
        self.dismissSearchScreen()
        searchTextField.text = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        MainNavigator.sharedInstance.getViewController().navigationItem.titleView = nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func refresh(_ sender: Any) {
        ActivitiesDataCenter.sharedInstance.reInitializeHomeDetails()
        ActivitiesDataCenter.sharedInstance.loadActivitiesWithDelegate(self, isRefreshing: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(UIUtilities.isScrollViewHitsEndOfScrollContent(scrollView,offset: -1000)){
            ActivitiesDataCenter.sharedInstance.loadActivitiesWithDelegate(self)
        }
        
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0){
            tabBarContainerHeightConstraint.constant = 40
        }else{
            tabBarContainerHeightConstraint.constant = 0
        }
        
        AnimationsUtility.animateLayoutFor(self.view, duration: 0.2, delay: 0, completionBlock: nil)
    }
    
    func dismissSearchScreen(){
        self.searchTextField.resignFirstResponder()
        self.blockImage.isHidden = true
        if(self.searchViewContainerHeightConstraint.constant != 0){
            self.searchViewContainerHeightConstraint.constant = 0            
            AnimationsUtility.animateLayoutFor(self.view, duration: 0.3, delay: 0, completionBlock: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.dismissSearchScreen()
        if(segue.identifier == "showFilterCategoriesPopup"){
            let popupSegue = segue as! CCMPopupSegue
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 64, height: 350)
            
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let sortAndFilterViewController = segue.destination as! SortAndFilterViewController
            sortAndFilterViewController.mode = .FilterCategory
            sortAndFilterViewController.delegate = self
            
        }else if (segue.identifier == "showSortPopup"){
            let popupSegue = segue as! CCMPopupSegue
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 64, height: 350)
            
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let sortAndFilterViewController = segue.destination as! SortAndFilterViewController
            sortAndFilterViewController.mode = .SortActivity
            sortAndFilterViewController.delegate = self
            
        }else if(segue.identifier == "showMapView"){
            let locationViewController = segue.destination as! LocationViewController
            locationViewController.userlatitude = self.userlatitude
            locationViewController.userlongitude = self.userlongitude
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if(identifier == "showMapView"){
            if(!CLLocationManager.locationServicesEnabled() ||
                CLLocationManager.authorizationStatus() == .denied){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("LocationServicesNotEnabled"))
                return false
            }else if(userlatitude == "" || userlongitude == ""){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("LocationServicesNotEnabled"))
                return false
            }
        }
        return true
    }
    
    @IBAction func didTapOnSearch(_ sender : AnyObject){
        self.searchTextField.becomeFirstResponder()
        self.blockImage.isHidden = false
        self.searchViewContainerHeightConstraint.constant = 40
        AnimationsUtility.animateLayoutFor(self.view, duration: 0.3, delay: 0, completionBlock: nil)
    }
}


extension HomeViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ActivitiesDataCenter.sharedInstance.activities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        let activity = ActivitiesDataCenter.sharedInstance.activities[indexPath.row]
        let companiesText = Int(activity.numberOfProviders)! > 1 ? (LocalizationManager.sharedInstance.getTranslationForKey("Companies"))! : (LocalizationManager.sharedInstance.getTranslationForKey("Company"))!
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            cell.coursesAndOffersLabel.text = activity.coursesAndOffersAr
            cell.activityName.text = activity.nameAr
            cell.activityCategories.text = activity.categoriesDescriptionAr
            
            cell.coursesAndOffersLabel.textAlignment = .right
            cell.activityName.textAlignment = .right
            cell.activityCategories.textAlignment = .right
            cell.providersLabel.textAlignment = .left
            
            cell.contentView.semanticContentAttribute = .forceRightToLeft
            cell.activityDetailContainerView.semanticContentAttribute = .forceRightToLeft
        }else{
            cell.coursesAndOffersLabel.text = activity.coursesAndOffersEn
            cell.activityName.text = activity.nameEn
            cell.activityCategories.text = activity.categoriesDescriptionEn
        }
        
        cell.providersLabel.text = ("\(activity.numberOfProviders) \(companiesText)")
        
        if(activity.image == nil){
            activity.loadImageWithDelegate(self)
        }else{
            cell.activityImage.image = activity.image
        }
        
        if(cell.coursesAndOffersLabel.text == ""){
            cell.coursesAndOffersContainerView.isHidden = true
        }else{
            cell.coursesAndOffersContainerView.isHidden = false
        }
        
        cell.providersLabel.isHidden = !activity.isShowNumberOfCompanies
        cell.activityCategories.isHidden = !activity.isShowCategories
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let activityProvidersViewController = storyboard.instantiateViewController(withIdentifier: "ActivityProvidersViewController") as! ActivityProvidersViewController
        activityProvidersViewController.activity = ActivitiesDataCenter.sharedInstance.activities[indexPath.row]
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: activityProvidersViewController)

    }
}

extension HomeViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        DispatchQueue.main.async {
            if(self.refreshControl.isRefreshing){
                self.refreshControl.endRefreshing()
            }
            self.tableView.reloadData()
        }
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension HomeViewController : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(locations.count > 0){
            let location = locations[0]
            userlongitude = "\(location.coordinate.longitude)"
            userlatitude = "\(location.coordinate.latitude)"
            locationManager.stopUpdatingLocation()
        }
    }
}

extension HomeViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.text != ""){
            self.dismissSearchScreen()
            let storyboard = UIStoryboard(name: "Activities", bundle: nil)
            let resultsViewController = storyboard.instantiateViewController(withIdentifier: "ResultsViewController") as! ResultsViewController
            resultsViewController.mode = .Search
            resultsViewController.searchKeyWords = textField.text!
            MainNavigator.sharedInstance.pushViewController(inCenterPanel: resultsViewController)
            return true
        }
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        searchTask?.cancel()
        searchTask = DispatchWorkItem {
            self.dismissSearchScreen()
            let storyboard = UIStoryboard(name: "Activities", bundle: nil)
            let resultsViewController = storyboard.instantiateViewController(withIdentifier: "ResultsViewController") as! ResultsViewController
            resultsViewController.mode = .Search
            resultsViewController.searchKeyWords = textField.text!
            MainNavigator.sharedInstance.pushViewController(inCenterPanel: resultsViewController)
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: searchTask!)
        
        return true
    }
}

extension HomeViewController : SortAndFilterDelegate{
    func didTapOnActionWithMode(_ mode: ViewMode, ids: [String]) {
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let resultsViewController = storyboard.instantiateViewController(withIdentifier: "ResultsViewController") as! ResultsViewController
        resultsViewController.mode = mode
        resultsViewController.selectedIDs = ids
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: resultsViewController)
    }
}
