//
//  LocationViewController.swift
//  EventManagement
//
//  Created by Ahmed AbdEl-Samie on 10/1/17.
//

import Foundation
import UIKit
import MapKit
import SwiftyStarRatingView

class LocationViewController: UIViewController {
    
    @IBOutlet weak var mapView : MKMapView!
    
    @IBOutlet weak var coursesAndOffersLabel : UILabel!
    @IBOutlet weak var coursesAndOffersContainerView : UIView!
    @IBOutlet weak var activityImage : UIImageView!
    @IBOutlet weak var activityName : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var depositeLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!
    @IBOutlet weak var fromLabel : UILabel!
    @IBOutlet weak var ratingView : SwiftyStarRatingView!
    @IBOutlet weak var favoriteButton : UIButton!
    @IBOutlet weak var activityDetailContainerView : UIView!
    
    @IBOutlet weak var activityProviderDetailsContainerView : UIView!
    
    var userlongitude = ""
    var userlatitude = ""
    var selectedActivityProvider : ActivityProvider?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityProvidersDataCenter.sharedInstance.clearFilterationData()
        ActivityProvidersDataCenter.sharedInstance.loadMapActivityProviderWithDelegate(self, lat: self.userlatitude, lng: self.userlongitude)
        
        let center = CLLocationCoordinate2D(latitude: Double(userlatitude)!, longitude: Double(userlongitude)!)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
        mapView.setRegion(region, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        MainNavigator.sharedInstance.setScreenTitle(LocalizationManager.sharedInstance.getTranslationForKey("MapView")!)
        MainNavigator.sharedInstance.delegate = self
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(true)
        
        if(selectedActivityProvider != nil){
            if((selectedActivityProvider?.isFavorited)!){
                self.favoriteButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
            }else{
                self.favoriteButton.setImage(#imageLiteral(resourceName: "notFavorite"), for: .normal)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {

    }
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    func showAnnotationsOnMap(){
        self.mapView.removeAnnotations(self.mapView.annotations)
        
        var annotations : [MKPointAnnotation] = []
        for activityProvider in ActivityProvidersDataCenter.sharedInstance.mapsActivitiesProviders {
            if(Double(activityProvider.lat) == nil || Double(activityProvider.lng) == nil){
               continue
            }
            let dropPin = LocationAnnotation()
                dropPin.coordinate = CLLocationCoordinate2D(latitude: Double(activityProvider.lat)!, longitude: Double(activityProvider.lng)!)
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                dropPin.title = activityProvider.nameAr
            }else{
                dropPin.title = activityProvider.nameEn
            }

            dropPin.object = activityProvider
            annotations.append(dropPin)
        }
        
        
        self.mapView.addAnnotations(annotations)
        self.mapView.showAnnotations(annotations, animated: true)
    }
    
    @IBAction func didTapProviderDetails(_ sender : AnyObject){
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let providerDetailsViewController = storyboard.instantiateViewController(withIdentifier: "ProviderDetailsViewController") as! ProviderDetailsViewController
        providerDetailsViewController.activityProviderID = (selectedActivityProvider?.id)!
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: providerDetailsViewController)
    }
    
    @IBAction func toggleFavoriteStatus(_ sender: AnyObject){
        if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
            ActivityProvidersDataCenter.sharedInstance.toggleProviderFavoriteStatusWithDelegate(self, activityProviderID: (selectedActivityProvider?.id)!, newFavStatus: ((selectedActivityProvider?.isFavorited)!) ? "0" : "1")
        }else{
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("YouNeedToSignIn"))
        }
    }
    
}

extension LocationViewController : MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) {
            //if annotation is not an MKPointAnnotation (eg. MKUserLocation),
            //return nil so map draws default view for it (eg. blue dot)...
            return nil
        }
        
        let reuseId = "locationAnnotation"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: "R")
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            // Resize image
            var pinImage = #imageLiteral(resourceName: "location").withRenderingMode(.alwaysTemplate)
            if(((annotation as! LocationAnnotation).object as! ActivityProvider).categoryImage != nil){
                pinImage = ((annotation as! LocationAnnotation).object as! ActivityProvider).categoryImage!
            }else{
                ((annotation as! LocationAnnotation).object as! ActivityProvider).loadCategoryImageWithDelegate(self)
            }
            let size = CGSize(width: 30, height: 30)
            UIGraphicsBeginImageContext(size)
            pinImage.draw(in: CGRect(x:0, y:0, width:size.width, height:size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            anView?.image = resizedImage
            anView?.canShowCallout = false
        }
        else {
            //we are re-using a view, update its annotation reference...
            anView?.annotation = annotation
        }
        
        return anView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let activityProvider = (view.annotation as! LocationAnnotation).object as! ActivityProvider
        selectedActivityProvider = activityProvider
        activityProviderDetailsContainerView.isHidden = false
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            
            coursesAndOffersLabel.text = activityProvider.coursesAndOffersAr
            activityName.text = activityProvider.nameAr
            
            coursesAndOffersLabel.textAlignment = .right
            activityName.textAlignment = .right
            ratingLabel.textAlignment = .right
            
            self.activityProviderDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            activityDetailContainerView.semanticContentAttribute = .forceRightToLeft
        }else{
            coursesAndOffersLabel.text = activityProvider.coursesAndOffersEn
            activityName.text = activityProvider.nameEn
            
            
        }
        
        ratingLabel.text = "\((activityProvider.rating))"
        
        priceLabel.text = ("\((activityProvider.averagePrice)) \((LocalizationManager.sharedInstance.getTranslationForKey("KD"))!)")
        
        if(activityProvider.hasDeposite){
            depositeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("DepositeAvailable")
        }else{
            depositeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("DepositeNotAvailable")
        }
        
        ratingView.value = CGFloat((activityProvider.rating))
        

        
        fromLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("From")
        
        if(activityProvider.image == nil){
            activityProvider.loadImageWithDelegate(self)
        }else{
            activityImage.image = activityProvider.image
        }
        
        if((activityProvider.isFavorited)){
            self.favoriteButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
        }else{
            self.favoriteButton.setImage(#imageLiteral(resourceName: "notFavorite"), for: .normal)
        }
        
        if(self.coursesAndOffersLabel.text == ""){
            self.coursesAndOffersContainerView.isHidden = true
        }else{
            self.coursesAndOffersContainerView.isHidden = false
        }
        
    }
}

extension LocationViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        if(operationID == .LoadingCategoryImage || operationID == .LoadMapActivityProviders){
            showAnnotationsOnMap()
        }else{
            if(self.mapView.selectedAnnotations.count > 0){
                activityImage.image = selectedActivityProvider?.image
            }
        }

    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if(operationID == .ToggleFavoriteStatus){
            let isError = object as! Bool
            if(isError){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("GenericError"))
            }else{
                if((selectedActivityProvider?.isFavorited)!){
                    self.favoriteButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
                }else{
                    self.favoriteButton.setImage(#imageLiteral(resourceName: "notFavorite"), for: .normal)
                }
            }
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension LocationViewController : MainNavigatorNavigationBarDelegate{
    func didTapOnBack() {
        ActivityProvidersDataCenter.sharedInstance.clearFilterationData()
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(false)
        MainNavigator.sharedInstance.popViewControllerInCenterPanel()
    }
    
    func didTapOnSort() {
        
    }
}

class LocationAnnotation : MKPointAnnotation{
    var object : Any? = nil
}
