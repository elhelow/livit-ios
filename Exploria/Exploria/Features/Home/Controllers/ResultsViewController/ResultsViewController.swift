//
//  ResultsViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/14/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit
import CCMPopup
import MapKit


class ResultsViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    var selectedIDs : [String] = []
    var searchKeyWords : String = ""
    var mode : ViewMode = .FilterCategory
    var indexForFavorite = -1
    var mainActivityID : String?
    var screenTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        ActivityProvidersDataCenter.sharedInstance.clearFilterationData()
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.tableView.semanticContentAttribute = .forceRightToLeft
        }
        
        if(mode == .FilterCategory){
            ActivityProvidersDataCenter.sharedInstance.filterActivitiesByIDsWithDelegate(self, categoryIDs: selectedIDs)
        }else if(mode == .Search){
            ActivityProvidersDataCenter.sharedInstance.searchActivitiesByKeywordWithDelegate(self, keyWord: searchKeyWords)
        }else if(mode == .SortActivity){
            ActivityProvidersDataCenter.sharedInstance.sortActivitiesWithDelegate(self, sortMethod: selectedIDs[0], mainActivityID: mainActivityID)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(mainActivityID == nil){
            let logoHeaderImage = UIImageView(image: #imageLiteral(resourceName: "logo"))
            logoHeaderImage.frame.size.height = 44
            logoHeaderImage.contentMode = .scaleAspectFit
            
            MainNavigator.sharedInstance.getViewController().navigationItem.titleView = logoHeaderImage
        }else{
            MainNavigator.sharedInstance.setScreenTitle(screenTitle)
        }
        
        MainNavigator.sharedInstance.delegate = self
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(true)
        
        self.tableView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(UIUtilities.isScrollViewHitsEndOfScrollContent(scrollView,offset: -1000)){
            if(mode == .FilterCategory){
                ActivityProvidersDataCenter.sharedInstance.filterActivitiesByIDsWithDelegate(self, categoryIDs: selectedIDs)
            }else if(mode == .Search){
                ActivityProvidersDataCenter.sharedInstance.searchActivitiesByKeywordWithDelegate(self, keyWord: searchKeyWords)
            }else if(mode == .SortActivity){
                ActivityProvidersDataCenter.sharedInstance.sortActivitiesWithDelegate(self, sortMethod: selectedIDs[0], mainActivityID: mainActivityID)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        ConnectionManager.sharedInstance.cancelAllConnections()
    }
}


extension ResultsViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(mode == .FilterCategory){
            return ActivityProvidersDataCenter.sharedInstance.filteredActivitiesByCategory.count
        }else if(mode == .Search){
            return ActivityProvidersDataCenter.sharedInstance.searchResults.count
        }else if(mode == .SortActivity){
            return ActivityProvidersDataCenter.sharedInstance.sortedActivities.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityProviderTableViewCell") as! ActivityProviderTableViewCell
        var activityProvider : ActivityProvider?
        if(mode == .FilterCategory){
            activityProvider = ActivityProvidersDataCenter.sharedInstance.filteredActivitiesByCategory[indexPath.row]
        }else if(mode == .Search){
            activityProvider = ActivityProvidersDataCenter.sharedInstance.searchResults[indexPath.row]
        }else if(mode == .SortActivity){
            activityProvider = ActivityProvidersDataCenter.sharedInstance.sortedActivities[indexPath.row]
        }
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            
            cell.coursesAndOffersLabel.text = activityProvider?.coursesAndOffersAr
            cell.activityName.text = activityProvider?.nameAr
            
            cell.coursesAndOffersLabel.textAlignment = .right
            cell.activityName.textAlignment = .right
            cell.ratingLabel.textAlignment = .right
            
            cell.contentView.semanticContentAttribute = .forceRightToLeft
            cell.activityDetailContainerView.semanticContentAttribute = .forceRightToLeft
        }else{
            cell.coursesAndOffersLabel.text = activityProvider?.coursesAndOffersEn
            cell.activityName.text = activityProvider?.nameEn
            
            
        }
        
        cell.ratingLabel.text = "\((activityProvider?.rating)!)"
        
        cell.priceLabel.text = ("\((activityProvider?.averagePrice)!) \((LocalizationManager.sharedInstance.getTranslationForKey("KD"))!)")
        
        if(activityProvider?.hasDeposite)!{
            cell.depositeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("DepositeAvailable")
        }else{
            cell.depositeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("DepositeNotAvailable")
        }
        
        cell.ratingView.value = CGFloat((activityProvider?.rating)!)
        
        cell.fromLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("From")
        
        if(activityProvider?.image == nil){
            activityProvider?.loadImageWithDelegate(self)
        }else{
            cell.activityImage.image = activityProvider?.image
        }
        
        if((activityProvider?.isFavorited)!){
            cell.favoriteButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
        }else{
            cell.favoriteButton.setImage(#imageLiteral(resourceName: "notFavorite"), for: .normal)
        }
        
        if(cell.coursesAndOffersLabel.text == ""){
            cell.coursesAndOffersContainerView.isHidden = true
        }else{
            cell.coursesAndOffersContainerView.isHidden = false
        }
        
        cell.favDelegate = self
        cell.index = indexPath.row
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let providerDetailsViewController = storyboard.instantiateViewController(withIdentifier: "ProviderDetailsViewController") as! ProviderDetailsViewController
        var activityProvider : ActivityProvider?
        if(mode == .FilterCategory){
            activityProvider = ActivityProvidersDataCenter.sharedInstance.filteredActivitiesByCategory[indexPath.row]
        }else if(mode == .Search){
            activityProvider = ActivityProvidersDataCenter.sharedInstance.searchResults[indexPath.row]
        }else if(mode == .SortActivity){
            activityProvider = ActivityProvidersDataCenter.sharedInstance.sortedActivities[indexPath.row]
        }
        providerDetailsViewController.activityProviderID = (activityProvider?.id)!
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: providerDetailsViewController)
    }
}

extension ResultsViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        self.tableView.reloadData()
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if(operationID == .ToggleFavoriteStatus){
            let isError = object as! Bool
            if(isError){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("GenericError"))
            }else{
                self.tableView.reloadData()
                
            }
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension ResultsViewController : MainNavigatorNavigationBarDelegate{
    func didTapOnBack() {
        ActivityProvidersDataCenter.sharedInstance.clearFilterationData()
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(false)
        MainNavigator.sharedInstance.popViewControllerInCenterPanel()
    }
    
    func didTapOnSort() {
        
    }
}

extension ResultsViewController : FavoriteProtocol{
    func toggleFavoriteStatusForItemWithIndex(_ index: Int) {
        if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
            indexForFavorite = index
            var activityProvider : ActivityProvider?
            if(mode == .FilterCategory){
                activityProvider = ActivityProvidersDataCenter.sharedInstance.filteredActivitiesByCategory[indexForFavorite]
            }else if(mode == .Search){
                activityProvider = ActivityProvidersDataCenter.sharedInstance.searchResults[indexForFavorite]
            }else if(mode == .SortActivity){
                activityProvider = ActivityProvidersDataCenter.sharedInstance.sortedActivities[indexForFavorite]
            }
            ActivityProvidersDataCenter.sharedInstance.toggleProviderFavoriteStatusWithDelegate(self, activityProviderID: (activityProvider?.id)!, newFavStatus: ((activityProvider?.isFavorited)!) ? "0" : "1")
        }else{
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("YouNeedToSignIn"))
        }
    }
}
