//
//  ReviewsViewConroller.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/14/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit
import CCMPopup

class ReviewsViewConroller: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var addReviewButton : UIButton!
    var activityProviderID : String = ""
    var isBooked = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addReviewButton.clipsToBounds = true
        addReviewButton.layer.cornerRadius = 10
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.tableView.semanticContentAttribute = .forceRightToLeft
        }
        
        self.addReviewButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("AddReview")!, for: .normal)
        
        self.tableView.estimatedRowHeight = 10
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        MainNavigator.sharedInstance.setScreenTitle(LocalizationManager.sharedInstance.getTranslationForKey("ActivityReview")!)
        
        MainNavigator.sharedInstance.delegate = self
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(true)
        
        ActivityProvidersDataCenter.sharedInstance.clearReviewsData()
        ActivityProvidersDataCenter.sharedInstance.loadActivityProviderReviewsWithDelegate(self, providerID: activityProviderID)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(UIUtilities.isScrollViewHitsEndOfScrollContent(scrollView,offset: -1000)){
            ActivityProvidersDataCenter.sharedInstance.loadActivityProviderReviewsWithDelegate(self, providerID: activityProviderID)
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if(identifier == "showAddReview"){
            if(UserIdentificationDataCenter.sharedInstance.loggedInUser == nil){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("YouNeedToSignIn"))
                return false
            }else if(!isBooked){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("NotBookedError"))
                return false
            }
        }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showAddReview") {
            let popupSegue = segue as! CCMPopupSegue
            
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.width-50
                , height: 420)
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let addReviewViewController = popupSegue.destination as! AddReviewViewController
            addReviewViewController.activityProviderID = self.activityProviderID
            
        }
    }
}


extension ReviewsViewConroller : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ActivityProvidersDataCenter.sharedInstance.reviews.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell") as! ReviewTableViewCell
        
        let reviewDetails = ActivityProvidersDataCenter.sharedInstance.reviews[indexPath.row]
        
        cell.reviewTitle.text = reviewDetails.name
        cell.reviewDate.text = reviewDetails.date
        cell.reviewDescription.text = reviewDetails.review
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            cell.reviewTitle.textAlignment = .right
            cell.reviewDate.textAlignment = .left
            cell.reviewDescription.textAlignment = .right
            
            cell.reviewDetailsContainerView.semanticContentAttribute = .forceRightToLeft
        }
        
        if(indexPath.row == ActivityProvidersDataCenter.sharedInstance.reviews.count - 1){
            cell.seperator.isHidden = true
        }else{
            cell.seperator.isHidden = false
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension ReviewsViewConroller : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        self.tableView.reloadData()
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension ReviewsViewConroller : MainNavigatorNavigationBarDelegate{
    func didTapOnBack() {
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(false)
        MainNavigator.sharedInstance.popViewControllerInCenterPanel()
    }
    
    func didTapOnSort() {
        
    }
}

