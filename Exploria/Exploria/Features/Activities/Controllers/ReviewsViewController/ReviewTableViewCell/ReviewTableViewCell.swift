//
//  ReviewTableViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/14/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class ReviewTableViewCell: UITableViewCell {
    @IBOutlet weak var reviewTitle : UILabel!
    @IBOutlet weak var reviewDescription : UILabel!
    @IBOutlet weak var reviewDate : UILabel!
    @IBOutlet weak var seperator : UIImageView!
    @IBOutlet weak var reviewDetailsContainerView : UIView!
    
    override func awakeFromNib() {
        
    }
    
    override func prepareForReuse() {

    }
}
