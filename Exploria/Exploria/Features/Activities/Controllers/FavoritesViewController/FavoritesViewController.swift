//
//  FavoritesViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/25/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit
import CCMPopup
import MapKit


class FavoritesViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    var indexForFavorite = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        ActivityProvidersDataCenter.sharedInstance.clearFilterationData()
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.tableView.semanticContentAttribute = .forceRightToLeft
        }
        
        ActivityProvidersDataCenter.sharedInstance.loadFavoritedActivityProvidersWithDelegate(self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MainNavigator.sharedInstance.setScreenTitle(LocalizationManager.sharedInstance.getTranslationForKey("MyFavorites")!)
        
        self.tableView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(UIUtilities.isScrollViewHitsEndOfScrollContent(scrollView,offset: -1000)){
            ActivityProvidersDataCenter.sharedInstance.loadFavoritedActivityProvidersWithDelegate(self)
        }
    }
    
}


extension FavoritesViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ActivityProvidersDataCenter.sharedInstance.favorites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityProviderTableViewCell") as! ActivityProviderTableViewCell
        let activityProvider = ActivityProvidersDataCenter.sharedInstance.favorites[indexPath.row]
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            
            cell.coursesAndOffersLabel.text = activityProvider.coursesAndOffersAr
            cell.activityName.text = activityProvider.nameAr
            
            cell.coursesAndOffersLabel.textAlignment = .right
            cell.activityName.textAlignment = .right
            cell.ratingLabel.textAlignment = .right
            
            cell.contentView.semanticContentAttribute = .forceRightToLeft
            cell.activityDetailContainerView.semanticContentAttribute = .forceRightToLeft
        }else{
            cell.coursesAndOffersLabel.text = activityProvider.coursesAndOffersEn
            cell.activityName.text = activityProvider.nameEn
            
            
        }
        
        cell.ratingLabel.text = "\((activityProvider.rating))"
        
        cell.priceLabel.text = ("\((activityProvider.averagePrice)) \((LocalizationManager.sharedInstance.getTranslationForKey("KD"))!)")
        
        if(activityProvider.hasDeposite){
            cell.depositeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("DepositeAvailable")
        }else{
            cell.depositeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("DepositeNotAvailable")
        }
        
        cell.ratingView.value = CGFloat((activityProvider.rating))
        
        cell.fromLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("From")
        
        if(activityProvider.image == nil){
            activityProvider.loadImageWithDelegate(self)
        }else{
            cell.activityImage.image = activityProvider.image
        }
        
        if((activityProvider.isFavorited)){
            cell.favoriteButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
        }else{
            cell.favoriteButton.setImage(#imageLiteral(resourceName: "notFavorite"), for: .normal)
        }
        
        if(cell.coursesAndOffersLabel.text == ""){
            cell.coursesAndOffersContainerView.isHidden = true
        }else{
            cell.coursesAndOffersContainerView.isHidden = false
        }
        
        cell.favDelegate = self
        cell.index = indexPath.row
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let providerDetailsViewController = storyboard.instantiateViewController(withIdentifier: "ProviderDetailsViewController") as! ProviderDetailsViewController
        let activityProvider = ActivityProvidersDataCenter.sharedInstance.favorites[indexPath.row]
        providerDetailsViewController.activityProviderID = (activityProvider.id)
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: providerDetailsViewController)
    }
}

extension FavoritesViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        self.tableView.reloadData()
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if(operationID == .ToggleFavoriteStatus){
            let isError = object as! Bool
            if(isError){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("GenericError"))
            }else{
                self.tableView.reloadData()
                
            }
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension FavoritesViewController : FavoriteProtocol{
    func toggleFavoriteStatusForItemWithIndex(_ index: Int) {
        if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
            indexForFavorite = index
            let activityProvider = ActivityProvidersDataCenter.sharedInstance.favorites[index]
            ActivityProvidersDataCenter.sharedInstance.toggleProviderFavoriteStatusWithDelegate(self, activityProviderID: (activityProvider.id), newFavStatus: ((activityProvider.isFavorited)) ? "0" : "1")
        }else{
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("YouNeedToSignIn"))
        }
    }
}
