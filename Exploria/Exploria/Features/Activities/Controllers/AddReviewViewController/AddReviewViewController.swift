//
//  AddReviewViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/18/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

class AddReviewViewController: UIViewController {
    
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var reviewTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var keypadHandleViewHeightConstraint : NSLayoutConstraint!
    
    var activityProviderID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendButton.clipsToBounds = true
        sendButton.layer.cornerRadius = 10
        
        emailTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("AddReviewEmailPlaceHolder")
        nameTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("AddReviewNamePlaceHolder")
        reviewTextField.placeholder = LocalizationManager.sharedInstance.getTranslationForKey("AddReviewReviewPlaceHolder")
        headerLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("AddReviewHeader")
        
        
        emailTextField.text = UserIdentificationDataCenter.sharedInstance.loggedInUser?.email ?? ""
        nameTextField.text = UserIdentificationDataCenter.sharedInstance.loggedInUser?.name ?? ""
        sendButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("AddReviewSendButton"), for: .normal)
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.emailTextField.textAlignment = .right
            self.nameTextField.textAlignment = .right
            self.headerLabel.textAlignment = .right
            self.reviewTextField.textAlignment = .right
        }
        
        UIUtilities.notifyMeWhenKeyPadWillShow(target: self, selector: #selector(updateViewScroll))
        UIUtilities.notifyMeWhenKeyPadDidChangeFrame(target: self, selector: #selector(updateViewScroll))
        UIUtilities.notifyMeWhenKeyPadWillHide(target: self, selector: #selector(updateViewScroll))
        
        UIUtilities.addTapGestureToView(self.view, withTarget: self, andSelector: #selector(dismissKeypad), andCanCancelTouchesInTheView: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateViewScroll(notification: NSNotification){
        let userInfo = notification.userInfo!
        
        let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        keypadHandleViewHeightConstraint.constant = self.view.bounds.maxY - convertedKeyboardEndFrame.minY
        if(keypadHandleViewHeightConstraint.constant < 0){
            keypadHandleViewHeightConstraint.constant = 0
        }
        AnimationsUtility.animateLayoutFor(self.view, duration: Float(animationDuration), delay: 0.0, completionBlock: nil)
    }
    
    func dismissKeypad(){
        self.emailTextField.resignFirstResponder()
        self.nameTextField.resignFirstResponder()
        self.reviewTextField.resignFirstResponder()
    }
    
    func validateAddReviewForm() -> Bool{
        if(!ValidationsUtility.isStringNotEmpty(self.nameTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("NameIsRequired"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.emailTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsRequired"))
            return false
        }else if(!ValidationsUtility.isEmailIsValid(self.emailTextField.text!, isStrict: true)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("EmailIsNotCorrect"))
            return false
        }else if(!ValidationsUtility.isStringNotEmpty(self.reviewTextField.text!)){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("ReviewIsRequired"))
            return false
        }
        
        return true
    }
    
    @IBAction func sendReview(_ sender: UIButton) {
        self.dismissKeypad()
        if(self.validateAddReviewForm()){
            ActivityProvidersDataCenter.sharedInstance.addReviewWithDelegate(self, providerID: self.activityProviderID, name: nameTextField.text!, email: emailTextField.text!, review: reviewTextField.text!)
        }
    }
    
}

extension AddReviewViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.dismissKeypad()
        self.sendReview(UIButton())
        return true
    }
}


extension AddReviewViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if((object as! Bool)){
            self.dismissAnimated()
        }else{
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("NotBookedError"))
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
    
}
