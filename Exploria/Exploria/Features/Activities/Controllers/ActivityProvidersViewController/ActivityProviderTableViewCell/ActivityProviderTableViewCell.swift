//
//  ActivityProviderTableViewCell.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/10/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import SwiftyStarRatingView



class ActivityProviderTableViewCell: UITableViewCell {
    @IBOutlet weak var coursesAndOffersLabel : UILabel!
    @IBOutlet weak var coursesAndOffersContainerView : UIView!
    @IBOutlet weak var activityImage : UIImageView!
    @IBOutlet weak var activityName : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var depositeLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!
    @IBOutlet weak var fromLabel : UILabel!
    @IBOutlet weak var ratingView : SwiftyStarRatingView!
    @IBOutlet weak var favoriteButton : UIButton!
    @IBOutlet weak var activityDetailContainerView : UIView!
    
    weak var favDelegate : FavoriteProtocol?
    var index = 0
    
    override func awakeFromNib() {
        
    }
    
    override func prepareForReuse() {
        self.activityImage.image = #imageLiteral(resourceName: "imagePlaceHolder")
    }
    
    @IBAction func toggleFavoriteStatus(_ sender: AnyObject){
        favDelegate?.toggleFavoriteStatusForItemWithIndex(index)
    }

}

