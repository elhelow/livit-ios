//
//  ActivityProvidersViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/10/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit
import CCMPopup

class ActivityProvidersViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    var activity : Activity?
    var indexForFavorite = -1
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.tableView.semanticContentAttribute = .forceRightToLeft
        }
        
        ActivityProvidersDataCenter.sharedInstance.loadActivityProviderWithDelegate(self, activityID: (activity?.id)!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(self.refreshControl.isRefreshing){
            self.refreshControl.endRefreshing()
        }
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            MainNavigator.sharedInstance.setScreenTitle((activity?.nameAr)!)
        }else{
            MainNavigator.sharedInstance.setScreenTitle((activity?.nameEn)!)
        }
        
        MainNavigator.sharedInstance.delegate = self
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(true)
        MainNavigator.sharedInstance.setupNavigationBarStyleWithSortingButtonEnabled()
        
        self.tableView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func refresh(_ sender: Any) {
        ActivityProvidersDataCenter.sharedInstance.clearLoadingDetailsForActivityWithID((activity?.id)!)
        ActivityProvidersDataCenter.sharedInstance.loadActivityProviderWithDelegate(self, activityID: (activity?.id)!, isRefreshing: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(UIUtilities.isScrollViewHitsEndOfScrollContent(scrollView,offset: -1000)){
            ActivityProvidersDataCenter.sharedInstance.loadActivityProviderWithDelegate(self, activityID: (activity?.id)!)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showSortPopup"){
            let popupSegue = segue as! CCMPopupSegue
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 64, height: 350)
            
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let sortAndFilterViewController = segue.destination as! SortAndFilterViewController
            sortAndFilterViewController.mode = .SortActivity
            sortAndFilterViewController.delegate = self
            
        }
    }
}


extension ActivityProvidersViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(ActivityProvidersDataCenter.sharedInstance.activitiesProviders[activity!.id] != nil){
            return ActivityProvidersDataCenter.sharedInstance.activitiesProviders[activity!.id]!.count
        }
        
        return 0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityProviderTableViewCell") as! ActivityProviderTableViewCell
        
        let activityProvider = ActivityProvidersDataCenter.sharedInstance.activitiesProviders[(activity?.id)!]?[indexPath.row]
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            
            cell.coursesAndOffersLabel.text = activityProvider?.coursesAndOffersAr
            cell.activityName.text = activityProvider?.nameAr
            
            cell.coursesAndOffersLabel.textAlignment = .right
            cell.activityName.textAlignment = .right
            cell.ratingLabel.textAlignment = .right
            
            cell.contentView.semanticContentAttribute = .forceRightToLeft
            cell.activityDetailContainerView.semanticContentAttribute = .forceRightToLeft
        }else{
            cell.coursesAndOffersLabel.text = activityProvider?.coursesAndOffersEn
            cell.activityName.text = activityProvider?.nameEn
            
            
        }
        
        cell.ratingLabel.text = "\((activityProvider?.rating)!)"
        
        cell.priceLabel.text = ("\((activityProvider?.averagePrice)!) \((LocalizationManager.sharedInstance.getTranslationForKey("KD"))!)")
        
        if(activityProvider?.hasDeposite)!{
            cell.depositeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("DepositeAvailable")
        }else{
            cell.depositeLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("DepositeNotAvailable")
        }
        
        cell.ratingView.value = CGFloat((activityProvider?.rating)!)
        
        cell.fromLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("From")
        
        if(activityProvider?.image == nil){
            activityProvider?.loadImageWithDelegate(self)
        }else{
            cell.activityImage.image = activityProvider?.image
        }
        
        if((activityProvider?.isFavorited)!){
           cell.favoriteButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
        }else{
           cell.favoriteButton.setImage(#imageLiteral(resourceName: "notFavorite"), for: .normal)
        }
        
        if(cell.coursesAndOffersLabel.text == ""){
            cell.coursesAndOffersContainerView.isHidden = true
        }else{
            cell.coursesAndOffersContainerView.isHidden = false
        }
        
        cell.favDelegate = self
        cell.index = indexPath.row
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let providerDetailsViewController = storyboard.instantiateViewController(withIdentifier: "ProviderDetailsViewController") as! ProviderDetailsViewController
        let activityProvider = ActivityProvidersDataCenter.sharedInstance.activitiesProviders[(activity?.id)!]?[indexPath.row]
        providerDetailsViewController.activityProviderID = (activityProvider?.id)!
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: providerDetailsViewController)
    }
}

extension ActivityProvidersViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        DispatchQueue.main.async {
            if(self.refreshControl.isRefreshing){
                self.refreshControl.endRefreshing()
            }
            self.tableView.reloadData()
        }
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if(operationID == .ToggleFavoriteStatus){
            let isError = object as! Bool
            if(isError){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("GenericError"))
            }else{
                DispatchQueue.main.async {
                    if(self.refreshControl.isRefreshing){
                        self.refreshControl.endRefreshing()
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension ActivityProvidersViewController : MainNavigatorNavigationBarDelegate{
    func didTapOnBack() {
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(false)
        MainNavigator.sharedInstance.popViewControllerInCenterPanel()
    }
    
    func didTapOnSort() {
        self.performSegue(withIdentifier: "showSortPopup", sender: nil)
    }
}

extension ActivityProvidersViewController : FavoriteProtocol{
    func toggleFavoriteStatusForItemWithIndex(_ index: Int) {
        if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
            indexForFavorite = index
            let activityProvider = ActivityProvidersDataCenter.sharedInstance.activitiesProviders[(activity?.id)!]?[indexForFavorite]
            ActivityProvidersDataCenter.sharedInstance.toggleProviderFavoriteStatusWithDelegate(self, activityProviderID: (activityProvider?.id)!, newFavStatus: ((activityProvider?.isFavorited)!) ? "0" : "1")
        }else{
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("YouNeedToSignIn"))
        }
    }
}

extension ActivityProvidersViewController : SortAndFilterDelegate{
    func didTapOnActionWithMode(_ mode: ViewMode, ids: [String]) {
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let resultsViewController = storyboard.instantiateViewController(withIdentifier: "ResultsViewController") as! ResultsViewController
        resultsViewController.mode = mode
        resultsViewController.selectedIDs = ids
        resultsViewController.mainActivityID = activity?.id
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            resultsViewController.screenTitle = activity?.nameAr ?? ""
        }else{
            resultsViewController.screenTitle = activity?.nameEn ?? ""
        }
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: resultsViewController)
    }
}

