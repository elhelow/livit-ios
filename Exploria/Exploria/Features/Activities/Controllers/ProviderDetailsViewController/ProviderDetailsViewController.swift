//
//  ProviderDetailsViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/30/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit
import SwiftyStarRatingView
import MapKit
import CCMPopup

class ProviderDetailsViewController: UIViewController {
    
    @IBOutlet weak var providerName : UILabel!
    @IBOutlet weak var providerImage : UIImageView!
    @IBOutlet weak var favButton : UIButton!
    @IBOutlet weak var topDetailsContainerView : UIView!

    @IBOutlet weak var offersContainerView : UIView!
    @IBOutlet weak var offersContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var offersTitle : UILabel!
    @IBOutlet weak var offersDetails : UILabel!
    
    @IBOutlet weak var coursesContainerView : UIView!
    @IBOutlet weak var coursesContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var coursesTitle : UILabel!
    @IBOutlet weak var coursesDetails : UILabel!
    
    @IBOutlet weak var rateContainerView : UIView!
    @IBOutlet weak var rateContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var rateTitle : UILabel!
    @IBOutlet weak var rateValue : UILabel!
    @IBOutlet weak var rateDetailsContainerView : UIView!
    @IBOutlet weak var ratingView : SwiftyStarRatingView!
    @IBOutlet weak var rateDetailsContainerViewHeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var descriptionContainerView : UIView!
    @IBOutlet weak var descriptionContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var descriptionTitle : UILabel!
    @IBOutlet weak var descriptionDetails : UILabel!
    
    @IBOutlet weak var dynamicContainerView : UIView!
    @IBOutlet weak var dynamicContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var dynamicSampleContainerView : UIView!
    @IBOutlet weak var dynamicSampleContainerViewBottomConstraint : NSLayoutConstraint!
    @IBOutlet weak var dynamicSampleTitle : UILabel!
    @IBOutlet weak var dynamicSampleDetails : UILabel!
    
    @IBOutlet weak var reviewContainerView : UIView!
    @IBOutlet weak var reviewSectionTitle : UILabel!
    @IBOutlet weak var reviewDate : UILabel!
    @IBOutlet weak var reviewTitle : UILabel!
    @IBOutlet weak var reviewDetails : UILabel!
    @IBOutlet weak var reviewArrow : UIImageView!
    @IBOutlet weak var reviewDetailsContainerView : UIView!
    @IBOutlet weak var reviewDetailsContainerViewHeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var locationContainerView : UIView!
    @IBOutlet weak var locationTitle : UILabel!
    @IBOutlet weak var locationMapView : MKMapView!
    
    @IBOutlet weak var bookingContainerView : UIView!
    @IBOutlet weak var bookingContainerViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var bookingActivity : UIButton!
    @IBOutlet weak var bookingCourse : UIButton!
    
    @IBOutlet weak var playIconContainer : UIView!
    
    var activityProviderID : String = "8"
    var providerDetails : ActivityProviderDetails?
    var shouldReload = true
    
    override func viewDidLoad() {
        
        bookingActivity.clipsToBounds = true
        bookingActivity.layer.cornerRadius = 10
        
        bookingCourse.clipsToBounds = true
        bookingCourse.layer.cornerRadius = 10
        
        self.playIconContainer.isHidden = true
        
        self.offersTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("LatestOffers")
        self.coursesTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("Courses")
        self.rateTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("RateThisActivity")
        self.descriptionTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("ActivityDescription")
        self.reviewSectionTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("WriteAReview")
        self.locationTitle.text = LocalizationManager.sharedInstance.getTranslationForKey("LocationOnTheMap")
        self.bookingActivity.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("BookActivity"), for: .normal)
        self.bookingCourse.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("BookCourse"), for: .normal)
        
        self.providerName.text = ""
        self.offersDetails.text = ""
        self.coursesDetails.text = ""
        self.descriptionDetails.text = ""
        self.dynamicSampleTitle.text = ""
        self.dynamicSampleDetails.text = ""
        self.reviewTitle.text = ""
        self.reviewDetails.text = ""
        self.reviewDate.text = ""
        self.ratingView.value = CGFloat(0.0)
        self.rateValue.text = "0.0"
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.topDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.offersContainerView.semanticContentAttribute = .forceRightToLeft
            self.coursesContainerView.semanticContentAttribute = .forceRightToLeft
            self.rateContainerView.semanticContentAttribute = .forceRightToLeft
            self.dynamicSampleContainerView.semanticContentAttribute = .forceRightToLeft
            self.locationContainerView.semanticContentAttribute = .forceRightToLeft
            self.reviewContainerView.semanticContentAttribute = .forceRightToLeft
            self.reviewDetailsContainerView.semanticContentAttribute = .forceRightToLeft
            self.bookingContainerView.semanticContentAttribute = .forceRightToLeft

            self.providerName.textAlignment = .right
            self.offersTitle.textAlignment = .right
            self.offersDetails.textAlignment = .right
            self.coursesTitle.textAlignment = .right
            self.coursesDetails.textAlignment = .right
            self.rateTitle.textAlignment = .right
            self.descriptionTitle.textAlignment = .right
            self.descriptionDetails.textAlignment = .right
            self.dynamicSampleTitle.textAlignment = .right
            self.dynamicSampleDetails.textAlignment = .right
            self.reviewSectionTitle.textAlignment = .right
            self.reviewTitle.textAlignment = .right
            self.reviewDetails.textAlignment = .right
            self.reviewDate.textAlignment = .left
            self.locationTitle.textAlignment = .right
            
            UIUtilities.rotateView(reviewArrow, withDegrees: Double.pi)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(providerDetails != nil){
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                MainNavigator.sharedInstance.setScreenTitle((providerDetails?.nameAr)!)
            }else{
                MainNavigator.sharedInstance.setScreenTitle((providerDetails?.nameEn)!)
            }
        }else{
            MainNavigator.sharedInstance.setScreenTitle("")
        }

        
        MainNavigator.sharedInstance.delegate = self
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(true)
        
        if(shouldReload){
            ActivityProvidersDataCenter.sharedInstance.loadActivityProviderDetailsWithDelegate(self, providerID: activityProviderID)
        }
    }
    
    func reloadData(){
        if(providerDetails != nil){
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                MainNavigator.sharedInstance.setScreenTitle((providerDetails?.nameAr)!)
            }else{
                MainNavigator.sharedInstance.setScreenTitle((providerDetails?.nameEn)!)
            }
            
            if(!(providerDetails?.hasCourses)!){
                if(bookingCourse != nil){
                    bookingCourse.removeFromSuperview()
                }
            }
        }else{
            MainNavigator.sharedInstance.setScreenTitle("")
        }

        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.providerName.text = providerDetails?.nameAr
            if(ValidationsUtility.isStringNotEmpty((providerDetails?.offersAr)!)){
                self.offersDetails.text = providerDetails?.offersAr
            }else{
                self.offersContainerViewHeightConstraint.priority = UILayoutPriorityDefaultHigh
            }
            if(ValidationsUtility.isStringNotEmpty((providerDetails?.coursesAr)!)){
                self.coursesDetails.text = providerDetails?.coursesAr
            }else{
                self.coursesContainerViewHeightConstraint.priority = UILayoutPriorityDefaultHigh
            }
            if(ValidationsUtility.isStringNotEmpty((providerDetails?.descriptionAr)!)){
                self.descriptionDetails.text = providerDetails?.descriptionAr
            }else{
                self.descriptionContainerViewHeightConstraint.priority = UILayoutPriorityDefaultHigh
            }
        }else{
            self.providerName.text = providerDetails?.nameEn
            if(ValidationsUtility.isStringNotEmpty((providerDetails?.coursesEn)!)){
                self.offersDetails.text = providerDetails?.offersEn
            }else{
                self.offersContainerViewHeightConstraint.priority = UILayoutPriorityDefaultHigh
            }
            if(ValidationsUtility.isStringNotEmpty((providerDetails?.coursesEn)!)){
                self.coursesDetails.text = providerDetails?.coursesEn
            }else{
                self.coursesContainerViewHeightConstraint.priority = UILayoutPriorityDefaultHigh
            }
            if(ValidationsUtility.isStringNotEmpty((providerDetails?.descriptionEn)!)){
                self.descriptionDetails.text = providerDetails?.descriptionEn
            }else{
                self.descriptionContainerViewHeightConstraint.priority = UILayoutPriorityDefaultHigh
            }
        }
        
        
        if((providerDetails?.dynamicContent.count)! > 0 && self.dynamicSampleContainerViewBottomConstraint != nil){
            var index = 0
            var lastView : UIView?
            for dynamicDetails in (providerDetails?.dynamicContent)!{
                var newView : UIView?
                if(index == 0){
                    newView = dynamicSampleContainerView
                }else{
                    if(index == 1){
                        dynamicContainerView.removeConstraint(self.dynamicSampleContainerViewBottomConstraint)
                    }
                    newView = (UIUtilities.makeACopyOfView(dynamicSampleContainerView) as! UIView)
                    self.dynamicContainerView.addSubview(newView!)
                    ConstraintsUtility.addTopConstraintToBottom(superView: self.dynamicContainerView, view: newView!, relatedView: lastView!, constant: 0, relation: .equal)
                    ConstraintsUtility.addLeadingConstraintTo(superView: self.dynamicContainerView, view:newView! , relatedView: self.dynamicContainerView, constant: 0, relation: .equal)
                    ConstraintsUtility.addTrailingConstraintTo(superView: self.dynamicContainerView, view: newView!, relatedView: self.dynamicContainerView, constant: 0, relation: .equal)
                }
                
                if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                    (newView?.viewWithTag(1) as! UILabel).text = dynamicDetails.titleAr
                    (newView?.viewWithTag(2) as! UILabel).text = dynamicDetails.descriptionAr
                }else{
                    (newView?.viewWithTag(1) as! UILabel).text = dynamicDetails.titleEn
                    (newView?.viewWithTag(2) as! UILabel).text = dynamicDetails.descriptionEn
                }
                
                index = index + 1
                lastView = newView
            }
            if(index > 1){
                ConstraintsUtility.addBottomConstraintTo(superView: self.dynamicContainerView, view: lastView!, relatedView: self.dynamicContainerView, constant: 0, relation: .equal)
            }
        }else if(self.dynamicSampleContainerViewBottomConstraint != nil){
            self.dynamicContainerViewHeightConstraint.priority = UILayoutPriorityDefaultHigh
        }
        
        if(providerDetails?.review != nil){
            self.reviewTitle.text = providerDetails?.review?.name
            self.reviewDetails.text = providerDetails?.review?.review
            self.reviewDate.text = providerDetails?.review?.date
        }else{
            self.reviewDetailsContainerViewHeightConstraint.priority = UILayoutPriorityDefaultHigh
        }
        
        self.ratingView.value = CGFloat((providerDetails?.rating)!)
        self.rateValue.text = "\(providerDetails?.rating ?? 0.0)"
        
        if(providerDetails?.image != nil){
            self.providerImage.image = providerDetails?.image
        }else{
            providerDetails?.loadImageWithDelegate(self)
        }
        

        if(Double((providerDetails?.lat)!) != nil && Double((providerDetails?.lng)!) != nil){
            var annotations : [MKPointAnnotation] = []
            
            let dropPin = MKPointAnnotation()
            dropPin.coordinate = CLLocationCoordinate2D(latitude: Double((providerDetails?.lat)!)!, longitude: Double((providerDetails?.lng)!)!)
            if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
                dropPin.title = providerDetails?.nameAr
            }else{
                dropPin.title = providerDetails?.nameEn
            }
            
            annotations.append(dropPin)
            
            self.locationMapView.addAnnotations(annotations)
            
            let center = CLLocationCoordinate2D(latitude: Double((providerDetails?.lat)!)!, longitude: Double((providerDetails?.lng)!)!)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
            self.locationMapView.setRegion(region, animated: true)
        }
        
        if((providerDetails?.isFavorited)!){
            self.favButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
        }else{
            self.favButton.setImage(#imageLiteral(resourceName: "notFavorite"), for: .normal)
        }
        
        if((providerDetails?.videoLink ?? "") != ""){
            self.playIconContainer.isHidden = false
        }else{
            self.playIconContainer.isHidden = true
        }
        
    }
    
    @IBAction func playVideo(_ sender: AnyObject){
        VideoUtility.playVideo(withURL: (providerDetails?.videoLink)!)
    }
    
    @IBAction func viewGallery(_ sender: AnyObject){
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let galleryViewController = storyboard.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
        galleryViewController.videoLink = (providerDetails?.videoLink)!
        galleryViewController.dataSource = (providerDetails?.gallery)!
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: galleryViewController)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if(identifier == "showRating"){
            if(UserIdentificationDataCenter.sharedInstance.loggedInUser == nil){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("YouNeedToSignIn"))
                return false
            }else if(!(providerDetails?.isBooked)!){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("NotBookedError"))
                return false
            }
        }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showRating") {
            shouldReload = true
            let popupSegue = segue as! CCMPopupSegue
            
            popupSegue.destinationBounds = CGRect(x: 0, y: 0, width: self.view.frame.width-50
                , height: 210)
            popupSegue.backgroundBlurRadius = 0.7
            popupSegue.backgroundViewAlpha = 0.7
            popupSegue.backgroundViewColor = UIColor.black
            popupSegue.dismissableByTouchingBackground = true
            
            let ratingViewController = popupSegue.destination as! RatingViewController
            ratingViewController.activityProviderID = self.activityProviderID
            
        }
    }
    
    
    @IBAction func viewReviews(_ sender: AnyObject){
        shouldReload = true
        let storyboard = UIStoryboard(name: "Activities", bundle: nil)
        let reviewsViewConroller = storyboard.instantiateViewController(withIdentifier: "ReviewsViewConroller") as! ReviewsViewConroller
        reviewsViewConroller.activityProviderID = self.activityProviderID
        reviewsViewConroller.isBooked = self.providerDetails?.isBooked ?? false
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: reviewsViewConroller)
    }
    
    @IBAction func toggleFavoriteStatus(_ sender: AnyObject){
        if(UserIdentificationDataCenter.sharedInstance.loggedInUser != nil){
            ActivityProvidersDataCenter.sharedInstance.toggleProviderFavoriteStatusWithDelegate(self, activityProviderID: (providerDetails?.providerID)!, newFavStatus: ((providerDetails?.isFavorited)!) ? "0" : "1")
        }else{
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("YouNeedToSignIn"))
        }
    }
    
    @IBAction func bookActivity(_ sender: AnyObject){
        shouldReload = false
        let storyboard = UIStoryboard(name: "Booking", bundle: nil)
        let bookActivityViewController = storyboard.instantiateViewController(withIdentifier: "BookActivityViewController") as! BookActivityViewController
        bookActivityViewController.providerID = (providerDetails?.providerID)!
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            bookActivityViewController.activityName = (providerDetails?.nameAr)!
        }else{
            bookActivityViewController.activityName = (providerDetails?.nameEn)!
        }
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: bookActivityViewController)
    }
    
    @IBAction func bookCourse(_ sender: AnyObject){
        shouldReload = false
        let storyboard = UIStoryboard(name: "Booking", bundle: nil)
        let bookCourseViewController = storyboard.instantiateViewController(withIdentifier: "BookCourseViewController") as! BookCourseViewController
        bookCourseViewController.providerID = (providerDetails?.providerID)!
        MainNavigator.sharedInstance.pushViewController(inCenterPanel: bookCourseViewController)
    }
}

extension ProviderDetailsViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        if(providerDetails?.image != nil){
            self.providerImage.image = providerDetails?.image
        }
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if(operationID == .ToggleFavoriteStatus){
            let isError = object as! Bool
            if(isError){
                AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("GenericError"))
            }else{
                providerDetails?.isFavorited = !((providerDetails?.isFavorited)!)
                if((providerDetails?.isFavorited)!){
                    self.favButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
                }else{
                    self.favButton.setImage(#imageLiteral(resourceName: "notFavorite"), for: .normal)
                }
            }
        }else{
            providerDetails = (object as! ActivityProviderDetails)
            self.reloadData()
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}

extension ProviderDetailsViewController : MainNavigatorNavigationBarDelegate{
    func didTapOnBack() {
        MainNavigator.sharedInstance.changeBackButtonStatusToStatusShow(false)
        MainNavigator.sharedInstance.popViewControllerInCenterPanel()
    }
    
    func didTapOnSort() {
        
    }
}

extension ProviderDetailsViewController : MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) {
            //if annotation is not an MKPointAnnotation (eg. MKUserLocation),
            //return nil so map draws default view for it (eg. blue dot)...
            return nil
        }
        
        let reuseId = "locationAnnotation"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: "R")
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            // Resize image
            let pinImage = #imageLiteral(resourceName: "detailsPin")
            
            let size = CGSize(width: 30, height: 40)
            UIGraphicsBeginImageContext(size)
            pinImage.draw(in: CGRect(x:0, y:0, width:size.width, height:size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            anView?.image = resizedImage
            anView?.canShowCallout = false
        }
        else {
            //we are re-using a view, update its annotation reference...
            anView?.annotation = annotation
        }
        
        return anView
    }
}
