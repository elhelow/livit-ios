//
//  RatingViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/18/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation
import SwiftyStarRatingView

class RatingViewController: UIViewController {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var sendButton: UIButton!
    
    var activityProviderID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendButton.clipsToBounds = true
        sendButton.layer.cornerRadius = 10
        
        headerLabel.text = LocalizationManager.sharedInstance.getTranslationForKey("RateThisActivity")
        
        sendButton.setTitle(LocalizationManager.sharedInstance.getTranslationForKey("RatingSendButton"), for: .normal)
        
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .arabic){
            self.headerLabel.textAlignment = .right
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validateRatingForm() -> Bool{
        if(ratingView.value == 0){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("RatingIsRequired"))
            return false
        }
        
        return true
    }
    
    @IBAction func sendRating(_ sender: UIButton) {
        if(self.validateRatingForm()){
            ActivityProvidersDataCenter.sharedInstance.addRatingWithDelegate(self, providerID: self.activityProviderID, rate: "\(self.ratingView.value)")
        }
    }
    
}

extension RatingViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {
        if((object as! Int) == 1){
            self.dismissAnimated()
        }else if((object as! Int) == 0){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("NotBookedError"))
        }else if((object as! Int) == 2){
            AlertUtility.showErrorAlert(LocalizationManager.sharedInstance.getTranslationForKey("AlreadyRated"))
        }
    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
    
}
