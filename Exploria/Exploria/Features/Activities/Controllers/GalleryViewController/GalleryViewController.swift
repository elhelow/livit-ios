//
//  GalleryViewController.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/31/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import SwiftPhotoGallery

class GalleryViewController : UIViewController {
    @IBOutlet weak var videoThumbnail : UIImageView!
    @IBOutlet weak var videoContainerView : UIView!
    @IBOutlet weak var videoContainerViewHeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var collectionView : UICollectionView!
    
    var videoLink : String = ""
    var dataSource : [GalleryItem] = []
    var gallery : SwiftPhotoGallery? = nil
    
    override func viewDidLoad() {
        MainNavigator.sharedInstance.setScreenTitle(LocalizationManager.sharedInstance.getTranslationForKey("Gallery")!)
        if(!ValidationsUtility.isStringNotEmpty(videoLink)){
            self.videoContainerViewHeightConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func playVideo(_ sender: AnyObject){
        VideoUtility.playVideo(withURL: videoLink)
    }
    
}

class GalleryCell : UICollectionViewCell {
    @IBOutlet weak var imageView : UIImageView!
    
    override func prepareForReuse() {
        self.imageView.image = #imageLiteral(resourceName: "imagePlaceHolder")
    }
}

extension GalleryViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as? GalleryCell
        let galleryItem = dataSource[indexPath.row]
        
        if(galleryItem.image != nil){
            cell?.imageView.image = galleryItem.image
        }else{
            galleryItem.loadImageWithDelegate(self)
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 10, 10, 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: floor((view.frame.size.width - 30) / 2), height: 161)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        gallery = SwiftPhotoGallery(delegate: self, dataSource: self)
        gallery?.backgroundColor = UIColor.black
        gallery?.pageIndicatorTintColor = UIColor.gray.withAlphaComponent(0.5)
        gallery?.currentPageIndicatorTintColor = UIColor.white
        gallery?.hidePageControl = false
        gallery?.currentPage = indexPath.row
        gallery?.view.alpha = 1.0
        present(gallery!, animated: true, completion: nil)
    }
}

extension GalleryViewController : OperationDelegate{
    
    func didFinishOperation(_ operationID: OperationID) {
        self.gallery?.imageCollectionView.reloadData()
        self.collectionView.reloadData()
    }
    
    func didFinishOperation(_ operationID: OperationID, object: AnyObject) {

    }
    
    func didRecieveErrorForOperation(_ operationID: OperationID, andWithError error: Error?) {
        
    }
    
    func didCancelOperation(_ operationID: OperationID) {
        
    }
}


extension GalleryViewController : SwiftPhotoGalleryDelegate, SwiftPhotoGalleryDataSource{
    func numberOfImagesInGallery(gallery: SwiftPhotoGallery) -> Int {
        return dataSource.count
    }
    
    func imageInGallery(gallery: SwiftPhotoGallery, forIndex: Int) -> UIImage? {
        if(dataSource[forIndex].image != nil){
            return dataSource[forIndex].image
        }
        return #imageLiteral(resourceName: "imagePlaceHolder")
    }
    
    func galleryDidTapToClose(gallery: SwiftPhotoGallery) {
        dismiss(animated: true, completion: nil)
    }
}
