//
//  AppDelegate.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 9/28/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UINavigationBar.appearance().barTintColor = ColorsUtility.colorWithHexString("#1e1f44")
        UINavigationBar.appearance().tintColor = .white
        
        UINavigationBar.appearance().isTranslucent = false
        
        let pre = NSLocale.preferredLanguages[0]
        
        if(pre.contains("ar") && !LocalizationManager.sharedInstance.isCachedLanguageExists()){
            LocalizationManager.sharedInstance.changeCurrentLanguageTo(.arabic)
        }
        
        self.updateTitleFont()
        
        User.setUserIfIsAlreadyLoggedIn()
        
        Thread.sleep(forTimeInterval: 2.0)
        
        return true
    }
    
    func updateTitleFont(){
        let fontSize = CGFloat(20.0)
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: "Dosis-Light", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)]
        }else{
            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: "DroidArabicKufi", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)]
        }
    }
    
    func updateCurrentTitleFont(){
        let fontSize = CGFloat(20.0)
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            MainNavigator.sharedInstance.getViewController().navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: "Dosis-Light", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)]
        }else{
            MainNavigator.sharedInstance.getViewController().navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: "DroidArabicKufi", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)]
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func setMainNavigatorAsRootViewController(){
        let navigationController = LighStatusBarNavigationController(rootViewController: MainNavigator.sharedInstance.getViewController())
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            MainNavigator.sharedInstance.changeMenuToTheLeft()
        }else{
            MainNavigator.sharedInstance.changeMenuToTheRight()
        }
        self.window?.rootViewController = navigationController
        UIView.transition(with: self.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: nil, completion: nil)
    }

}

class LighStatusBarNavigationController : UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}

