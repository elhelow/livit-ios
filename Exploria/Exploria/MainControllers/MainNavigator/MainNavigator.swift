//
//  MainNavigator.swift
//  EventManagement
//
//  Created by Ahmed AbdEl-Samie on 9/25/17.
//

import Foundation
import UIKit
import LGSideMenuController

protocol MainNavigatorNavigationBarDelegate: NSObjectProtocol {
    func didTapOnBack()
    func didTapOnSort()
}

class MainNavigator: NSObject {
    
    private var isMenuToTheRight = false
    private let mainSlidingNavigator : LGSideMenuController = LGSideMenuController()
    weak var delegate : MainNavigatorNavigationBarDelegate?
    
    static let sharedInstance: MainNavigator = {
        let sharedInstance = MainNavigator()
        sharedInstance.initWithInitialViews()
        sharedInstance.setupNavigationBarStyleWithBackButtonEnabled(false)
        return sharedInstance
    }()
    
    fileprivate func initWithInitialViews(){
//        mainSlidingNavigator.shouldDelegateAutorotateToVisiblePanel = false;
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        self.mainSlidingNavigator.leftViewWidth = 300
        self.mainSlidingNavigator.rightViewWidth = 300
        self.changeCenterPanel(to: homeViewController)
    }
    
    func setupNavigationBarStyleWithBackButtonEnabled(_ isBackButtonEnabled : Bool) {
        let menuIcon = UIImageView(image: #imageLiteral(resourceName: "menuIcon"))
        menuIcon.contentMode = .scaleAspectFit
        menuIcon.clipsToBounds = true
        var frame: CGRect = menuIcon.frame
        frame.size.width = 25
        menuIcon.frame = frame
        let tapper = UITapGestureRecognizer(target: self, action: #selector(self.openMenu))
        menuIcon.addGestureRecognizer(tapper)
        let menuItem = UIBarButtonItem(customView: menuIcon)
        
        let backIcon = UIImageView(image: #imageLiteral(resourceName: "backIcon").withRenderingMode(.alwaysTemplate))
        backIcon.contentMode = .scaleAspectFit
        backIcon.tintColor = .white
        backIcon.clipsToBounds = true
        var frame2: CGRect = backIcon.frame
        frame2.size.width = 25
        backIcon.frame = frame2
        let tapper2 = UITapGestureRecognizer(target: self, action: #selector(self.back))
        backIcon.addGestureRecognizer(tapper2)
        let backItem = UIBarButtonItem(customView: backIcon)
        
        if isMenuToTheRight {
            if(isBackButtonEnabled){
                mainSlidingNavigator.navigationItem.leftBarButtonItems = [backItem]
                mainSlidingNavigator.navigationItem.rightBarButtonItems = []
            }else{
                mainSlidingNavigator.navigationItem.leftBarButtonItems = []
                mainSlidingNavigator.navigationItem.rightBarButtonItems = [menuItem]
            }
        }
        else {
            if(isBackButtonEnabled){
                mainSlidingNavigator.navigationItem.leftBarButtonItems = [backItem]
            }else{
                mainSlidingNavigator.navigationItem.leftBarButtonItems = [menuItem]
            }
            mainSlidingNavigator.navigationItem.rightBarButtonItems = []
        }
    }
    
    func setupNavigationBarStyleWithSortingButtonEnabled() {
        let sortingIcon = UIImageView(image: #imageLiteral(resourceName: "sortIcon").withRenderingMode(.alwaysTemplate))
        sortingIcon.contentMode = .scaleAspectFit
        sortingIcon.clipsToBounds = true
        var frame: CGRect = sortingIcon.frame
        frame.size.width = 30
        frame.size.height = 30
        sortingIcon.frame = frame
        let tapper = UITapGestureRecognizer(target: self, action: #selector(self.sort))
        sortingIcon.addGestureRecognizer(tapper)
        let sortItem = UIBarButtonItem(customView: sortingIcon)
        
        mainSlidingNavigator.navigationItem.rightBarButtonItems = [sortItem]
    }
    
    var prefersStatusBarHidden: Bool {
        return false
    }
    
    func getViewController() -> UIViewController {
        return mainSlidingNavigator
    }
    
    func getCenteralPanelViewController() -> UIViewController {
        return mainSlidingNavigator.rootViewController!
    }
    
    func changeCenterPanel(to viewController: UIViewController) {
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.isNavigationBarHidden = true
        mainSlidingNavigator.rootViewController = navigationController
    }
    
    func pushViewController(inCenterPanel viewController: UIViewController) {
        (mainSlidingNavigator.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: true)
    }
    
    func popViewControllerInCenterPanel() {
        (mainSlidingNavigator.rootViewController as? UINavigationController)?.popViewController(animated: true)
    }
    
    func popToRootViewControllerInCenterPanel() {
        (mainSlidingNavigator.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
    }
    
    func changeMenuToTheLeft() {
        isMenuToTheRight = false
        mainSlidingNavigator.leftViewController = MenuViewController()
        mainSlidingNavigator.isLeftViewSwipeGestureEnabled = true
        mainSlidingNavigator.isRightViewSwipeGestureEnabled = false
        MenuViewController.sharedInstance().reloadViewsBasedOnCurrentLocalization()
//        mainSlidingNavigator.rightViewController = nil
        setupNavigationBarStyleWithBackButtonEnabled(false)
    }
    
    func changeMenuToTheRight() {
        isMenuToTheRight = true
        mainSlidingNavigator.rightViewController = MenuViewController()
        mainSlidingNavigator.isLeftViewSwipeGestureEnabled = false
        mainSlidingNavigator.isRightViewSwipeGestureEnabled = true
        MenuViewController.sharedInstance().reloadViewsBasedOnCurrentLocalization()
//        mainSlidingNavigator.leftViewController = nil
        setupNavigationBarStyleWithBackButtonEnabled(false)
    }
    
    func openMenu() {
        if isMenuToTheRight {
            mainSlidingNavigator.toggleRightViewAnimated()
        }
        else {
            mainSlidingNavigator.toggleLeftViewAnimated()
        }
    }
    func closeMenu() {
        if isMenuToTheRight {
            if !mainSlidingNavigator.isRightViewHidden {
                mainSlidingNavigator.toggleRightViewAnimated()
            }
        }
        else {
            if !mainSlidingNavigator.isLeftViewHidden {
                mainSlidingNavigator.toggleLeftViewAnimated()
            }
        }
    }
    
    func closeReversedMenu() {
        if !isMenuToTheRight {
            if !mainSlidingNavigator.isRightViewHidden {
                mainSlidingNavigator.toggleRightViewAnimated()
            }
        }
        else {
            if !mainSlidingNavigator.isLeftViewHidden {
                mainSlidingNavigator.toggleLeftViewAnimated()
            }
        }
    }
    
    func setScreenTitle(_ screenTitle: String) {
        mainSlidingNavigator.title = screenTitle
    }
    
    func back(){
        if(delegate != nil){
            delegate?.didTapOnBack()
        }
    }
    
    func sort(){
        if(delegate != nil){
            delegate?.didTapOnSort()
        }
    }
    
    func changeBackButtonStatusToStatusShow(_ isShown: Bool) {
        setupNavigationBarStyleWithBackButtonEnabled(isShown)
    }
    
    func getCurrentMenuViewController() -> MenuViewController{
        if isMenuToTheRight {
            return mainSlidingNavigator.rightViewController as! MenuViewController
        }
        
        return mainSlidingNavigator.leftViewController as! MenuViewController
    }

}
