//
//  ActivitiesDataCenter.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/2/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class ActivitiesDataCenter: NSObject {
    
    fileprivate weak var delegate: OperationDelegate?
    fileprivate(set) var activities : [Activity] = []
    fileprivate var loadingDetails : [LoadingItem : LoadingDetails] = [:]
    
    static let sharedInstance: ActivitiesDataCenter = {
        let sharedInstance = ActivitiesDataCenter()
        sharedInstance.initializeLoadingDetails()
        return sharedInstance
    }()
    
    func initializeLoadingDetails(){
        reInitializeHomeDetails()
    }
    
    func loadActivitiesWithDelegate(_ delegate: OperationDelegate?, isRefreshing : Bool = false){
        self.delegate = delegate
        if((activities.count <= 0 || activities.count % (loadingDetails[.Home]?.pageSize)! == 0) && !(loadingDetails[.Home]?.isRequestingData)!){
            loadingDetails[.Home]?.isRequestingData = true
            let parameters : [String:String] = ["page_index":"\(loadingDetails[.Home]!.pageIndex)","page_size":"\(loadingDetails[.Home]!.pageSize)","country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!]
            let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: (loadingDetails[.Home]?.url)!, requestMethod: .post, parameters: parameters, isShowLoading: activities.count <= 0 && !isRefreshing, aRequestID: .LoadActivities, isACoreRequest: false)
            cr.initiateRequest()
            loadingDetails[.Home]?.pageIndex = (loadingDetails[.Home]?.pageIndex)! + 1
        }
    }
    
    func reInitializeHomeDetails(){
        activities.removeAll()
        let homeLoadingDetails = LoadingDetails()
        homeLoadingDetails.url = "getHomeList.php"
        self.loadingDetails[.Home] = homeLoadingDetails
    }
}

extension ActivitiesDataCenter : ConnectionRequestDelegate{
    
    func requestDidCompleteLoading(request: ConnectionRequest, data: Data) {
        if(request.requestID == .LoadActivities){
            let activitiesResponse = ParsingUtility.parseDataAsDictionary(data)
            let activitiesList = activitiesResponse["mainActivityList"] as! [[String:AnyObject]]
            
            for activityData in activitiesList {
                let activity : Activity = Activity(data: activityData)
                self.activities.append(activity)
            }
            
            loadingDetails[.Home]?.isRequestingData = false
            delegate?.didFinishOperation(request.requestID)
        }
    }
    
    func requestDidRecieveError(request: ConnectionRequest, error: Error?) {
        if(request.requestID == .LoadActivities){
            loadingDetails[.Home]?.isRequestingData = false
            delegate?.didRecieveErrorForOperation(request.requestID, andWithError: error)
        }
    }
    
}
