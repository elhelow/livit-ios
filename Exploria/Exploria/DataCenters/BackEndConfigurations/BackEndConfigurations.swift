//
//  BackEndConfigurations.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 9/28/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

enum Server {
    case testing
    
    static func getCurrentServer() -> Server{
        return .testing
    }
}

class BackEndConfigurations: NSObject {
    static let testingServiceURL = "http://explorea.com/mobileApp/"
    static let testingImagesURL = "http://explorea.com/images/"
    
    class func getServiceURL() -> String{
        switch Server.getCurrentServer() {
        case .testing:
            return testingServiceURL
        }
    }
    
    class func getImagesURL() -> String{
        switch Server.getCurrentServer() {
        case .testing:
            return testingImagesURL
        }
    }

}
