//
//  ActivityProvidersDataCenter.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/10/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class ActivityProvidersDataCenter: NSObject {
    
    private var sortActivityProviderURL = "sortActivityProvider.php"
    private var activityProviderURL = "getActivityProviderList.php"
    private var mapActivityProviderURL = "getMapList.php"
    private var providerProfileDetails = "getProviderProfileDetails.php"
    private var reviewsURL = "getReviewsList.php"
    private var toggleFavoriteStatusURL = "addToFavorites.php"
    
    private var addReviewURL = "addReview.php"
    private var rateURL = "addRate.php"
    
    fileprivate weak var delegate: OperationDelegate?
    fileprivate(set) var activitiesProviders : [String : [ActivityProvider]] = [:]
    fileprivate(set) var mapsActivitiesProviders : [ActivityProvider] = []
    fileprivate var loadingDetails : [String : LoadingDetails] = [:]
    
    fileprivate(set) var filteredActivitiesByCategory : [ActivityProvider] = []
    fileprivate(set) var searchResults : [ActivityProvider] = []
    fileprivate(set) var favorites : [ActivityProvider] = []
    fileprivate(set) var sortedActivities : [ActivityProvider] = []
    fileprivate(set) var reviews : [Review] = []
    
    
    static let sharedInstance: ActivityProvidersDataCenter = {
        let sharedInstance = ActivityProvidersDataCenter()
        sharedInstance.initializeLoadingDetails()        
        return sharedInstance
    }()
    
    func initializeLoadingDetails(){
        
        let filterByCategoryDetails = LoadingDetails()
        filterByCategoryDetails.url = "FilterActivities.php"
        self.loadingDetails[LoadingItem.FilterByCategory.rawValue] = filterByCategoryDetails
        
        let searchDetails = LoadingDetails()
        searchDetails.url = "SearchActivities.php"
        self.loadingDetails[LoadingItem.Search.rawValue] = searchDetails
        
        let sortDetails = LoadingDetails()
        sortDetails.url = "SortActivities.php"
        self.loadingDetails[LoadingItem.Sort.rawValue] = sortDetails
        
        let favoritesDetails = LoadingDetails()
        favoritesDetails.url = "getFavorites.php"
        self.loadingDetails[LoadingItem.Favorites.rawValue] = favoritesDetails
        
    }
    
    func loadActivityProviderWithDelegate(_ delegate: OperationDelegate?, activityID : String, isRefreshing : Bool = false){
        self.delegate = delegate
        
        if(loadingDetails[activityID] == nil){
            let activityProviderLoadingDetails = LoadingDetails()
            activityProviderLoadingDetails.url = activityProviderURL
            self.loadingDetails[activityID] = activityProviderLoadingDetails
        }
        
        if((activitiesProviders[activityID] == nil || (activitiesProviders[activityID
            ]?.count)! % self.loadingDetails[activityID]!.pageSize == 0) && !self.loadingDetails[activityID]!.isRequestingData){
            self.loadingDetails[activityID]!.isRequestingData = true
            let parameters : [String:String] = ["page_index":"\(self.loadingDetails[activityID]!.pageIndex)","page_size":"\(self.loadingDetails[activityID]!.pageSize)","country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!,"main_activity_id":activityID,"user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? ""]
            let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: self.loadingDetails[activityID]!.url, requestMethod: .post, parameters: parameters, isShowLoading: (activitiesProviders[activityID] == nil || activitiesProviders[activityID]!.count <= 0) && !isRefreshing , aRequestID: .LoadActivityProviders, isACoreRequest: false)
            cr.object = activityID
            cr.initiateRequest()
            self.loadingDetails[activityID]!.pageIndex = self.loadingDetails[activityID]!.pageIndex + 1
        }
    }
    
    func loadActivityProviderReviewsWithDelegate(_ delegate: OperationDelegate?, providerID : String){
        self.delegate = delegate
        
        if(loadingDetails[LoadingItem.Reviews.rawValue] == nil){
            let reviewsLoadingDetails = LoadingDetails()
            reviewsLoadingDetails.url = reviewsURL
            self.loadingDetails[LoadingItem.Reviews.rawValue] = reviewsLoadingDetails
        }
        
        if((reviews.count % self.loadingDetails[LoadingItem.Reviews.rawValue]!.pageSize == 0) && !self.loadingDetails[LoadingItem.Reviews.rawValue]!.isRequestingData){
            self.loadingDetails[LoadingItem.Reviews.rawValue]!.isRequestingData = true
            let parameters : [String:String] = ["page_index":"\(self.loadingDetails[LoadingItem.Reviews.rawValue]!.pageIndex)","page_size":"\(self.loadingDetails[LoadingItem.Reviews.rawValue]!.pageSize)","country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!,"activity_prov_id":providerID]
            let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: self.loadingDetails[LoadingItem.Reviews.rawValue]!.url, requestMethod: .post, parameters: parameters, isShowLoading: self.reviews.count <= 0 , aRequestID: .LoadActivityProviderReviews, isACoreRequest: false)
            cr.initiateRequest()
            self.loadingDetails[LoadingItem.Reviews.rawValue]!.pageIndex = self.loadingDetails[LoadingItem.Reviews.rawValue]!.pageIndex + 1
        }
    }
    
    func loadMapActivityProviderWithDelegate(_ delegate: OperationDelegate?, lat:String, lng:String){
        self.delegate = delegate
        let parameters : [String:String] = ["lat":"\(lat)","lng":"\(lng)","user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? ""]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: mapActivityProviderURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .LoadMapActivityProviders, isACoreRequest: false)
        cr.initiateRequest()
    }
    
    func filterActivitiesByIDsWithDelegate(_ delegate: OperationDelegate?, categoryIDs: [String]){
        self.delegate = delegate
        if((filteredActivitiesByCategory.count <= 0 || filteredActivitiesByCategory.count % (loadingDetails[LoadingItem.FilterByCategory.rawValue]?.pageSize)! == 0) && !(loadingDetails[LoadingItem.FilterByCategory.rawValue]?.isRequestingData)!){
            loadingDetails[LoadingItem.FilterByCategory.rawValue]?.isRequestingData = true
            let parameters : [String:Any] = ["page_index":"\(loadingDetails[LoadingItem.FilterByCategory.rawValue]!.pageIndex)","page_size":"\(loadingDetails[LoadingItem.FilterByCategory.rawValue]!.pageSize)","country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!,"category_id":categoryIDs]
            let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: (loadingDetails[LoadingItem.FilterByCategory.rawValue]?.url)!, requestMethod: .post, parameters: parameters, isShowLoading: filteredActivitiesByCategory.count <= 0, aRequestID: .FilterActivitiesByCategories, isACoreRequest: false)
            cr.initiateRequest()
            loadingDetails[LoadingItem.FilterByCategory.rawValue]?.pageIndex = (loadingDetails[LoadingItem.FilterByCategory.rawValue]?.pageIndex)! + 1
        }
    }
    
    func searchActivitiesByKeywordWithDelegate(_ delegate: OperationDelegate?, keyWord: String){
        self.delegate = delegate
        if((searchResults.count <= 0 || searchResults.count % (loadingDetails[LoadingItem.Search.rawValue]?.pageSize)! == 0) && !(loadingDetails[LoadingItem.Search.rawValue]?.isRequestingData)!){
            loadingDetails[LoadingItem.Search.rawValue]?.isRequestingData = true
            let parameters : [String:Any] = ["page_index":"\(loadingDetails[LoadingItem.Search.rawValue]!.pageIndex)","page_size":"\(loadingDetails[LoadingItem.Search.rawValue]!.pageSize)","country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!,"keyword":keyWord,"user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? ""]
            let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: (loadingDetails[LoadingItem.Search.rawValue]?.url)!, requestMethod: .post, parameters: parameters, isShowLoading: searchResults.count <= 0, aRequestID: .SearchActivitiesByKeyword, isACoreRequest: false)
            cr.initiateRequest()
            loadingDetails[LoadingItem.Search.rawValue]?.pageIndex = (loadingDetails[LoadingItem.Search.rawValue]?.pageIndex)! + 1
        }
    }
    
    func sortActivitiesWithDelegate(_ delegate: OperationDelegate?, sortMethod: String, mainActivityID : String? = nil){
        self.delegate = delegate
        if((sortedActivities.count <= 0 || sortedActivities.count % (loadingDetails[LoadingItem.Sort.rawValue]?.pageSize)! == 0) && !(loadingDetails[LoadingItem.Sort.rawValue]?.isRequestingData)!){
            loadingDetails[LoadingItem.Sort.rawValue]?.isRequestingData = true
            var parameters : [String:Any] = ["page_index":"\(loadingDetails[LoadingItem.Sort.rawValue]!.pageIndex)","page_size":"\(loadingDetails[LoadingItem.Sort.rawValue]!.pageSize)","country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!,"sort":sortMethod,"user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? ""]
            if(mainActivityID != nil){
                parameters["main_activity_id"] = mainActivityID!
            }
            let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: mainActivityID == nil ? (loadingDetails[LoadingItem.Sort.rawValue]?.url)! : sortActivityProviderURL, requestMethod: .post, parameters: parameters, isShowLoading: sortedActivities.count <= 0, aRequestID: .SortActivities, isACoreRequest: false)
            cr.initiateRequest()
            loadingDetails[LoadingItem.Sort.rawValue]?.pageIndex = (loadingDetails[LoadingItem.Sort.rawValue]?.pageIndex)! + 1
        }
    }
    
    func loadActivityProviderDetailsWithDelegate(_ delegate: OperationDelegate?, providerID: String){
        self.delegate = delegate
        let parameters : [String:Any] = ["activity_prov_id":providerID,"user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? ""]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: providerProfileDetails, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .LoadActivityProviderDetails, isACoreRequest: false)
        cr.initiateRequest()
    }
    
    func addReviewWithDelegate(_ delegate: OperationDelegate?, providerID: String, name: String, email: String, review:String){
        
        self.delegate = delegate
        let parameters : [String:Any] = ["activity_prov_id":providerID,"user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? "","name":name,"email":email,"review":review]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: addReviewURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .AddReview, isACoreRequest: false)
        cr.initiateRequest()
    
    }
    
    func addRatingWithDelegate(_ delegate: OperationDelegate?, providerID: String, rate: String){
        
        self.delegate = delegate
        let parameters : [String:Any] = ["activity_prov_id":providerID,"user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? "","rate":rate]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: rateURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .AddRate, isACoreRequest: false)
        cr.initiateRequest()
        
    }
    
    func loadFavoritedActivityProvidersWithDelegate(_ delegate: OperationDelegate?){
        self.delegate = delegate
        if((favorites.count <= 0 || favorites.count % (loadingDetails[LoadingItem.Favorites.rawValue]?.pageSize)! == 0) && !(loadingDetails[LoadingItem.Favorites.rawValue]?.isRequestingData)!){
            loadingDetails[LoadingItem.Favorites.rawValue]?.isRequestingData = true
            let parameters : [String:Any] = ["page_index":"\(loadingDetails[LoadingItem.Favorites.rawValue]!.pageIndex)","page_size":"\(loadingDetails[LoadingItem.Favorites.rawValue]!.pageSize)","country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!,"user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? ""]
            let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: (loadingDetails[LoadingItem.Favorites.rawValue]?.url)!, requestMethod: .post, parameters: parameters, isShowLoading: favorites.count <= 0, aRequestID: .FavoritedActivityProviders, isACoreRequest: false)
            cr.initiateRequest()
            loadingDetails[LoadingItem.Favorites.rawValue]?.pageIndex = (loadingDetails[LoadingItem.Favorites.rawValue]?.pageIndex)! + 1
        }
    }
    
    
    func clearFilterationData(){
        self.filteredActivitiesByCategory = []
        self.loadingDetails[LoadingItem.FilterByCategory.rawValue]?.pageIndex = 1
        
        self.searchResults = []
        self.loadingDetails[LoadingItem.Search.rawValue]?.pageIndex = 1

        self.sortedActivities = []
        self.loadingDetails[LoadingItem.Sort.rawValue]?.pageIndex = 1
        
        mapsActivitiesProviders = []
        
        self.favorites = []
        self.loadingDetails[LoadingItem.Favorites.rawValue]?.pageIndex = 1
    }
    
    func clearReviewsData(){
        self.reviews = []
        self.loadingDetails[LoadingItem.Reviews.rawValue]?.pageIndex = 1
    }
    
    func clearDataForLogout(){
        self.loadingDetails.removeAll()
        self.activitiesProviders.removeAll()
        self.mapsActivitiesProviders.removeAll()
        self.initializeLoadingDetails()
    }
    
    func toggleProviderFavoriteStatusWithDelegate(_ delegate: OperationDelegate?,activityProviderID:String, newFavStatus:String){
        self.delegate = delegate
        let parameters : [String:String] = ["activity_prov_id":activityProviderID, "fav":newFavStatus,"user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? ""]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: toggleFavoriteStatusURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .ToggleFavoriteStatus, isACoreRequest: false)
        cr.object = parameters
        cr.initiateRequest()
    }
    
    func updateFavoriteStatusForCachedValuesWithProviderID(_ activityProviderID:String, newFavStatus:Bool){
        
        for activityProviderEntry in filteredActivitiesByCategory{
            if(activityProviderEntry.id == activityProviderID){
                activityProviderEntry.isFavorited = newFavStatus
                break
            }
        }
        
        for activityProviderEntry in searchResults{
            if(activityProviderEntry.id == activityProviderID){
                activityProviderEntry.isFavorited = newFavStatus
                break
            }
        }
        
        for activityProviderEntry in sortedActivities{
            if(activityProviderEntry.id == activityProviderID){
                activityProviderEntry.isFavorited = newFavStatus
                break
            }
        }
        
        for activityProviderEntry in mapsActivitiesProviders{
            if(activityProviderEntry.id == activityProviderID){
                activityProviderEntry.isFavorited = newFavStatus
                break
            }
        }
        
        var newFavorites : [ActivityProvider] = []
        for activityProviderEntry in favorites{
            if(activityProviderEntry.id == activityProviderID){
                if(!newFavStatus){
                    continue
                }
            }
            
            newFavorites.append(activityProviderEntry)
        }
        
        favorites = newFavorites
        
        for activityProvider in activitiesProviders{
            for activityProviderEntry in activityProvider.value{
                if(activityProviderEntry.id == activityProviderID){
                    activityProviderEntry.isFavorited = newFavStatus
                    return
                }
            }
        }
    }
    
    func clearLoadingDetailsForActivityWithID(_ activityID : String){
        loadingDetails[activityID] = nil
        activitiesProviders[activityID]?.removeAll()
    }
    
}


extension ActivityProvidersDataCenter : ConnectionRequestDelegate{
    
    func requestDidCompleteLoading(request: ConnectionRequest, data: Data) {

        let providersResponse = ParsingUtility.parseDataAsDictionary(data)

        if(request.requestID == .LoadActivityProviders){
            let providersList = providersResponse["ProvidersList"] as? [[String:AnyObject]] ?? []
            self.activitiesProviders[request.object as! String] = []
            
            for providerData in providersList {
                let activityProvider : ActivityProvider = ActivityProvider(data: providerData)
                self.activitiesProviders[request.object as! String]!.append(activityProvider)
            }
            
            self.loadingDetails[request.object as! String]!.isRequestingData = false
        }else if(request.requestID == .LoadMapActivityProviders){
            let providersList = providersResponse["ProvidersList"] as! [[String:AnyObject]]
            
            for providerData in providersList {
                let activityProvider : ActivityProvider = ActivityProvider(data: providerData)
                self.mapsActivitiesProviders.append(activityProvider)
            }
        }else if(request.requestID == .FilterActivitiesByCategories){
            let providersList = providersResponse["ProvidersList"] as? [[String:AnyObject]] ?? []
            
            for providerData in providersList {
                let activityProvider : ActivityProvider = ActivityProvider(data: providerData)
                self.filteredActivitiesByCategory.append(activityProvider)
            }
            
            loadingDetails[LoadingItem.FilterByCategory.rawValue]?.isRequestingData = false

        }else if(request.requestID == .SearchActivitiesByKeyword){
            let providersList = providersResponse["ProvidersList"] as? [[String:AnyObject]] ?? []
            
            for providerData in providersList {
                let activityProvider : ActivityProvider = ActivityProvider(data: providerData)
                self.searchResults.append(activityProvider)
            }
            
            loadingDetails[LoadingItem.Search.rawValue]?.isRequestingData = false

        }else if(request.requestID == .SortActivities){
            let providersList = providersResponse["ProvidersList"] as? [[String:AnyObject]] ?? []
            
            for providerData in providersList {
                let activityProvider : ActivityProvider = ActivityProvider(data: providerData)
                self.sortedActivities.append(activityProvider)
            }
            
            loadingDetails[LoadingItem.Sort.rawValue]?.isRequestingData = false
        }else if(request.requestID == .LoadActivityProviderDetails){
            let providerProfile = providersResponse["ProfileDetails"] as? [[String:AnyObject]] ?? []
            var activityProviderDetails = ActivityProviderDetails(data: [:])
            if(providerProfile.count > 0){
                activityProviderDetails = ActivityProviderDetails(data: providerProfile[0])
            }

            delegate?.didFinishOperation(request.requestID, object: activityProviderDetails)
        }else if(request.requestID == .LoadActivityProviderReviews){
            let providerReviews = providersResponse["Review"] as? [[String:AnyObject]] ?? []
            
            for providerReviewData in providerReviews {
                let review : Review = Review(data: providerReviewData)
                self.reviews.append(review)
            }
            
            loadingDetails[LoadingItem.Reviews.rawValue]?.isRequestingData = false
        }else if(request.requestID == .ToggleFavoriteStatus){
            let isError = providersResponse["IsError"] as? Bool
            if(request.object != nil && !isError!){
                let parameters : [String:String] = request.object as! [String:String]
                self.updateFavoriteStatusForCachedValuesWithProviderID(parameters["activity_prov_id"]!, newFavStatus: (parameters["fav"]!).toBool()!)
            }
            delegate?.didFinishOperation(request.requestID, object: isError as AnyObject)
            return
        }else if(request.requestID == .FavoritedActivityProviders){
            let providersList = providersResponse["ProvidersList"] as? [[String:AnyObject]] ?? []
            
            for providerData in providersList {
                let activityProvider : ActivityProvider = ActivityProvider(data: providerData)
                self.favorites.append(activityProvider)
            }
            
            loadingDetails[LoadingItem.Favorites.rawValue]?.isRequestingData = false
            
        }else if(request.requestID == .AddReview){
            if(Int(ValidationsUtility.forceObjectToBeString(providersResponse["status"])) == 0){
                delegate?.didFinishOperation(request.requestID, object: false as AnyObject)
            }else{
                delegate?.didFinishOperation(request.requestID, object: true as AnyObject)
            }
            
            return
        }else if(request.requestID == .AddRate){
            delegate?.didFinishOperation(request.requestID, object: Int(ValidationsUtility.forceObjectToBeString(providersResponse["status"])) as AnyObject)
            
            return
        }
        
        delegate?.didFinishOperation(request.requestID)
        
    }
    
    func requestDidRecieveError(request: ConnectionRequest, error: Error?) {
        if(request.requestID == .LoadActivityProviders){
            self.loadingDetails[LoadingItem.Home.rawValue]?.isRequestingData = false
        }else if(request.requestID == .FilterActivitiesByCategories){
            loadingDetails[LoadingItem.FilterByCategory.rawValue]?.isRequestingData = false
        }else if(request.requestID == .SearchActivitiesByKeyword){
            loadingDetails[LoadingItem.Search.rawValue]?.isRequestingData = false
        }else if(request.requestID == .SearchActivitiesByKeyword){
            loadingDetails[LoadingItem.Sort.rawValue]?.isRequestingData = false
        }else if(request.requestID == .LoadActivityProviderReviews){
            loadingDetails[LoadingItem.Reviews.rawValue]?.isRequestingData = false
        }
        
        delegate?.didRecieveErrorForOperation(request.requestID, andWithError: error)
    }
    
}
