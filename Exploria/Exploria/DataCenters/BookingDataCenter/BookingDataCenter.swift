//
//  BookingDataCenter.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/29/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class BookingDataCenter: NSObject {
    
    fileprivate weak var delegate: OperationDelegate?
    private var bookActivityDetailsURL = "getBookActivity.php"
    private var bookCourseDetailsURL = "getCourseDetails.php"
    private var saveActivityBookingDetailsURL = "postBookActivity.php"
    private var saveCourseBookingDetailsURL = "postBookCourse.php"
    private var myBookingsListURL = "getBookingList.php"
    private var checkPromoCodeURL = "checkPromoCode.php"
    
    static let sharedInstance: BookingDataCenter = {
        let sharedInstance = BookingDataCenter()
        return sharedInstance
    }()
    
    func loadBookingDetailsWithDelegate(_ delegate: OperationDelegate?, providerID : String){
        self.delegate = delegate
        let parameters : [String:Any] = ["activity_prov_id":providerID]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: bookActivityDetailsURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .BookingActivityDetails, isACoreRequest: false)
        cr.initiateRequest()
    }
    
    func loadCourseDetailsWithDelegate(_ delegate: OperationDelegate?, providerID : String){
        self.delegate = delegate
        let parameters : [String:Any] = ["activity_prov_id":providerID, "user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? ""]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: bookCourseDetailsURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .BookingCourseDetails, isACoreRequest: false)
        cr.initiateRequest()
    }
    
    func loadBookingsForCurrentUserWithDelegate(_ delegate: OperationDelegate?){
        self.delegate = delegate
        let parameters : [String:Any] = ["user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? "","page_index": "1","page_size":"100"]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: myBookingsListURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .MyBookings, isACoreRequest: false)
        cr.initiateRequest()
    }
    
    func checkPromoCodeWithDelegate(_ delegate: OperationDelegate?, providerID : String, promoCode : String){
        self.delegate = delegate
        
        let parameters : [String:Any] = ["activity_prov_id":providerID, "user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? "", "promo_code":promoCode]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: checkPromoCodeURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .CheckPromoCode, isACoreRequest: false)
        cr.initiateRequest()
    }
    
    
    
    func saveActivityBookingDetailsWithDelegate(_ delegate: OperationDelegate?, bookingQuery : BookingQuery){
        
        self.delegate = delegate
        
        let parameters : [String:Any] = ["activityName":bookingQuery.activityName,
                                         "activity_price":bookingQuery.activityPrice,
                                         "activity_prov_id":bookingQuery.activityProviderID,
                                         "date":bookingQuery.dateAsString,
                                         "deposite_value":bookingQuery.depositeValue,
                                         "num_of_player":bookingQuery.numberOfPlayers,
                                         "payment_method":bookingQuery.paymentMethod,
                                         "payment_type":bookingQuery.paymentType,
                                         "pricing_type":bookingQuery.pricingType,
                                         "questions":bookingQuery.questions,
                                         "selected_date_id":bookingQuery.dateAsID,
                                         "selectedDateTitle":bookingQuery.dateTitle,
                                         "selected_time_id":bookingQuery.timeAsID,
                                         "services":bookingQuery.services,
                                         "time":bookingQuery.timeAsString,
                                         "total_payment":bookingQuery.totalPayment,
                                         "userEmail":UserIdentificationDataCenter.sharedInstance.loggedInUser?.email ?? "",
                                         "user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? "",
                                         "userName":UserIdentificationDataCenter.sharedInstance.loggedInUser?.name ?? "",
                                         "userPhone":UserIdentificationDataCenter.sharedInstance.loggedInUser?.phone ?? ""]
        
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: saveActivityBookingDetailsURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .SaveBookingActivityDetails, isACoreRequest: false)
        cr.initiateRequest()
    }
    
    func saveCourseBookingDetailsWithDelegate(_ delegate: OperationDelegate?, bookingQuery : CourseBookingQuery){
        
        self.delegate = delegate
        
        let parameters : [String:Any] = ["activity_prov_id":bookingQuery.activityProviderID,
                                         "lesson_id":bookingQuery.lessonsIDs,
                                         "payment_method":bookingQuery.paymentMethod,
                                         "selected_date_id":bookingQuery.selectedDatesIDs,
                                         "selected_time_id":bookingQuery.selectedTimesIDs,
                                         "total_payment":bookingQuery.totalPayment,
                                         "price_type":bookingQuery.pricingType,
                                         "user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? ""]
        
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: saveCourseBookingDetailsURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .SaveCourseDetails, isACoreRequest: false)
        cr.initiateRequest()
    }
    
}


extension BookingDataCenter : ConnectionRequestDelegate{
    
    func requestDidCompleteLoading(request: ConnectionRequest, data: Data) {
        let bookingResponse = ParsingUtility.parseDataAsDictionary(data)
        if(request.requestID == .BookingActivityDetails){
            if let bookingData = bookingResponse["BookingDetails"] as? [String : AnyObject]{
                let bookingDetails = ActivityBookingDetails(data: bookingData)
                delegate?.didFinishOperation(request.requestID, object: bookingDetails)
            }else{
                delegate?.didFinishOperation(request.requestID, object: ActivityBookingDetails(data: [:]))
            }
        }else if(request.requestID == .BookingCourseDetails){
            if let courseData = bookingResponse["courseDetails"] as? [String : AnyObject]{
                let courseDetails = CourseBookingDetails(data: courseData)
                delegate?.didFinishOperation(request.requestID, object: courseDetails)
            }else{
                delegate?.didFinishOperation(request.requestID, object: CourseBookingDetails(data: [:]))
            }
        }else if(request.requestID == .SaveBookingActivityDetails){
            if let bookingDetails =  bookingResponse["BookingDetails"] as? [String : AnyObject]{
                if let trackNumber = bookingDetails["track_number"]{
                    delegate?.didFinishOperation(request.requestID, object: trackNumber)
                }
            }else{
                delegate?.didFinishOperation(request.requestID, object: false as AnyObject)
            }
        }else if(request.requestID == .SaveCourseDetails){
            if let bookingDetails =  bookingResponse["BookingDetails"] as? [String : AnyObject]{
                if let trackNumber = bookingDetails["track_number"]{
                    delegate?.didFinishOperation(request.requestID, object: trackNumber)
                }
            }else{
                delegate?.didFinishOperation(request.requestID, object: false as AnyObject)
            }
        }else if(request.requestID == .CheckPromoCode){
            if let codeDetailsArray =  bookingResponse["Code"] as? [[String : AnyObject]]{
                if codeDetailsArray.count > 0 {
                    let aPromoCode = PromoCode(data: codeDetailsArray[0])
                    delegate?.didFinishOperation(request.requestID, object: aPromoCode)
                }else{
                    delegate?.didFinishOperation(request.requestID, object: false as AnyObject)
                }
            }else{
                delegate?.didFinishOperation(request.requestID, object: false as AnyObject)
            }
        }else if(request.requestID == .MyBookings){
            if let bookingData = bookingResponse["bookingList"] as? [[String : AnyObject]]{
                var bookingsHistory : [BookingHistory] = []
                for aBookingData in bookingData{
                    let bookingHistory = BookingHistory(data: aBookingData)
                    bookingsHistory.append(bookingHistory)
                }
                delegate?.didFinishOperation(request.requestID, object: bookingsHistory as AnyObject)
            }else{
                delegate?.didFinishOperation(request.requestID, object: [] as AnyObject)
            }
        }
    }
    
    func requestDidRecieveError(request: ConnectionRequest, error: Error?) {
        delegate?.didRecieveErrorForOperation(request.requestID, andWithError: error)
    }
    
}

