//
//  CategoriesDataCenter.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/11/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class CategoriesDataCenter: NSObject {
    
    private var filterCategoriesURL = "getCategoriesList.php"
    
    fileprivate weak var delegate: OperationDelegate?
    fileprivate(set) var filterCategories : [FilterCategory] = []
    
    static let sharedInstance: CategoriesDataCenter = {
        let sharedInstance = CategoriesDataCenter()
        return sharedInstance
    }()
    
    func loadFilterCategoiresWithDelegate(_ delegate: OperationDelegate?){
        self.delegate = delegate

        if(filterCategories.count <= 0){
            let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: filterCategoriesURL, requestMethod: .post, parameters: nil, isShowLoading: true, aRequestID: .LoadFilterCategories, isACoreRequest: false)
            cr.initiateRequest()
        }
    }
    
}


extension CategoriesDataCenter : ConnectionRequestDelegate{
    
    func requestDidCompleteLoading(request: ConnectionRequest, data: Data) {
        if(request.requestID == .LoadFilterCategories){
            let filterCategoriesResponse = ParsingUtility.parseDataAsDictionary(data)
            let filterCategoriesList = filterCategoriesResponse["CategoriesList"] as! [[String:AnyObject]]
            
            for categoriesFilterData in filterCategoriesList {
                let categoriesFilter : FilterCategory = FilterCategory(data: categoriesFilterData)
                self.filterCategories.append(categoriesFilter)
            }
            
        }
        
        delegate?.didFinishOperation(request.requestID)
        
    }
    
    func requestDidRecieveError(request: ConnectionRequest, error: Error?) {
        delegate?.didRecieveErrorForOperation(request.requestID, andWithError: error)
    }
    
}

