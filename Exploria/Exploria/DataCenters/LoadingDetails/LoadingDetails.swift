//
//  LoadingDetails.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/19/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

enum LoadingItem : String{
    case Home
    case FilterByCategory
    case Search
    case Sort
    case Reviews
    case Favorites
}

class LoadingDetails: NSObject {
    public var url = ""
    public var pageIndex = 1
    public var pageSize = 10
    
    public var isRequestingData = false
}
