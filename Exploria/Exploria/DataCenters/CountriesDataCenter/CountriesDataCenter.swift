//
//  CountriesDataCenter.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 9/28/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class CountriesDataCenter: NSObject {
    fileprivate weak var delegate: OperationDelegate?
    private var countiresURL = "getCountriesList.php"
    fileprivate(set) var countries : [Country] = []
    
    var selectedCountry : Country?

    static let sharedInstance: CountriesDataCenter = {
        let sharedInstance = CountriesDataCenter()
        return sharedInstance
    }()
    
    func loadCountriesWithDelegate(_ delegate: OperationDelegate?){
        if(countries.count <= 0){
            self.delegate = delegate
            let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: countiresURL, requestMethod: .get, parameters: nil, isShowLoading: true, aRequestID: .LoadCountries, isACoreRequest: false)
            cr.initiateRequest()
        }

    }
    
}


extension CountriesDataCenter : ConnectionRequestDelegate{
    
    func requestDidCompleteLoading(request: ConnectionRequest, data: Data) {
        let countryResponse = ParsingUtility.parseDataAsDictionary(data)
        let countryList = countryResponse["CountriesList"] as! [[String:AnyObject]]
        
        self.countries = []
        for countryData in countryList {
            let country : Country = Country(data: countryData)
            if(country.isEnabled){
                self.countries.append(country)
            }
        }
        
        delegate?.didFinishOperation(request.requestID)
    }
    
    func requestDidRecieveError(request: ConnectionRequest, error: Error?) {
        delegate?.didRecieveErrorForOperation(request.requestID, andWithError: error)
    }
    
}
