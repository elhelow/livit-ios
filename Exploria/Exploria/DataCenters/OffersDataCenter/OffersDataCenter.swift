//
//  OffersDataCenter.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/10/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class OffersDataCenter: NSObject {
    
    private var offersURL = "getOffersList.php"
    private var specialOffersURL = "getSpecialOffer.php"
    
    fileprivate weak var delegate: OperationDelegate?
    fileprivate weak var specialOffersDelegate: OperationDelegate?
    fileprivate(set) var offers : [Offer] = []
    fileprivate(set) var specialOffer : SpecialOffer?
    
    static let sharedInstance: OffersDataCenter = {
        let sharedInstance = OffersDataCenter()
        return sharedInstance
    }()
    
    func loadOffersWithDelegate(_ delegate: OperationDelegate?){
        self.delegate = delegate
        if(offers.count <= 0){
            let parameters : [String:String] = ["country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!]
            let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: offersURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .LoadOffers, isACoreRequest: false)
            cr.initiateRequest()
        }
    }
    
    func loadSpecialOffersWithDelegate(_ delegate: OperationDelegate?){
        self.specialOffersDelegate = delegate
        let parameters : [String:String] = ["country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: specialOffersURL, requestMethod: .post, parameters: parameters, isShowLoading: false, aRequestID: .LoadSpecialOffers, isACoreRequest: false)
        cr.initiateRequest()
    }
}


extension OffersDataCenter : ConnectionRequestDelegate{
    
    func requestDidCompleteLoading(request: ConnectionRequest, data: Data) {
        if(request.requestID == .LoadOffers){
            let offersResponse = ParsingUtility.parseDataAsDictionary(data)
            let offersList = offersResponse["OffersList"] as! [[String:AnyObject]]
            
            for offerData in offersList {
                let offer : Offer = Offer(data: offerData)
                self.offers.append(offer)
            }
            delegate?.didFinishOperation(request.requestID)
        }else if(request.requestID == .LoadSpecialOffers){
            let offersResponse = ParsingUtility.parseDataAsDictionary(data)
            let offersData = offersResponse["OffersList"] as? [String:AnyObject]
            
            if(offersData != nil && offersData!["image"] != nil){
                specialOffer = SpecialOffer(data: offersData!)
            }else{
                specialOffer = nil
            }

            specialOffersDelegate?.didFinishOperation(request.requestID)
        }
    }
    
    func requestDidRecieveError(request: ConnectionRequest, error: Error?) {
        delegate?.didRecieveErrorForOperation(request.requestID, andWithError: error)
    }
    
}

