//
//  AppInfosDataCenter.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/18/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

class AppInfosDataCenter: NSObject {
    
    private var contactUsURL = "contactUs.php"
    
    fileprivate weak var delegate: OperationDelegate?
    
    static let sharedInstance: AppInfosDataCenter = {
        let sharedInstance = AppInfosDataCenter()
        sharedInstance.initializeLoadingDetails()
        return sharedInstance
    }()
    
    func initializeLoadingDetails(){
        
//        let filterByCategoryDetails = LoadingDetails()
//        filterByCategoryDetails.url = "FilterActivities.php"
//        self.loadingDetails[LoadingItem.FilterByCategory.rawValue] = filterByCategoryDetails
    }
    
    func contactUsWithDelegate(_ delegate: OperationDelegate?, name: String, email: String, message:String){
        
        self.delegate = delegate
        let parameters : [String:Any] = ["user_id":UserIdentificationDataCenter.sharedInstance.loggedInUser?.userID ?? "","first_name":name,"last_name":name,"email":email,"message":message]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: contactUsURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .ContactUs, isACoreRequest: false)
        cr.initiateRequest()
        
    }
}


extension AppInfosDataCenter : ConnectionRequestDelegate{
    
    func requestDidCompleteLoading(request: ConnectionRequest, data: Data) {
        
        let _ = ParsingUtility.parseDataAsDictionary(data)
        
        if(request.requestID == .ContactUs){

        }
        
        delegate?.didFinishOperation(request.requestID)
        
    }
    
    func requestDidRecieveError(request: ConnectionRequest, error: Error?) {        
        delegate?.didRecieveErrorForOperation(request.requestID, andWithError: error)
    }
    
}
