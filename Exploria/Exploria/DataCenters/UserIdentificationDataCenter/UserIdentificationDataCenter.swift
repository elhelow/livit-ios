//
//  UserIdentificationDataCenter.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/14/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class UserIdentificationDataCenter: NSObject {
    
    private var singInURL = "signIn.php"
    private var singUpURL = "signUp.php"
    private var updateProfileURL = "updateProfile.php"
    fileprivate(set) var loggedInUser : User?
    
    fileprivate weak var delegate: OperationDelegate?
    
    static let sharedInstance: UserIdentificationDataCenter = {
        let sharedInstance = UserIdentificationDataCenter()
        return sharedInstance
    }()
    
    func loginWithDelegate(_ delegate: OperationDelegate?, email: String, password: String){
        self.delegate = delegate
        let parameters : [String:String] = ["email":email, "password":password]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: singInURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .SignIn, isACoreRequest: false)
        cr.initiateRequest()
    }
    
    func signupWithDelegate(_ delegate: OperationDelegate?, name: String, email: String, mobile: String, password: String, birthDate: String, gender: String, address: String){
        self.delegate = delegate
        let parameters : [String:String] = ["country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!, "name":name, "email":email, "phone":mobile, "password":password, "confirmPass":password, "birth_date":birthDate, "gender":gender, "address":address]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: singUpURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .SignUp, isACoreRequest: false)
        cr.initiateRequest()
    }
    
    func updateProfileWithDelegate(_ delegate: OperationDelegate?, name: String, email: String, mobile: String, password: String, birthDate: String, gender: String, address: String){
        self.delegate = delegate
        let parameters : [String:String] = ["country_id":(CountriesDataCenter.sharedInstance.selectedCountry?.countryID)!, "name":name, "email":email, "phone":mobile, "password":password, "confirmPass":password, "birth_date":birthDate, "gender":gender, "address":address,"user_id":loggedInUser?.userID ?? ""]
        let cr : ConnectionRequest = ConnectionRequest(delegate: self, requestURL: updateProfileURL, requestMethod: .post, parameters: parameters, isShowLoading: true, aRequestID: .UpdateProfile, isACoreRequest: false)
        cr.initiateRequest()
    }
    
    func logout(){
        User.clearUserInfo()
        self.loggedInUser = nil
    }
    
    func setCurrentLoggedInUser(_ user : User){
        self.loggedInUser = user
    }
}


extension UserIdentificationDataCenter : ConnectionRequestDelegate{
    
    func requestDidCompleteLoading(request: ConnectionRequest, data: Data) {
        let identificationResponse = ParsingUtility.parseDataAsDictionary(data)
        
        if(request.requestID == .SignIn){
            let isError = identificationResponse["IsError"] as? Bool
            if(!(isError!)){
                loggedInUser = User(data: identificationResponse["user"] as! [String : AnyObject])
            }
            delegate?.didFinishOperation(request.requestID, object: isError as AnyObject)
        }else if(request.requestID == .SignUp){
            let isError = identificationResponse["IsError"] as? Bool
            if(!(isError!)){
                loggedInUser = User(data: identificationResponse["User"] as! [String : AnyObject])
            }
            delegate?.didFinishOperation(request.requestID, object: isError as AnyObject)
        }else if(request.requestID == .UpdateProfile){
            let isError = identificationResponse["IsError"] as? Bool
            if(!(isError!)){
                loggedInUser = User(data: identificationResponse["User"] as! [String : AnyObject])
            }
            delegate?.didFinishOperation(request.requestID, object: isError as AnyObject)
        }
        
    }
    
    func requestDidRecieveError(request: ConnectionRequest, error: Error?) {
        delegate?.didRecieveErrorForOperation(request.requestID, andWithError: error)
    }

}
