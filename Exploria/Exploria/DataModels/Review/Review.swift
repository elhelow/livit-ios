//
//  Review.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/30/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class Review : NSObject {
    var id = ""
    var activityProfileID = ""
    var name = ""
    var email = ""
    var review = ""
    var date = ""
    
    init(data:[String:AnyObject]) {
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProfileID = ValidationsUtility.forceObjectToBeString(data["activity_profile_id"])
        name = ValidationsUtility.forceObjectToBeString(data["name"])
        email = ValidationsUtility.forceObjectToBeString(data["email"])
        review = ValidationsUtility.forceObjectToBeString(data["review"])
        date = ValidationsUtility.forceObjectToBeString(data["date"])
    }
}
