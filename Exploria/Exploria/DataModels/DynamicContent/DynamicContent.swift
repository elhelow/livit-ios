//
//  DynamicContent.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/30/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class DynamicContent : NSObject {
    var titleEn = ""
    var titleAr = ""
    var descriptionEn = ""
    var descriptionAr = ""
    
    init(data:[String:AnyObject]) {
        titleEn = ValidationsUtility.forceObjectToBeString(data["title"])
        titleAr = ValidationsUtility.forceObjectToBeString(data["title_ar"])
        descriptionEn = ValidationsUtility.forceObjectToBeString(data["description"])
        descriptionAr = ValidationsUtility.forceObjectToBeString(data["description_ar"])
    }
}
