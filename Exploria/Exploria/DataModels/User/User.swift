//
//  User.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/20/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

class User : NSObject {
    private static let userKey = "LoggedInUserData"
    var phone = ""
    var gender = ""
    var userID = ""
    var birthDate = ""
    var address = ""
    var email = ""
    var name = ""
    var countryID = ""
    var password = ""
    
    init(data:[String:AnyObject]) {
        super.init()
        phone = ValidationsUtility.forceObjectToBeString(data["phone"])
        gender = ValidationsUtility.forceObjectToBeString(data["gender"])
        userID = ValidationsUtility.forceObjectToBeString(data["id"])
        birthDate = ValidationsUtility.forceObjectToBeString(data["birth_date"])
        address = ValidationsUtility.forceObjectToBeString(data["address"])
        email = ValidationsUtility.forceObjectToBeString(data["email"])
        countryID = ValidationsUtility.forceObjectToBeString(data["country_id"])
        name = ValidationsUtility.forceObjectToBeString(data["name"])
        password = ValidationsUtility.forceObjectToBeString(data["password"])
        User.saveUserInfo(data: data)
    }
    
    class func saveUserInfo(data:[String:AnyObject]){
        UserDefaults.standard.set(data, forKey: userKey)
        UserDefaults.standard.synchronize()
    }
    
    class func clearUserInfo(){
        UserDefaults.standard.removeObject(forKey: userKey)
        UserDefaults.standard.synchronize()
    }
    
    class func setUserIfIsAlreadyLoggedIn(){
        if let userData = (UserDefaults.standard.value(forKey: userKey)) as? [String:AnyObject]{
            UserIdentificationDataCenter.sharedInstance.setCurrentLoggedInUser(User(data: userData))
        }
    }
}

