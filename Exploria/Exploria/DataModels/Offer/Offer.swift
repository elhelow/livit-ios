//
//  Offer.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/10/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

class Offer: NSObject {
    var id = ""
    var activityProviderID = ""
    var countryID = ""
    var nameEn = ""
    var nameAr = ""
    var descriptionEn = ""
    var descriptionAr = ""
    var price = ""
    var imageURL = ""
    var image : UIImage? = nil
    var isApproved = true
    var date = ""
    var activityProviderNameAr = ""
    var activityProviderNameEn = ""
    
    private var imageDownloader : ImageDataProvider?
    weak var delegate : OperationDelegate?
    
    
    init(data:[String:AnyObject]) {
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_provider_id"])
        countryID = ValidationsUtility.forceObjectToBeString(data["country_id"])
        nameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        nameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        descriptionEn = ValidationsUtility.forceObjectToBeString(data["description"])
        descriptionAr = ValidationsUtility.forceObjectToBeString(data["description_ar"])
        price = ValidationsUtility.forceObjectToBeString(data["price"])
        imageURL = ValidationsUtility.forceObjectToBeString(data["image"])
        isApproved = Bool(ValidationsUtility.forceObjectToBeString(data["approved"])) ?? true
        date = ValidationsUtility.forceObjectToBeString(data["date"])
        activityProviderNameAr = ValidationsUtility.forceObjectToBeString(data["ActivityProviderName_ar"])
        activityProviderNameEn = ValidationsUtility.forceObjectToBeString(data["ActivityProviderName"])
        
    }
    
    func loadImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.image == nil){
            if(imageDownloader == nil || !imageDownloader!.isDownloading){
                imageDownloader = ImageDataProvider(aDelegate: self)
                imageDownloader?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(imageURL)")
            }
        }
    }
}

extension Offer : ImageDataProviderDelegate{
    func didLoadImage(_ image: UIImage, specialKey: String) {
        self.image = image
        delegate?.didFinishOperation(.LoadImageGeneralOperationID)
    }
    
    func didFailLoadingImage() {
        delegate?.didRecieveErrorForOperation(.LoadImageGeneralOperationID, andWithError: nil)
    }
}
