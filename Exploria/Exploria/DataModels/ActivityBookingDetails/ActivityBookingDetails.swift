//
//  ActivityBookingDetails.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/29/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

enum BookingType {
    case Single
    case Group
    case Both
    case None
    
    static func getBookingTypeFromString(_ string : String) -> BookingType{
        if(ValidationsUtility.isString(string, equalsString: "single", withCaseSensitivityCoparison: false)){
            return .Single
        }else if(ValidationsUtility.isString(string, equalsString: "group", withCaseSensitivityCoparison: false)){
            return .Group
        }else if(ValidationsUtility.isString(string, equalsString: "both", withCaseSensitivityCoparison: false)){
            return .Both
        }
        
        return .Both
    }
}

enum PricingType {
    case Activity
    case Service
    case Player
    case None
    
    static func getPricingTypeFromString(_ string : String) -> PricingType{
        if(ValidationsUtility.isString(string, equalsString: "per_activity", withCaseSensitivityCoparison: false)){
            return .Activity
        }else if(ValidationsUtility.isString(string, equalsString: "per_service", withCaseSensitivityCoparison: false)){
            return .Service
        }else if(ValidationsUtility.isString(string, equalsString: "per_player", withCaseSensitivityCoparison: false)){
            return .Player
        }
        
        return .Activity
    }
}

class ActivityBookingDetails : NSObject {
    var id = ""
    var bookingType : BookingType = .Both
    var pricingType : PricingType = .Activity
    var pricingTypeAsString = ""
    var maxNumberOfPlayers = 1
    var activityPrice = 0.0
    var deposite = 0.0
    var questions : [ActivityBookingQuestion] = []
    var dates : [ActivityBookingDate] = []
    var services : [ActivityBookingService] = []
    
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        bookingType = BookingType.getBookingTypeFromString(ValidationsUtility.forceObjectToBeString(data["booking_type"]))
        pricingType = PricingType.getPricingTypeFromString(ValidationsUtility.forceObjectToBeString(data["pricing_type"]))
        pricingTypeAsString = ValidationsUtility.forceObjectToBeString(data["pricing_type"])
        maxNumberOfPlayers = Int(ValidationsUtility.forceObjectToBeString(data["num_of_player"])) ?? 1
        activityPrice = Double(ValidationsUtility.forceObjectToBeString(data["activity_price"])) ?? 0.0
        deposite = Double(ValidationsUtility.forceObjectToBeString(data["Deposite_value"])) ?? 0.0
        
        
        if let bookingHasQuestions = data["has_questions"] as? NSArray {
            for questionData in bookingHasQuestions {
                self.questions.append(ActivityBookingQuestion(data: questionData as! [String : AnyObject]))
            }
        }
        
        if let preferedDates = data["PreferedDates"] as? NSArray {
            for preferedDateData in preferedDates {
                self.dates.append(ActivityBookingDate(data: preferedDateData as! [String : AnyObject]))
            }
        }
        
        if let servicesData = data["Services"] as? NSArray {
            for serviceData in servicesData {
                let service = ActivityBookingService(data: serviceData as! [String : AnyObject])
                if(service.subServices.count != 0){
                    self.services.append(service)
                }
            }
        }

    }
}

class ActivityBookingQuestion : NSObject{
    var id = ""
    var activityProviderID = ""
    var questionAr = ""
    var questionEn = ""
    var answers : [ActivityBookingQuestionAnswer] = []
    
    init(data:[String:AnyObject]) {
        super.init()
    
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_prov_id"])
        questionAr = ValidationsUtility.forceObjectToBeString(data["ques_name_ar"])
        questionEn = ValidationsUtility.forceObjectToBeString(data["ques_name"])

        
        if let questionHasAnswers = data["answers"] as? NSArray {
            for answerData in questionHasAnswers {
                self.answers.append(ActivityBookingQuestionAnswer(data: answerData as! [String : AnyObject]))
            }
        }
        
    }
}

class ActivityBookingQuestionAnswer : NSObject{

    var id = ""
    var activityProviderID = ""
    var questionID = ""
    var choiceAr = ""
    var choiceEn = ""
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_prov_id"])
        questionID = ValidationsUtility.forceObjectToBeString(data["ques_id"])
        choiceAr = ValidationsUtility.forceObjectToBeString(data["choice_ar"])
        choiceEn = ValidationsUtility.forceObjectToBeString(data["choice"])
    }
}

class ActivityBookingDate : NSObject {
    var id = ""
    var dateAsString = ""
    var date = Date()
    var price = 0.0
    var times : [ActivityBookingTime] = []
    var title = ""
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        dateAsString = ValidationsUtility.forceObjectToBeString(data["date"])
        title = ValidationsUtility.forceObjectToBeString(data["title"])
        
        if(dateAsString != ""){
            date = DateUtility.convertStringToDate(dateAsString, dateFormat: "yyyy-MM-dd")!
        }
        price = Double(ValidationsUtility.forceObjectToBeString(data["price"])) ?? 0.0
        
        if let dateHasTimes = data["times"] as? NSArray {
            for timeData in dateHasTimes {
                self.times.append(ActivityBookingTime(data: timeData as! [String : AnyObject]))
            }
        }
        
    }
}

class ActivityBookingTime : NSObject {
    var id = ""
    var time = ""
    var isTaken = false
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        time = ValidationsUtility.forceObjectToBeString(data["time"])
        isTaken = ValidationsUtility.forceObjectToBeString(data["taken"]).toBool() ?? false
    }
}

class ActivityBookingService : NSObject {
    var id = ""
    var nameAr = ""
    var nameEn = ""
    var subServices : [ActivityBookingSubService] = []
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        nameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        nameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        
        if let activitySubServices = data["subService"] as? NSArray {
            for subServiceData in activitySubServices {
                self.subServices.append(ActivityBookingSubService(data: subServiceData as! [String : AnyObject]))
            }
        }
        
    }
}

class ActivityBookingSubService : NSObject {
    var id = ""
    var activityProviderID = ""
    var groupID = ""
    var nameAr = ""
    var nameEn = ""
    var briefAr = ""
    var briefEn = ""
    var type = ""
    var price = 0.0
    var prices : [SubServicePrice] = []
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_prov_id"])
        groupID = ValidationsUtility.forceObjectToBeString(data["group_id"])
        nameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        nameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        briefAr = ValidationsUtility.forceObjectToBeString(data["brief_ar"])
        briefEn = ValidationsUtility.forceObjectToBeString(data["brief"])
        type = ValidationsUtility.forceObjectToBeString(data["type"])
        price = Double(ValidationsUtility.forceObjectToBeString(data["price"])) ?? 0.0
        
        if let priceList =  data["PriceList"] as? [[String:AnyObject]]{
            for priceInfo in priceList{
                let price = SubServicePrice(data : priceInfo)
                prices.append(price)
            }
        }
    }
}

class SubServicePrice : NSObject{
    var id = ""
    var activityProviderID = ""
    var price = 0.0
    var title = ""
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_prov_id"])
        price = Double(ValidationsUtility.forceObjectToBeString(data["price"])) ?? 0.0
        title = ValidationsUtility.forceObjectToBeString(data["title"])
    }
}
