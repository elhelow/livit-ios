//
//  Country.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 9/28/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

class Country: NSObject {
    var countryNameEn = ""
    var countryNameAr = ""
    var countryImageURL = ""
    var countryImage : UIImage? = nil
    var countryID = ""
    var isEnabled : Bool = true
    var prefix = ""
    
    private var imageDownloader : ImageDataProvider?
    weak var delegate : OperationDelegate?
    
    
    init(data:[String:AnyObject]) {
        countryNameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        countryNameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        countryImageURL = ValidationsUtility.forceObjectToBeString(data["image"])
        countryID = ValidationsUtility.forceObjectToBeString(data["id"])
        prefix = ValidationsUtility.forceObjectToBeString(data["prefix"])
        if(data["enable"] as? Int == 0){
            isEnabled = false
        }
    }
    
    init(countryNameEn : String, countryNameAr : String, countryImageName: String, countryID: String, prefix: String) {
        self.countryNameEn = countryNameEn
        self.countryNameAr = countryNameAr
        self.countryImage = UIImage(named: countryImageName)
        self.countryID = countryID
        self.prefix = prefix
    }
    
    func loadImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.countryImage == nil){
            if(imageDownloader == nil || !imageDownloader!.isDownloading){
                imageDownloader = ImageDataProvider(aDelegate: self)
                imageDownloader?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(countryImageURL)")
            }
        }
    }
}

extension Country : ImageDataProviderDelegate{
    func didLoadImage(_ image: UIImage, specialKey: String) {
        self.countryImage = image
        delegate?.didFinishOperation(.LoadImageGeneralOperationID)
    }
    
    func didFailLoadingImage() {
        delegate?.didRecieveErrorForOperation(.LoadImageGeneralOperationID, andWithError: nil)
    }
}
