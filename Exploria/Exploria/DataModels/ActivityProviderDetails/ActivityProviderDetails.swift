//
//  ActivityProviderDetails.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/30/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

class ActivityProviderDetails: NSObject {
    
    var id = ""
    var nameEn = ""
    var nameAr = ""
    var descriptionEn = ""
    var descriptionAr = ""
    var offersEn = ""
    var offersAr = ""
    var coursesEn = ""
    var coursesAr = ""
    var imageURL = ""
    var image : UIImage? = nil
    var isFavorited = false
    var lat = ""
    var lng = ""
    var rating = 0.0
    var dynamicContent : [DynamicContent] = []
    var gallery : [GalleryItem] = []
    var review : Review?
    var providerID = ""
    var hasCourses = false
    var isBooked = false
    
    var videoLink = ""
    
    private var imageDownloader : ImageDataProvider?
    weak var delegate : OperationDelegate?
    
    
    override init() {
        
    }
    
    init(data:[String:AnyObject]) {
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        nameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        nameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        descriptionEn = ValidationsUtility.forceObjectToBeString(data["description"])
        descriptionAr = ValidationsUtility.forceObjectToBeString(data["description_ar"])
        offersEn = ValidationsUtility.forceObjectToBeString(data["offers"])
        offersAr = ValidationsUtility.forceObjectToBeString(data["offers_ar"])
        coursesEn = ValidationsUtility.forceObjectToBeString(data["courses"])
        coursesAr = ValidationsUtility.forceObjectToBeString(data["courses_ar"])
        imageURL = ValidationsUtility.forceObjectToBeString(data["main_image"])
        videoLink = ValidationsUtility.forceObjectToBeString(data["video_link"])
        lat = ValidationsUtility.forceObjectToBeString(data["lat"])
        lng = ValidationsUtility.forceObjectToBeString(data["lng"])
        isFavorited = ValidationsUtility.forceObjectToBeString(data["favorite"]).toBool()!
        providerID = ValidationsUtility.forceObjectToBeString(data["activity_prov_id"])
        hasCourses = ValidationsUtility.forceObjectToBeString(data["has_course"]).toBool() ?? false
        isBooked = ValidationsUtility.forceObjectToBeString(data["booked"]).toBool() ?? false
        if(ValidationsUtility.isStringNotEmpty(ValidationsUtility.forceObjectToBeString(data["Rating"]))){
            rating = Double(ValidationsUtility.forceObjectToBeString(data["Rating"]))!
        }
        
        if let dynamicContents = data["DynamicDescription"] as? NSArray {
            for aDynamicContentData in dynamicContents{
                dynamicContent.append(DynamicContent(data: aDynamicContentData as! [String : AnyObject]))
            }
        }
        
        if let reviews = data["Review"] as? NSArray {
            for aReview in reviews{
                review = Review(data: aReview as! [String : AnyObject])
            }
        }
        
        if let aGallery = data["Gallery"] as? NSArray {
            for galleryItem in aGallery{
                gallery.append(GalleryItem(data: galleryItem as! [String : AnyObject]))
            }
        }
        
    }
    
    func loadImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.image == nil){
            if(imageDownloader == nil || !imageDownloader!.isDownloading){
                imageDownloader = ImageDataProvider(aDelegate: self)
                imageDownloader?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(imageURL)")
            }
        }
    }
}

extension ActivityProviderDetails : ImageDataProviderDelegate{
    func didLoadImage(_ image: UIImage, specialKey: String) {
        self.image = image
        delegate?.didFinishOperation(.LoadImageGeneralOperationID)
    }
    
    func didFailLoadingImage() {
        delegate?.didRecieveErrorForOperation(.LoadImageGeneralOperationID, andWithError: nil)
    }
}
