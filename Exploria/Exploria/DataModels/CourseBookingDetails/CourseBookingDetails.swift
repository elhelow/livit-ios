//
//  CourseBookingDetails.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/14/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

class CourseBookingDetails : NSObject {
    var id = ""
    var activityProviderID = ""
    var nameEn = ""
    var nameAr = ""
    var price = ""
    var overviewEn = ""
    var overviewAr = ""
    var lat = ""
    var lng = ""
    var organizerEn = ""
    var organizerAr = ""
    var courseImageURL = ""
    var organizerImageURL = ""
    var date = ""
    var priceType = ""
    var isFavorite = false
    var isBooked = false
    
    var isPreDefined = false
    var isScheduled = false
    
    var predefinedLessons : [CourseBookingPredefinedLesson] = []
    var scheduledLessons : [CourseBookingScheduledLesson] = []
    
    
    var courseImage : UIImage? = nil
    var organizerImage : UIImage? = nil
    
    fileprivate var organizerImageKey = "OR_IMG"
    private var imageDownloader : ImageDataProvider?
    private var imageDownloader2 : ImageDataProvider?
    weak var delegate : OperationDelegate?
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_provider_id"])
        nameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        nameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        price = ValidationsUtility.forceObjectToBeString(data["price"])
        overviewEn = ValidationsUtility.forceObjectToBeString(data["overview"])
        overviewAr = ValidationsUtility.forceObjectToBeString(data["overview_ar"])
        lat = ValidationsUtility.forceObjectToBeString(data["lat"])
        lng = ValidationsUtility.forceObjectToBeString(data["lng"])
        organizerEn = ValidationsUtility.forceObjectToBeString(data["organizer_name"])
        organizerAr = ValidationsUtility.forceObjectToBeString(data["organizer_name_ar"])
        courseImageURL = ValidationsUtility.forceObjectToBeString(data["image"])
        organizerImageURL = ValidationsUtility.forceObjectToBeString(data["icon"])
        date = ValidationsUtility.forceObjectToBeString(data["date"])
        priceType = ValidationsUtility.forceObjectToBeString(data["price_type"])
        isFavorite = ValidationsUtility.forceObjectToBeString(data["favorite"]).toBool() ?? false
        isBooked = ValidationsUtility.forceObjectToBeString(data["booked"]).toBool() ?? false
        
        if let predefinedLessons = data["PreDefined_lessons"] as? NSArray {
            if(predefinedLessons.count > 0){
                isPreDefined = true
            }
            for predefinedLessonData in predefinedLessons {
                self.predefinedLessons.append(CourseBookingPredefinedLesson(data: predefinedLessonData as! [String : AnyObject]))
            }
        }
        
        if let scheduledLessons = data["scheduled_lessons"] as? NSArray {
            if(scheduledLessons.count > 0){
                isScheduled = true
            }
            for scheduledLessonData in scheduledLessons {
                self.scheduledLessons.append(CourseBookingScheduledLesson(data: scheduledLessonData as! [String : AnyObject]))
            }
        }
    }
    
    func loadCourseImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.courseImage == nil){
            if(imageDownloader == nil || !imageDownloader!.isDownloading){
                imageDownloader = ImageDataProvider(aDelegate: self)
                imageDownloader?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(courseImageURL)")
            }
        }
    }
    
    func loadOrganizerImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.organizerImage == nil){
            if(imageDownloader2 == nil || !imageDownloader2!.isDownloading){
                imageDownloader2 = ImageDataProvider(aDelegate: self)
                imageDownloader2?.specialKey = organizerImageKey
                imageDownloader2?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(organizerImageURL)")
            }
        }
    }
}

extension CourseBookingDetails : ImageDataProviderDelegate{
    func didLoadImage(_ image: UIImage, specialKey: String) {
        if(specialKey == organizerImageKey){
            self.organizerImage = image
        }else{
            self.courseImage = image
        }
        
        delegate?.didFinishOperation(.LoadImageGeneralOperationID)
    }
    
    func didFailLoadingImage() {
        delegate?.didRecieveErrorForOperation(.LoadImageGeneralOperationID, andWithError: nil)
    }
}

class CourseBookingPredefinedLesson : NSObject {
    var id = ""
    var activityProviderID = ""
    var hasSchedule = false
    var price = 0.0
    var nameAr = ""
    var nameEn = ""
    var bookDate = ""
    var startTime = ""
    var endTime = ""
    var date = ""
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_provider_id"])
        hasSchedule = ValidationsUtility.forceObjectToBeString(data["has_schedule"]).toBool() ?? false
        price = Double(ValidationsUtility.forceObjectToBeString(data["price"])) ?? 0.0
        nameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        nameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        bookDate = ValidationsUtility.forceObjectToBeString(data["book_date"])
        startTime = ValidationsUtility.forceObjectToBeString(data["start_time"])
        endTime = ValidationsUtility.forceObjectToBeString(data["end_time"])
        date = ValidationsUtility.forceObjectToBeString(data["date"])
    }
}

class CourseBookingScheduledLesson : NSObject {
    var id = ""
    var activityProviderID = ""
    var hasSchedule = false
    var price = 0.0
    var nameAr = ""
    var nameEn = ""
    var date = ""
    var dates : [CourseBookingDate] = []
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_provider_id"])
        hasSchedule = ValidationsUtility.forceObjectToBeString(data["has_schedule"]).toBool() ?? false
        price = Double(ValidationsUtility.forceObjectToBeString(data["price"])) ?? 0.0
        nameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        nameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        date = ValidationsUtility.forceObjectToBeString(data["date"])
        
        if let availableDates = data["PreferedDate"] as? NSArray {
            for dateData in availableDates {
                self.dates.append(CourseBookingDate(data: dateData as! [String : AnyObject]))
            }
        }
        
    }
}

class CourseBookingDate : NSObject {
    var id = ""
    var dateAsString = ""
    var date = Date()
    var times : [CourseBookingTime] = []
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        dateAsString = ValidationsUtility.forceObjectToBeString(data["date"])
        date = DateUtility.convertStringToDate(dateAsString, dateFormat: "yyyy-MM-dd")!
        
        if let dateHasTimes = data["times"] as? NSArray {
            for timeData in dateHasTimes {
                self.times.append(CourseBookingTime(data: timeData as! [String : AnyObject]))
            }
        }
        
    }
}

class CourseBookingTime : NSObject {
    var id = ""
    var activityProviderID = ""
    var dateID = ""
    var startTime = ""
    var endTime = ""
    var isTaken = false
    var date = ""
    var price = 0.0
    
    init(data:[String:AnyObject]) {
        super.init()
        
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_prov_id"])
        dateID = ValidationsUtility.forceObjectToBeString(data["date_id"])
        startTime = ValidationsUtility.forceObjectToBeString(data["start_time"])
        endTime = ValidationsUtility.forceObjectToBeString(data["end_time"])
        isTaken = ValidationsUtility.forceObjectToBeString(data["taken"]).toBool() ?? false
        date = ValidationsUtility.forceObjectToBeString(data["date"])
        price = Double(ValidationsUtility.forceObjectToBeString(data["lesson_price"])) ?? 0.0
        
    }
}
