//
//  BookingQuery.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 2/18/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

class BookingQuery{
    var activityName = ""
    var activityPrice = ""
    var activityProviderID = ""
    var dateAsString = ""
    var depositeValue = ""
    var numberOfPlayers = ""
    var paymentMethod = "K-net"
    var paymentType = ""
    var pricingType = ""
    var questions : [String] = []
    var dateAsID = ""
    var dateTitle = ""
    var timeAsID = ""
    var services : [[String:String]] = [[:]]
    var timeAsString = ""
    var totalPayment = ""
}
