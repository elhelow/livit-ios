//
//  GeneralProtocols.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 11/25/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation

protocol FavoriteProtocol : NSObjectProtocol {
    func toggleFavoriteStatusForItemWithIndex(_ index : Int)
}
