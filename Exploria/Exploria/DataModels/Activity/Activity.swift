//
//  Activity.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/1/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

class Activity: NSObject {
    var id = ""
    var nameEn = ""
    var nameAr = ""
    var imageURL = ""
    var image : UIImage? = nil
    var numberOfProviders = ""
    var categoriesDescriptionEn = ""
    var categoriesDescriptionAr = ""
    var coursesAndOffersEn = ""
    var coursesAndOffersAr = ""
    var isShowNumberOfCompanies = true
    var isShowCategories = true
    
    private var imageDownloader : ImageDataProvider?
    weak var delegate : OperationDelegate?
    
    
    init(data:[String:AnyObject]) {
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        nameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        nameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        imageURL = ValidationsUtility.forceObjectToBeString(data["image"])
        numberOfProviders = ValidationsUtility.forceObjectToBeString(data["NumOfProviders"])
        categoriesDescriptionEn = ValidationsUtility.forceObjectToBeString(data["MainCategories"])
        categoriesDescriptionAr = ValidationsUtility.forceObjectToBeString(data["MainCategories_ar"])
        coursesAndOffersEn = ValidationsUtility.forceObjectToBeString(data["hasCourses"])
        coursesAndOffersAr = ValidationsUtility.forceObjectToBeString(data["hasCourses_ar"])
        isShowNumberOfCompanies = ValidationsUtility.forceObjectToBeString(data["show_comp"]).toBool() ?? true
        isShowCategories = ValidationsUtility.forceObjectToBeString(data["show_cat"]).toBool() ?? true
    }
    
    func loadImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.image == nil){
            if(imageDownloader == nil || !imageDownloader!.isDownloading){
                imageDownloader = ImageDataProvider(aDelegate: self)
                imageDownloader?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(imageURL)")
            }
        }
    }
}

extension Activity : ImageDataProviderDelegate{
    func didLoadImage(_ image: UIImage, specialKey: String) {
        self.image = image
        delegate?.didFinishOperation(.LoadImageGeneralOperationID)
    }
    
    func didFailLoadingImage() {
        delegate?.didRecieveErrorForOperation(.LoadImageGeneralOperationID, andWithError: nil)
    }
}
