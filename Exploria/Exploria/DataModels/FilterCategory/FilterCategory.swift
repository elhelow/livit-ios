//
//  FilterCategory.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/11/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

class FilterCategory: NSObject {
    var id = ""
    var nameEn = ""
    var nameAr = ""
    var imageURL = ""
    var image : UIImage? = nil
    
    private var imageDownloader : ImageDataProvider?
    weak var delegate : OperationDelegate?
    
    override init() {
        
    }
    
    init(data:[String:AnyObject]) {
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        nameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        nameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        imageURL = ValidationsUtility.forceObjectToBeString(data["image"])
        
    }
    
    func loadImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.image == nil){
            if(imageDownloader == nil || !imageDownloader!.isDownloading){
                imageDownloader = ImageDataProvider(aDelegate: self)
                imageDownloader?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(imageURL)")
            }
        }
    }
}

extension FilterCategory : ImageDataProviderDelegate{
    func didLoadImage(_ image: UIImage, specialKey: String) {
        self.image = image
        delegate?.didFinishOperation(.LoadImageGeneralOperationID)
    }
    
    func didFailLoadingImage() {
        delegate?.didRecieveErrorForOperation(.LoadImageGeneralOperationID, andWithError: nil)
    }
}


