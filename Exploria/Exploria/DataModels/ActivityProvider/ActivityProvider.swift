//
//  ActivityProvider.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/10/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import UIKit

class ActivityProvider: NSObject {
    var id = ""
    var categoryID = ""
    var nameEn = ""
    var nameAr = ""
    var rating = 0.0
    var averagePrice = ""
    var hasDeposite = false
    var imageURL = ""
    var categoryImageURL = ""
    
    var image : UIImage? = nil
    var categoryImage : UIImage? = nil
    var isFavorited = false
    
    var lat = ""
    var lng = ""
    var numberOfProviders = ""
    var activityNameEn = ""
    var activityNameAr = ""
    var coursesAndOffersEn = ""
    var coursesAndOffersAr = ""
    
    private var imageDownloader : ImageDataProvider?
    private var imageDownloader2 : ImageDataProvider?
    weak var delegate : OperationDelegate?
    
    
    override init() {
        
    }
    
    init(data:[String:AnyObject]) {
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        nameEn = ValidationsUtility.forceObjectToBeString(data["name"])
        nameAr = ValidationsUtility.forceObjectToBeString(data["name_ar"])
        rating = Double(ValidationsUtility.forceObjectToBeString(data["rating"]))!
        averagePrice = ValidationsUtility.forceObjectToBeString(data["average_price"])
        hasDeposite = ValidationsUtility.forceObjectToBeString(data["has_deposite"]).toBool()!
        isFavorited = ValidationsUtility.forceObjectToBeString(data["favorite"]).toBool()!
        imageURL = ValidationsUtility.forceObjectToBeString(data["image"])
        numberOfProviders = ValidationsUtility.forceObjectToBeString(data["NumOfProviders"])
        activityNameEn = ValidationsUtility.forceObjectToBeString(data["MainActivityName_Ar"])
        activityNameAr = ValidationsUtility.forceObjectToBeString(data["MainCategories_ar"])
        coursesAndOffersEn = ValidationsUtility.forceObjectToBeString(data["hasCourses"])
        coursesAndOffersAr = ValidationsUtility.forceObjectToBeString(data["hasCourses_ar"])
        lat = ValidationsUtility.forceObjectToBeString(data["lat"])
        lng = ValidationsUtility.forceObjectToBeString(data["lng"])
        categoryImageURL = ValidationsUtility.forceObjectToBeString(data["categoryImage"])
        categoryID = ValidationsUtility.forceObjectToBeString(data["categoryID"])
    }
    
    func loadImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.image == nil){
            if(imageDownloader == nil || !imageDownloader!.isDownloading){
                imageDownloader = ImageDataProvider(aDelegate: self)
                imageDownloader?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(imageURL)")
            }
        }
    }
    
    func loadCategoryImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.categoryImage == nil){
            if((imageDownloader2 == nil || !imageDownloader2!.isDownloading) && categoryImageURL != ""){
                imageDownloader2 = ImageDataProvider(aDelegate: self)
                imageDownloader2?.specialKey = "CA"
                imageDownloader2?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(categoryImageURL)")
            }
        }
    }
}

extension ActivityProvider : ImageDataProviderDelegate{
    func didLoadImage(_ image: UIImage, specialKey: String) {
        if(specialKey == "CA"){
            self.categoryImage = image
            delegate?.didFinishOperation(.LoadingCategoryImage)
        }else{
            self.image = image
            delegate?.didFinishOperation(.LoadImageGeneralOperationID)
        }
    }
    
    func didFailLoadingImage() {
        delegate?.didRecieveErrorForOperation(.LoadImageGeneralOperationID, andWithError: nil)
    }
}

extension String {
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return false
        }
    }
}
