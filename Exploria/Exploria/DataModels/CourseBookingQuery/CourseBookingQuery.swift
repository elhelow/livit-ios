//
//  CourseBookingQuery.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 2/18/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

class CourseBookingQuery{
    var activityProviderID = ""
    var lessonsIDs : [String] = []
    var paymentMethod = "K-net"
    var selectedDatesIDs : [String] = []
    var selectedTimesIDs : [String] = []
    var totalPayment = ""
    var pricingType = ""
}
