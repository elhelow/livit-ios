//
//  SpecialOffer.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 1/19/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

class SpecialOffer: NSObject {
    var id = ""
    var activityProviderID = ""
    var imageURL = ""
    var image : UIImage? = nil
    var date = ""
    
    private var imageDownloader : ImageDataProvider?
    weak var delegate : OperationDelegate?
    
    
    init(data:[String:AnyObject]) {
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_provider_id"])
        imageURL = ValidationsUtility.forceObjectToBeString(data["image"])
        date = ValidationsUtility.forceObjectToBeString(data["date"])
    }
    
    func loadImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.image == nil){
            if(imageDownloader == nil || !imageDownloader!.isDownloading){
                imageDownloader = ImageDataProvider(aDelegate: self)
                imageDownloader?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(imageURL)")
            }
        }
    }
}

extension SpecialOffer : ImageDataProviderDelegate{
    func didLoadImage(_ image: UIImage, specialKey: String) {
        self.image = image
        delegate?.didFinishOperation(.LoadImageGeneralOperationID)
    }
    
    func didFailLoadingImage() {
        delegate?.didRecieveErrorForOperation(.LoadImageGeneralOperationID, andWithError: nil)
    }
}
