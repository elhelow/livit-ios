//
//  PromoCode.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 2/27/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

class PromoCode: NSObject {
    var id = ""
    var activityProviderID = ""
    var date = ""
    var expiryDate = ""
    var code = ""
    var percentage = 0.0
    
    init(data:[String:AnyObject]) {
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_provider_id"])
        code = ValidationsUtility.forceObjectToBeString(data["code"])
        percentage = Double(ValidationsUtility.forceObjectToBeString(data["percentage"])) ?? 0.0
        expiryDate = ValidationsUtility.forceObjectToBeString(data["expire_date"])
        date = ValidationsUtility.forceObjectToBeString(data["date"])
    }
}
