//
//  BookingHistory.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 2/28/18.
//  Copyright © 2018 Exploria. All rights reserved.
//

import Foundation

class BookingHistory: NSObject {
    var id = ""
    var activityProviderID = ""
    var date = ""
    var depositeValue = ""
    var totalPayment = ""
    var numberOfPlayers = ""
    var providerNameAr = ""
    var providerNameEn = ""
    var providerImageURL = ""
    var type = ""
    var providerImage : UIImage? = nil
    
    private var imageDownloader : ImageDataProvider?
    weak var delegate : OperationDelegate?
    
    init(data:[String:AnyObject]) {
        id = ValidationsUtility.forceObjectToBeString(data["id"])
        activityProviderID = ValidationsUtility.forceObjectToBeString(data["activity_provider_id"])
        depositeValue = ValidationsUtility.forceObjectToBeString(data["deposite_value"])
        totalPayment = ValidationsUtility.forceObjectToBeString(data["total_payment"])
        numberOfPlayers = ValidationsUtility.forceObjectToBeString(data["num_of_player"])
        date = ValidationsUtility.forceObjectToBeString(data["date"])
        providerNameAr = ValidationsUtility.forceObjectToBeString(data["ActivityProviderName_ar"])
        providerNameEn = ValidationsUtility.forceObjectToBeString(data["ActivityProviderName"])
        providerImageURL = ValidationsUtility.forceObjectToBeString(data["ActivityProviderImage"])
        type = ValidationsUtility.forceObjectToBeString(data["type"])
    }
    
    func loadProviderImageWithDelegate(_ delegate : OperationDelegate){
        self.delegate = delegate
        if(self.providerImage == nil){
            if(imageDownloader == nil || !imageDownloader!.isDownloading){
                imageDownloader = ImageDataProvider(aDelegate: self)
                imageDownloader?.loadImageWithURL("\(BackEndConfigurations.getImagesURL())\(providerImageURL)")
            }
        }
    }
}

extension BookingHistory : ImageDataProviderDelegate{
    func didLoadImage(_ image: UIImage, specialKey: String) {
        self.providerImage = image
        delegate?.didFinishOperation(.LoadImageGeneralOperationID)
    }
    
    func didFailLoadingImage() {
        delegate?.didRecieveErrorForOperation(.LoadImageGeneralOperationID, andWithError: nil)
    }
}

