//
//  AlertUtility.swift
//  Le Cadeau
//
//  Created by Mohammad Shaker on 1/21/16.
//

import Foundation
import SCLAlertView

class AlertUtility {
    
    class func showErrorAlert(_ message: String!) {
        var font : UIFont = UIFont()
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            font = UIFont(name: "Dosis-Light", size: CGFloat(18))!
        }else{
            font = UIFont(name: "DroidArabicKufi", size: CGFloat(18))!
        }
        
        let appearance : SCLAlertView.SCLAppearance = SCLAlertView.SCLAppearance(kTitleFont: font, kTextFont: font, kButtonFont: font)
        
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.showTitle(
            NSLocalizedString(LocalizationManager.sharedInstance.getTranslationForKey("Error")!, comment: "Error"),
            subTitle: message,
            duration: 0.0,
            completeText: NSLocalizedString(LocalizationManager.sharedInstance.getTranslationForKey("Dismiss")!, comment: "Dismiss"),
            style: .error,
            colorStyle: 0x2a8376,
            colorTextButton: 0xFFFFFF
        )
    }
    
    class func showNoteAlert(_ message: String!) {
        var font : UIFont = UIFont()
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            font = UIFont(name: "Dosis-Light", size: CGFloat(18))!
        }else{
            font = UIFont(name: "DroidArabicKufi", size: CGFloat(18))!
        }
        
        let appearance : SCLAlertView.SCLAppearance = SCLAlertView.SCLAppearance(kTitleFont: font, kTextFont: font, kButtonFont: font)
        
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.showTitle(
            NSLocalizedString("Note", comment: "Note"),
            subTitle: message,
            duration: 0.0,
            completeText: NSLocalizedString("Dismiss", comment: "Dismiss"),
            style: .info,
            colorStyle: 0x2a8376,
            colorTextButton: 0xFFFFFF
        )
    }
    
    class func showSuccessAlert(_ message: String!,callback: @escaping () -> Void = { _ in }) {
        var font : UIFont = UIFont()
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            font = UIFont(name: "Dosis-Light", size: CGFloat(18))!
        }else{
            font = UIFont(name: "DroidArabicKufi", size: CGFloat(18))!
        }
        
        let appearance : SCLAlertView.SCLAppearance = SCLAlertView.SCLAppearance(kTitleFont: font, kTextFont: font, kButtonFont: font)
        
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.showTitle(
            NSLocalizedString("Success", comment: "Success"),
            subTitle: message,
            duration: 0.0,
            completeText: NSLocalizedString("Dismiss", comment: "Dismiss"),
            style: .success,
            colorStyle: 0x2a8376,
            colorTextButton: 0xFFFFFF
            ).setDismissBlock {
                callback()
        }
        
     
    }
    
    class func showAlertWithButton(msg: String ,title: String , buttonTitle: String, style: SCLAlertViewStyle = .notice, callback: @escaping () -> Void)
    {
        var font : UIFont = UIFont()
        if(LocalizationManager.sharedInstance.getCurrentLanguage() == .english){
            font = UIFont(name: "Dosis-Light", size: CGFloat(18))!
        }else{
            font = UIFont(name: "DroidArabicKufi", size: CGFloat(18))!
        }
        
        let appearance : SCLAlertView.SCLAppearance = SCLAlertView.SCLAppearance(kTitleFont: font, kTextFont: font, kButtonFont: font)
        
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton(buttonTitle) {
            callback()
        }
        
        alertView.showTitle(
            title,
            subTitle: msg,
            duration: 0.0,
            completeText: NSLocalizedString("Dismiss", comment: "Dismiss"),
            style: style,
            colorStyle: 0x2a8376,
            colorTextButton: 0xFFFFFF
        )
        
    }
}
