//
//  GenericDataUtility.swift
//  
//
//  Created by Ahmed AbdEl-Samie on 5/25/17.
//  Copyright © 2017 Ahmed AbdEl-Samie. All rights reserved.
//

import Foundation

/**
 GenericDataUtility is a class which is responsible for providing the generic data of the application.
 Such as professions and genders ...etc
 */
class GenericDataUtility: NSObject {
    
    /**
     Will load countries based on the current langauge of the application.
     
     - returns: Countries as an array based on the current langauge.
     */
    class func countries() -> [String] {
        let languageAsString: String = LocalizationManager.sharedInstance.getCurrentLanguage().rawValue
        let path: String? = Bundle.main.path(forResource: "LocalizedData-\(languageAsString)", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!) as! [String : Any]
        return dict["Countries"] as! [String]!
    }
}
