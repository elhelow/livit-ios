//
//  VideoUtility.swift
//  Exploria
//
//  Created by Ahmed AbdEl-Samie on 10/31/17.
//  Copyright © 2017 Exploria. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation


class VideoUtility {
    class func playVideo(withURL url: String) {
        if ((url as NSString).range(of: "youtube")).location == NSNotFound && ((url as NSString).range(of: "youtu.be")).location == NSNotFound {
            let player = AVPlayer(url: URL(string: url)!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            UIApplication.shared.keyWindow?.rootViewController?.present(playerViewController, animated: true) { _ in }
        }
        else {
            let videoPlayerViewController = XCDYouTubeVideoPlayerViewController(videoIdentifier: self.extractYoutubeId(fromLink: url))
            UIApplication.shared.keyWindow?.rootViewController?.present(videoPlayerViewController as UIViewController , animated: true) { _ in }
            videoPlayerViewController.moviePlayer.play()
        }
    }
    
    class func extractYoutubeId(fromLink link: String) -> String {
        let regexString = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        let regExp = try? NSRegularExpression(pattern: regexString, options: .caseInsensitive)
        let array = regExp?.matches(in: link, options: [], range: NSRange(location: 0, length: (link.characters.count )))
        if (array?.count)! > 0 {
            let result = array?.first
            return (link as NSString).substring(with: result?.range ?? NSRange())
        }
        return link
    }

}
