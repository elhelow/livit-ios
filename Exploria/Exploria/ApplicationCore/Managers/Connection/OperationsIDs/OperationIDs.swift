//
//  OperationIDs.swift
//  
//
//  Created by Ahmed Abd El-Samie on 5/20/17.
//  Copyright © 2017 Ahmed AbdEl-Samie. All rights reserved.
//

import Foundation

/**
 Operation IDs which is unique identifiers to identify every request is going out from the application
 */
enum OperationID : Int {
    case None
    case LoadImageGeneralOperationID
    case LoadCountries
    case LoadActivities
    case LoadActivityProviders
    case LoadMapActivityProviders
    case LoadOffers
    case LoadSpecialOffers
    case LoadingCategoryImage
    case LoadFilterCategories
    case FilterActivitiesByCategories
    case SearchActivitiesByKeyword
    case SortActivities
    case LoadActivityProviderDetails
    case LoadActivityProviderReviews
    case SignIn
    case SignUp
    case UpdateProfile
    case ToggleFavoriteStatus
    case FavoritedActivityProviders
    case BookingActivityDetails
    case BookingCourseDetails
    case AddReview
    case AddRate
    case ContactUs
    case SaveBookingActivityDetails
    case SaveCourseDetails
    case CheckPromoCode
    case MyBookings
}

